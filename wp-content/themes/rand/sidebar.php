<?php if ( is_active_sidebar( 'blog-sidebar' )  ) : ?>
	<aside id="secondary" class="sidebar widget-area col-xs-12 col-sm-4" role="complementary">
		<?php dynamic_sidebar( 'blog-sidebar' ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
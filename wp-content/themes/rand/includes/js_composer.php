<?php
add_action( 'vc_before_init', 'rand_theme_vc_integration' );
function rand_theme_vc_integration() {
	//vc_remove_param( "vc_row", "full_width" );
	$attributes = array(
					array(
						'type' => 'dropdown',
						'heading' => "Row Style",
						'param_name' => 'row_style',
						'weight' => 89,
						'value' => array( "Default" => "default" , "Alternate" => "alternate" ),
						'description' => __( "Select styling options for row and content", RM_TEXT_DOMAIN )
					),
					array(
						'type' => 'dropdown',
						'heading' => "Background Position Y",
						'param_name' => 'bg_pos_y',
						'value' => array( "Default" => "" , "Top" => "top" , "Center" => "center" , "Bottom" => "bottom" ),
						'description' => __( "Select background Y position", RM_TEXT_DOMAIN ),
						'group' => __( 'Design Options', RM_TEXT_DOMAIN ),
					),
					array(
						'type' => 'dropdown',
						'heading' => "Background Position X",
						'param_name' => 'bg_pos_x',
						'value' => array( "Default" => "" , "Left" => "left" , "Center" => "center" , "Right" => "right" ),
						'description' => __( "Select background X position", RM_TEXT_DOMAIN ),
						'group' => __( 'Design Options', RM_TEXT_DOMAIN ),
					),
					/*array(
						'type' => 'dropdown',
						'heading' => "Background Position Y (Large Devices)",
						'param_name' => 'bg_pos_y_lg',
						'value' => array( "Default" => "" , "Top" => "top" , "Center" => "center" , "Bottom" => "bottom" ),
						'description' => __( "Select background Y position", RM_TEXT_DOMAIN ),
						'group' => __( 'Design Options', RM_TEXT_DOMAIN ),
					),
					array(
						'type' => 'dropdown',
						'heading' => "Background Position X(Large Devices)",
						'param_name' => 'bg_pos_x_lg',
						'value' => array( "Default" => "" , "Left" => "left" , "Center" => "center" , "Right" => "right" ),
						'description' => __( "Select background X position", RM_TEXT_DOMAIN ),
						'group' => __( 'Design Options', RM_TEXT_DOMAIN ),
					),
					array(
						'type' => 'dropdown',
						'heading' => "Background Position Y (Medium Devices)",
						'param_name' => 'bg_pos_y_md',
						'value' => array( "Default" => "" , "Top" => "top" , "Center" => "center" , "Bottom" => "bottom" ),
						'description' => __( "Select background Y position", RM_TEXT_DOMAIN ),
						'group' => __( 'Design Options', RM_TEXT_DOMAIN ),
					),
					array(
						'type' => 'dropdown',
						'heading' => "Background Position X(Medium Devices)",
						'param_name' => 'bg_pos_x_md',
						'value' => array( "Default" => "" , "Left" => "left" , "Center" => "center" , "Right" => "right" ),
						'description' => __( "Select background X position", RM_TEXT_DOMAIN ),
						'group' => __( 'Design Options', RM_TEXT_DOMAIN ),
					),
					array(
						'type' => 'dropdown',
						'heading' => "Background Position Y (Small Devices)",
						'param_name' => 'bg_pos_y_sm',
						'value' => array( "Default" => "" , "Top" => "top" , "Center" => "center" , "Bottom" => "bottom" ),
						'description' => __( "Select background Y position", RM_TEXT_DOMAIN ),
						'group' => __( 'Design Options', RM_TEXT_DOMAIN ),
					),
					array(
						'type' => 'dropdown',
						'heading' => "Background Position X(Small Devices)",
						'param_name' => 'bg_pos_x_sm',
						'value' => array( "Default" => "" , "Left" => "left" , "Center" => "center" , "Right" => "right" ),
						'description' => __( "Select background X position", RM_TEXT_DOMAIN ),
						'group' => __( 'Design Options', RM_TEXT_DOMAIN ),
					),
					array(
						'type' => 'dropdown',
						'heading' => "Background Position Y (Extra Small Devices)",
						'param_name' => 'bg_pos_y_xs',
						'value' => array( "Default" => "" , "Top" => "top" , "Center" => "center" , "Bottom" => "bottom" ),
						'description' => __( "Select background Y position", RM_TEXT_DOMAIN ),
						'group' => __( 'Design Options', RM_TEXT_DOMAIN ),
					),
					array(
						'type' => 'dropdown',
						'heading' => "Background Position X(Extra Small Devices)",
						'param_name' => 'bg_pos_x_xs',
						'value' => array( "Default" => "" , "Left" => "left" , "Center" => "center" , "Right" => "right" ),
						'description' => __( "Select background X position", RM_TEXT_DOMAIN ),
						'group' => __( 'Design Options', RM_TEXT_DOMAIN ),
					),*/
				);
	vc_add_params( 'vc_row', $attributes ); // Note: 'vc_message' was used as a base for "Message box" element*/
	
	vc_map( array(
		"name" => __( "Site Logo", RM_TEXT_DOMAIN ),
		"base" => "site_logo",
		"category" => __( "Content", RM_TEXT_DOMAIN),
		"params" => array(
			 /*array(
				"type" => "posttypes",
				"heading" => __( "Post type", RM_TEXT_DOMAIN ),
				"param_name" => "post_type",
				"description" => __( "select the post type", RM_TEXT_DOMAIN )
			),*/
			array(
				"type" => "checkbox",
				"heading" => __( "Box", RM_TEXT_DOMAIN ),
				"param_name" => "box",
				"value" => '',
				"description" => __( "add box around image", RM_TEXT_DOMAIN ),
				"admin_label" => true
		 	),			
			array(
				"type" => "textfield",
				"heading" => __( "Image Size", RM_TEXT_DOMAIN ),
				"param_name" => "size",
				"value" => '',
				"description" => __( "Enter the iamge size eg thumbnail, medium, large,full or any other custom image size", RM_TEXT_DOMAIN ),
				"admin_label" => true
		 	), 
			array(
				"type" => "textfield",
				"heading" => __( "Attributes", RM_TEXT_DOMAIN ),
				"param_name" => "attr",
				"value" => '',
				"description" => __( "Enter attributes separated by commas eg class:class_name,alt:alt_text", RM_TEXT_DOMAIN ),
				"admin_label" => true
		 	),
	 	 ),
	) );
	vc_map( array(
		"name" => __( "Alternate Site Logo", RM_TEXT_DOMAIN ),
		"base" => "site_logo_alt",
		"category" => __( "Content", RM_TEXT_DOMAIN),
		"params" => array(
			 /*array(
				"type" => "posttypes",
				"heading" => __( "Post type", RM_TEXT_DOMAIN ),
				"param_name" => "post_type",
				"description" => __( "select the post type", RM_TEXT_DOMAIN )
			),*/
			array(
				"type" => "checkbox",
				"heading" => __( "Box", RM_TEXT_DOMAIN ),
				"param_name" => "box",
				"value" => '',
				"description" => __( "add box around image", RM_TEXT_DOMAIN ),
				"admin_label" => true
		 	),			
			array(
				"type" => "textfield",
				"heading" => __( "Image Size", RM_TEXT_DOMAIN ),
				"param_name" => "size",
				"value" => '',
				"description" => __( "Enter the iamge size eg thumbnail, medium, large,full or any other custom image size", RM_TEXT_DOMAIN ),
				"admin_label" => true
		 	), 
			array(
				"type" => "textfield",
				"heading" => __( "Attributes", RM_TEXT_DOMAIN ),
				"param_name" => "attr",
				"value" => '',
				"description" => __( "Enter attributes separated by commas eg class:class_name,alt:alt_text", RM_TEXT_DOMAIN ),
				"admin_label" => true
		 	), 
	 	 ),
	) );
	
	vc_map( array(
	  "name" => __( "Header", RM_TEXT_DOMAIN ),
	  "base" => "header",
	  "category" => __( "Content", RM_TEXT_DOMAIN),
	  "params" => array(
		 array(
			"type" => "checkbox",
			"heading" => __( "Featured Image as Background?", RM_TEXT_DOMAIN ),
			"param_name" => "featured_image_bg",
			"value" => 'yes',
			"description" => __( "", RM_TEXT_DOMAIN )
		 ),
		 array(
			"type" => "checkbox",
			"heading" => __( "Use the Post title?", RM_TEXT_DOMAIN ),
			"param_name" => "title",
			"value" => 'yes',
			"description" => __( "", RM_TEXT_DOMAIN )
		 ),
		 array(
			"type" => "textfield",
			"heading" => __( "Custom title", RM_TEXT_DOMAIN ),
			"param_name" => "custom_title",
			"value" => '',
			"description" => __( "", RM_TEXT_DOMAIN )
		 ),
		 array(
			"type" => "textfield",
			"heading" => __( "Class", RM_TEXT_DOMAIN ),
			"param_name" => "class",
			"value" => '',
			"description" => __( "", RM_TEXT_DOMAIN )
		 )
	  )
	) );
	
	$icon_styles = array( "Default" => "", "Box" => "box-style");
	
	$icon_styles = apply_filters( 'rand_vc_icon_styles', $icon_styles );
	
	vc_map( array(
		"name" => __( "icon", RM_TEXT_DOMAIN ),
		"base" => "icon",
		"category" => __( "Content", RM_TEXT_DOMAIN),
		"params" => array(
			array(
				"type" => "dropdown",
				"heading" => __( "Style", RM_TEXT_DOMAIN ),
				"param_name" => "style",
				"value" => $icon_styles,
				"description" => __( "select the style of the icon box", RM_TEXT_DOMAIN ),
				"admin_label" => true
		 	),
			array(
				"type" => "textfield",
				"heading" => __( "Icon Class", RM_TEXT_DOMAIN ),
				"param_name" => "class",
				"value" => '',
				"description" => __( "enter the class for the icon such as fa fa-*", RM_TEXT_DOMAIN ),
				"admin_label" => true
		 	),
			array(
				"type" => "textfield",
				"heading" => __( "Title", RM_TEXT_DOMAIN ),
				"param_name" => "title",
				"value" => '',
				"description" => __( "enter the the title that goes next to the icon", RM_TEXT_DOMAIN ),
				"admin_label" => true
		 	),
			array(
				"type" => "textfield",
				"heading" => __( "Inner Icon Class", RM_TEXT_DOMAIN ),
				"param_name" => "inner_icon_class",
				"value" => '',
				"description" => __( "enter the class for the icon such as fa fa-* for the icon inside the content box", RM_TEXT_DOMAIN ),
				"admin_label" => true
		 	),
		 	array(
				"type" => "textarea_html",
				"heading" => __( "Content", RM_TEXT_DOMAIN ),
				"param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
				"value" => __( "", RM_TEXT_DOMAIN ),
				"description" => __( "Enter your content.", RM_TEXT_DOMAIN ),
				"admin_label" => true
			 )			 
	 	 )
	) );
	vc_map( array(
		"name" => __( "Box Button", RM_TEXT_DOMAIN ),
		"base" => "box_btn",
		"category" => __( "Content", RM_TEXT_DOMAIN),
		"params" => array(
		 	array(
				"type" => "vc_link",
				"heading" => __( "Link", RM_TEXT_DOMAIN ),
				"param_name" => "vc_link", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
				"value" => __( "", RM_TEXT_DOMAIN ),
				"description" => __( "Enter your link.", RM_TEXT_DOMAIN )
			 ),
			 array(
				"type" => "dropdown",
				"heading" => __( "Align", RM_TEXT_DOMAIN ),
				"param_name" => "vc_align",
				"value" => array( "None" => "" , "Left" => "pull-left" , "Right" => "pull-right", "Center" => "center-block" ),
				"description" => __( "select the alignment of the button", RM_TEXT_DOMAIN )
			),
			array(
				"type" => "textfield",
				"heading" => __( "Extra Classes", RM_TEXT_DOMAIN ),
				"param_name" => "class",
				"value" => '',
				"description" => __( "enter extra classes", RM_TEXT_DOMAIN )
		 	),			 
	 	 ),
	) );
	vc_map( array(
		"name" => __( "Post Carousel", RM_TEXT_DOMAIN ),
		"base" => "post_carousel",
		"category" => __( "Content", RM_TEXT_DOMAIN),
		"params" => array(
			 array(
				"type" => "posttypes",
				"heading" => __( "Post type", RM_TEXT_DOMAIN ),
				"param_name" => "post_type",
				"description" => __( "select the post type", RM_TEXT_DOMAIN )
			),
			array(
				"type" => "textfield",
				"heading" => __( "Number Of Posts", RM_TEXT_DOMAIN ),
				"param_name" => "limit",
				"value" => '10',
				"description" => __( "enter the number of posts to grab, enter -1 for all. if pagination available it will limit to the number of posts per page", RM_TEXT_DOMAIN )
		 	),	
			/*array(
				"type" => "checkbox",
				"heading" => __( "Featured Only", RM_TEXT_DOMAIN ),
				"param_name" => "featured_only",
				"value" => '',
				"description" => __( "enter extra classes", RM_TEXT_DOMAIN )
		 	),	*/		 
	 	 ),
	) );
	vc_map( array(
		"name" => __( "Post Grid", RM_TEXT_DOMAIN ),
		"base" => "post_grid",
		"category" => __( "Content", RM_TEXT_DOMAIN),
		"params" => array(
			array(
				"type" => "textfield",
				"heading" => __( "Grid Id", RM_TEXT_DOMAIN ),
				"param_name" => "id",
				"value" => '',
				"description" => __( "enter the id", RM_TEXT_DOMAIN )
		 	),
			array(
				"type" => "posttypes",
				"heading" => __( "Post type", RM_TEXT_DOMAIN ),
				"param_name" => "post_type",
				"description" => __( "select the post type", RM_TEXT_DOMAIN )
			),
			array(
				"type" => "textfield",
				"heading" => __( "Number Of Posts", RM_TEXT_DOMAIN ),
				"param_name" => "limit",
				"value" => '10',
				"description" => __( "enter the number of posts to grab, enter -1 for all. if pagination available it will limit to the number of posts per page", RM_TEXT_DOMAIN )
		 	),	
			/*array(
				"type" => "checkbox",
				"heading" => __( "Featured Only", RM_TEXT_DOMAIN ),
				"param_name" => "featured_only",
				"value" => '',
				"description" => __( "enter extra classes", RM_TEXT_DOMAIN )
		 	),	*/		 
	 	 ),
	) );
	do_action('rand_vc_mapping');
}
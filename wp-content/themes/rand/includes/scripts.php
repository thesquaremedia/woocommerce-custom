<?php
/**
 * Load site scripts.
 */
function rand_theme_enqueue_scripts() {
	
	/*
	 * Load site JS.
	 */
	wp_enqueue_script( 'html5shiv' , 'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js');
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
	// jQuery.
	wp_enqueue_script( 'jquery' );
	// Bootstrap
	wp_enqueue_script( 'bootstrap-script', PARENT_URI . '/js/bootstrap.min.js', array( 'jquery' ), RAND_VERSION, true );
	wp_register_script( 'owl-carousel', PARENT_URI . '/js/owl.carousel.min.js', array('jquery'), RAND_VERSION, true );
	wp_enqueue_script( 'jquery-scrollTo', 'http://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/1.4.11/jquery.scrollTo.min.js', array('jquery') );
	wp_enqueue_script( 'jquery-localscroll', 'http://cdnjs.cloudflare.com/ajax/libs/jquery-localScroll/1.3.5/jquery.localScroll.min.js', array('jquery') );
	// Load Thread comments WordPress script.
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	/*
	 * Load site CSS.
	 */
	wp_enqueue_style( 'bootstrap-style', PARENT_URI . '/css/bootstrap.min.css' );

	//Main Style
	wp_enqueue_style( 'rand-style', get_stylesheet_uri() );
	wp_enqueue_style( 'font-awesome-style', PARENT_URI . '/css/font-awesome.min.css' );
	
	wp_register_style( 'owl-carousel', PARENT_URI . '/css/owl.carousel.css', NULL , RAND_VERSION, false);
	
}

add_action( 'wp_enqueue_scripts', 'rand_theme_enqueue_scripts', 10 );

function rand_admin_enqueue_scripts(){
	wp_register_script( 'rand-admin-ui', PARENT_URI . '/js/admin-ui.js', array('jquery', 'rand-select2'), RAND_VERSION, true );
	wp_register_style( 'rand-admin-ui', PARENT_URI . '/css/admin-ui.css', NULL, RAND_VERSION, false );
	wp_register_script( 'rand-select2', PARENT_URI . '/js/select2.min.js', array('jquery'), RAND_VERSION, true );
	wp_register_style( 'rand-select2', PARENT_URI . '/css/select2.min.css', NULL , RAND_VERSION, false );
}
add_action( 'admin_enqueue_scripts', 'rand_admin_enqueue_scripts' );


function rand_theme_wp_head_start(){
	?>
	<!-- rand head start -->
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php if ('' != of_get_option('favicon')) { ?>
		<link rel="shortcut icon" href="<?php echo stripslashes(of_get_option('favicon')); ?>" />
	<?php } ?>
	<!-- rand head start -->
	<?php
}
add_action( 'wp_head' , 'rand_theme_wp_head_start' , 1);

function rand_theme_wp_head_end(){
	?>
	<!-- rand head end -->
	<script>
		jQuery(document).ready(function($) {
			var scrollOffset = <?php if( of_get_option( 'sticky_header' ) ): ?>-(jQuery('#wrapper > header').outerHeight(true)+30) <?php else: ?> 0 <?php endif; ?>;
			
			jQuery(window).scroll(function($) {
				<?php if ( of_get_option( 'sticky_header' ) ): ?>
				if ( jQuery(this).scrollTop() > jQuery('#wrapper > header').outerHeight(true)/3 ){  
					jQuery('#main-container').css({'padding-top':jQuery('#wrapper > header').outerHeight(true)});
					jQuery('#wrapper > header').addClass("fixed-to-top");
					scrollOffset = -(jQuery('#wrapper > header').outerHeight(true)+30);					
					
				}
				else{
					jQuery('#wrapper > header').removeClass("fixed-to-top");
					jQuery('#main-container').removeAttr('style');
					scrollOffset = 0;
				}
				<?php endif;//check if sticky theme option is enabled ?>
				
				if ( ( jQuery(this).scrollTop() + jQuery('#wrapper > footer').outerHeight(true) ) >= (jQuery('#wrapper > footer').offset().top - jQuery('#wrapper > footer').outerHeight(true)) ){ 
					jQuery('.fixed').css({'bottom': (jQuery('#wrapper > footer').outerHeight(true)), 'top': 'auto'});
				}else{
					jQuery('.fixed').css({'top': (jQuery('#wrapper > header').outerHeight(true)+30), 'bottom' : 'auto'});
				}
			});
			jQuery(window).resize(function($) {
				<?php if ( of_get_option( 'sticky_header' ) ): ?>
				if ( jQuery(this).scrollTop() > jQuery('#wrapper > header').outerHeight(true)/3 ){
					jQuery('#main-container').css({'padding-top':jQuery('#wrapper > header').outerHeight(true)});
					scrollOffset = -jQuery('#wrapper > header').outerHeight(true);
				}
				<?php endif;//check if sticky theme option is enabled ?>
				
				jQuery('.fixed').width(jQuery('.fixed').parent().width());
			});
			  
			jQuery('.fixed').affix({
			  offset: {
				top: function () {
					return (this.top = (jQuery('#wrapper > header').outerHeight(true)+30) )
				}
			  }
			});				
			
			jQuery('.fixed').on('affixed.bs.affix', function () {
				 jQuery(this).width(jQuery(this).parent().width());
				 jQuery(this).css({'top': (jQuery('#wrapper > header').outerHeight(true)+30), 'bottom' : 'auto'});
			});
			
			
			
			$.localScroll({
				target: 'body',
				queue:true,
				duration:1000,
				hash:true,
				filter: '*:not(a[data-toggle="collapse"])',
				offset: {top:scrollOffset},	
				onBefore: function(){
						this.offset = {top:scrollOffset}
						//console.log(scrollOffset)
					}			
			});

			var hash = window.location.hash;

			if(hash){
				jQuery('a[href="/' + hash + '"]').addClass("active");
			};
					
			jQuery( 'a.modal-link' ).on( 'click', function(e){
				e.preventDefault();
				var modalID = jQuery(this).attr('href');
				$( modalID ).modal('show');
			});
		
		});
	</script>
	<!-- rand head end -->
	<?php
}
add_action( 'wp_head' , 'rand_theme_wp_head_end' , 999);

function rand_theme_wp_footer_start(){
	?>
	<!-- rand footer start -->
	<?php
	if ( of_get_option('back_to_top') == '1' ):
	?>
	<a href="#top" id="back-to-top" rel="nofollow"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
	<?php
	endif;
	?>
	<!-- rand footer start -->
	<?php
}
add_action( 'wp_footer' , 'rand_theme_wp_footer_start', 1 );
function rand_theme_wp_footer_end(){
	?>
	<!-- rand footer end -->
	<!-- rand footer end -->
	<?php
}
add_action( 'wp_footer' , 'rand_theme_wp_footer_end', 999 );
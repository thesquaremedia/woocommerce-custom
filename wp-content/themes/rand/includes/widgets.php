<?php
add_filter('dynamic_sidebar_params', 'rand_theme_add_classes_to_widget'); 
function rand_theme_add_classes_to_widget($params){
	//var_dump( $params );
    if ($params[0]['id'] == 'footer-sidebar' ){ //make sure its your widget id here
        // its your widget so you add  your classes
        $classe_to_add = 'col-xs-12 col-sm-12 col-md-4 '; // make sure you leave a space at the end
        $classe_to_add = 'class=" '.$classe_to_add;
        $params[0]['before_widget'] = str_replace('class="',$classe_to_add,$params[0]['before_widget']);
    }
	if ($params[0]['id'] == 'footer-bottom-sidebar' ){ //make sure its your widget id here
        // its your widget so you add  your classes
        $classe_to_add = 'container '; // make sure you leave a space at the end
        $classe_to_add = 'class=" '.$classe_to_add;
        $params[0]['before_widget'] = str_replace('class="',$classe_to_add,$params[0]['before_widget']);
    }
    return $params;
} 

/* 
 * ====================================================== Include Widgets =========================================
 */
 
require_once( PARENT_DIR . "/includes/widgets/the-title-widget.php" );	//require shortcodes file
 
// register Foo_Widget widget
function register_rand_widgets() {
    register_widget( 'The_Title_Widget' );
}
add_action( 'widgets_init', 'register_rand_widgets' );
 

<?php
/*
 * Loads the login Styles
 */
if ( ! has_action( 'login_enqueue_scripts', 'wp_print_styles' ) )
    add_action( 'login_enqueue_scripts', 'wp_print_styles', 11 );

function sm_login_scripts() {
    wp_enqueue_style( 'square-custom-login', PARENT_URI . '/css/login.css.php' );
}
add_action( 'login_enqueue_scripts', 'sm_login_scripts' );

/*
 * Changes Login Logo url to home URL
 */

function square_url_login(){
	return get_home_url(); // your URL here
}
add_filter('login_headerurl', 'square_url_login');

<?php
/*
 * Here we add custom styles to the TinyMCE editor in wordpress
 *
 * Apply styles to the visual editor
 */
function rand_mcekit_editor_style($url) {
    if ( !empty($url) )
        $url .= ',';
    // Retrieves the plugin directory URL
    // Change the path here if using different directories
	$url .= PARENT_URI . '/css/font-awesome.min.css,';
   	$url .= PARENT_URI . '/css/tinymce.css';
	//$url .= PARENT_URI . '/includes/options.css.php';
    return $url;
}
//add_filter('mce_css', 'rand_mcekit_editor_style');

/**
 * Add "Styles" drop-down
 */
function rand_mce_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
add_filter( 'mce_buttons_2', 'rand_mce_editor_buttons' );

/*
 * Add styles/classes to the "Styles" drop-down
 */
function rand_mce_before_init( $settings ) {
    $style_formats = array(
		array(
            'title' => '.table',
			'selector'	=> 'table',
            'classes' => 'table'
            ),
		array(
            'title' => '.big-text',
			'selector'	=> 'p',
            'classes' => 'big-text'
            ),
		array(
            'title' => 'Icon List',
			'selector'	=> 'ul',
            'classes' => 'fa-ul'
            ),
		array(
            'title' => 'Text Shadow',
			'inline'	=> 'span',
            'classes' => 'text-shadow'
            ),
    );
	$settings['style_formats_merge'] = false;
    
	$style_formats = apply_filters( 'rand_tinymce_style_formats', $style_formats );
	
	$settings['style_formats'] = json_encode( $style_formats );
    
	$default_colors =	'"000000", "Black",
						"993300", "Burnt orange",
						"333300", "Dark olive",
						"003300", "Dark green",
						"003366", "Dark azure",
						"000080", "Navy Blue",
						"333399", "Indigo",
						"333333", "Very dark gray",
						"800000", "Maroon",
						"FF6600", "Orange",
						"808000", "Olive",
						"008000", "Green",
						"008080", "Teal",
						"0000FF", "Blue",
						"666699", "Grayish blue",
						"808080", "Gray",
						"FF0000", "Red",
						"FF9900", "Amber",
						"99CC00", "Yellow green",
						"339966", "Sea green",
						"33CCCC", "Turquoise",
						"3366FF", "Royal blue",
						"800080", "Purple",
						"999999", "Medium gray",
						"FF00FF", "Magenta",
						"FFCC00", "Gold",
						"FFFF00", "Yellow",
						"00FF00", "Lime",
						"00FFFF", "Aqua",
						"00CCFF", "Sky blue",
						"993366", "Red violet",
						"FFFFFF", "White",
						"FF99CC", "Pink",
						"FFCC99", "Peach",
						"FFFF99", "Light yellow",
						"CCFFCC", "Pale green",
						"CCFFFF", "Pale cyan",
						"99CCFF", "Light sky blue",
						"CC99FF", "Plum"';
	
	$default_colors = apply_filters( 'rand_tinymce_default_colors', $default_colors );
	
	$custom_colors = 	'';
	
	$custom_colors = apply_filters( 'rand_tinymce_custom_colors', $custom_colors );
	
	// build colour grid default+custom colors
	$settings['textcolor_map'] = '['.$custom_colors.','.$default_colors.']';
	
	$rows = 5;
	
	$rows = apply_filters( 'rand_tinymce_textcolor_rows', $rows );
	
	// enable 6th row for custom colours in grid
	$settings['textcolor_rows'] = $rows;
	
	return $settings;
}
add_filter( 'tiny_mce_before_init', 'rand_mce_before_init' );


/* Learn TinyMCE style format options at http://www.tinymce.com/wiki.php/Configuration:formats */

/*
 *
 * Here we add custom shortcode select box into the TinyMCE
 *
 */
function rand_add_shortcode_selector(){
	// Don't bother doing this stuff if the current user lacks permissions
	if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
		return;

	// Add only in Rich Editor mode
	add_filter('mce_buttons', 'rand_register_shortcode_selector');
	add_filter('mce_external_plugins', 'rand_add_shortcode_selector_plugin');
    //you can use the filters mce_buttons_2, mce_buttons_3 and mce_buttons_4 
    //to add your button to other toolbars of your tinymce
}
add_action('admin_init', 'rand_add_shortcode_selector');

function rand_register_shortcode_selector( $buttons ){
	array_push( $buttons, 'rand_shortcode_select') ;
	return $buttons;
}
 
function rand_add_shortcode_selector_plugin( $plugin_array ){
	$plugin_array['rand_shortcode_select'] = PARENT_URI . '/js/custom-shortcodes-select.js';
	
	global $tinymce_version;
	$variant = ( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ) ? '' : '.min';

	if ( version_compare( $tinymce_version, '4100', '<' ) ) {

		$plugin_array['table'] = PARENT_URI . '/js/tinymce4-table/plugin' . $variant . '.js';

	} else {

		$plugin_array['table'] = PARENT_URI . '/js/tinymce41-table/plugin' . $variant . '.js';

	}
	
	return $plugin_array;
}

/*
 * Add add extra row to tinymce editor'
 */
function rand_enable_more_buttons( $buttons ) {
	//$buttons[] = 'fontselect';
	$buttons[] = 'fontsizeselect'; 
	$buttons[] = 'backcolor'; 
	//$buttons[] = 'image';
	//$buttons[] = 'media'; 
	//$buttons[] = 'anchor'; 
	//$buttons[] = 'sub'; 
	//$buttons[] = 'sup';
	//$buttons[] = 'hr';
	$buttons[] = 'table';
	//$buttons[] = 'wp_page';
	$buttons[] = 'cut'; 
	$buttons[] = 'copy'; 
	$buttons[] = 'paste';
	//$buttons[] = 'newdocument'; 
	$buttons[] = 'code';
	$buttons[] = 'cleanup'; 
	
	return $buttons;
}
add_filter("mce_buttons_3", "rand_enable_more_buttons");

function rand_mce_inits( $initArray ){
    $initArray['fontsize_formats'] = '8px 10px 12px 13px 14px 15px 16px 18px 20px 22px 24px 26px 28px 30px 32px 34px 36px 38px 40px 42px 44px 46px 50px 54px 60px 66px 70px 76px 80px 88px 90px 100px 114px';
    
    return $initArray;
}
add_filter('tiny_mce_before_init', 'rand_mce_inits');

/*
 *
 * Here we have a function to add custom mimes for upload
 *
 */
function rand_upload_mimes ( $existing_mimes = array() ) {

	// Add *.EPS files to Media upload
	$existing_mimes['zip'] = 'application/x-compressed';
	
	return $existing_mimes;
}
add_filter('upload_mimes', 'rand_upload_mimes');
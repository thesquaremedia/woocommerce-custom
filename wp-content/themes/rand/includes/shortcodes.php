<?php

require_once( PARENT_DIR . "/includes/shortcodes/wp-login-register-forms.php" );	//require wordpress login,register and forgot password forms shortcode

function rand_the_title_func(){
	return get_the_title();	
}
add_shortcode('the_title', 'rand_the_title_func');

function rand_featured_image_func( $atts ) {
	$atts = shortcode_atts( array(
		'box' => 'yes',
		'size' => 'medium',
		'attr' => '',
	), $atts, 'featured_image' );
	
	$attr = array();
	if ( $atts['attr'] !== '' ):
		$args = explode(',', $atts['attr']);
		
		foreach( $args as $key => $val):
			$nums = explode(':',$val);
			//var_dump( $nums );
			$attr[$nums[0]] = $nums[1];
		endforeach;
	endif;
	
	$output = '';
	if ( has_post_thumbnail() ):
		$image = get_the_post_thumbnail(get_the_ID(), $atts['size'], $attr);
		if ( $atts['box'] == 'yes'):
			$output .= '<div class="image-box featured_image_box">'. $image .'</div>';
		else:
			$output .= $image;
		endif;
	endif;
	
	return $output;
}
add_shortcode( 'featured_image', 'rand_featured_image_func' );

function rand_site_logo_func( $atts ) {
	$atts = shortcode_atts( array(
		'box' => 'no',
		'size' => 'medium',
		'attr' => '',
	), $atts, 'site_logo' );
	
	$attr = array();
	if ( $atts['attr'] !== '' ):
		$args = explode(',', $atts['attr']);
		
		foreach( $args as $key => $val):
			$nums = explode(':',$val);
			//var_dump( $nums );
			$attr[$nums[0]] = $nums[1];
		endforeach;
	endif;

	$output = '';
	if( of_get_option( 'logo' ) ):
		$logo_id		= of_get_option( 'logo' );
		$size			= $atts['size']; 
		$attr['class']	= !empty( $attr['class'] ) ? $attr['class'] . " attachment-$size size-$size" : " attachment-$size size-$size" ;  
		$image = wp_get_attachment_image( $logo_id, $size, false, $attr );
		if ( $atts['box'] == 'yes' || $atts['box'] == 1 ):
			$output .= '<div class="image-box logo-box">'. $image .'</div>';
		else:
			$output .= $image;
		endif;
	endif;
	
	return $output;
}
add_shortcode( 'site_logo', 'rand_site_logo_func' );

function rand_site_logo_alt_func( $atts ) {
	$atts = shortcode_atts( array(
		'box' => 'no',
		'size' => 'medium',
		'attr' => '',
	), $atts, 'site_logo' );
	
	$attr = array();
	if ( $atts['attr'] !== '' ):
		$args = explode(',', $atts['attr']);
		
		foreach( $args as $key => $val):
			$nums = explode(':',$val);
			//var_dump( $nums );
			$attr[$nums[0]] = $nums[1];
		endforeach;
	endif;
	
	$output = '';
	if( of_get_option( 'logo-alt' ) ):
		$logo_id		= of_get_option( 'logo-alt' );
		$size			= $atts['size']; 
		$attr['class']	= !empty( $attr['class'] ) ? $attr['class'] . " attachment-$size size-$size" : " attachment-$size size-$size" ;  
		$image = wp_get_attachment_image( $logo_id, $size, false, $attr );
		if ( $atts['box'] == 'yes' || $atts['box'] == 1 ):
			$output .= '<div class="image-box alt-logo-box">'. $image .'</div>';
		else:
			$output .= $image;
		endif;
	endif;
	
	return $output;
}
add_shortcode( 'site_logo_alt', 'rand_site_logo_alt_func' );

function rand_social_icons_shortcode( $atts ) {
    $atts = shortcode_atts( array(
        'icons' => ''        
    ), $atts, 'social_icons' );
	
	//$icons = explode('|', $atts['icons']);
	
	$icons = array();
	if ( $atts['icons'] !== '' ):
		$icons_arr = explode('|', $atts['icons']);
		
		foreach( $icons_arr as $key => $val):
			$nums = explode(':',$val);
			//write_log( $nums );
			$icons[$nums[0]] = $nums[1];
		endforeach;
	endif;
	
	$output = '<div class="social-icons">';
	foreach($icons as $key => $val):
		//write_log( of_get_option( $key ) );
		if (of_get_option( $key ) != '') :
			$output .= '<a href="'. stripslashes(of_get_option( $key )).'" title="'. $key .'" class="'. $val .'" target="_blank"></a>';
		endif;
	endforeach;
	
	$output .= '</div>';
	
    return $output;
}
add_shortcode( 'social_icons', 'rand_social_icons_shortcode' );

function rand_wp_menu_shortcode($atts){
	$atts = shortcode_atts( array(
        'menu'	=> '',
		'menu_type' => 'simple',
    ), $atts, 'wp_menu' );
	
	$menu 		= !empty( $atts['menu'] ) 		? $atts['menu']							: '';
	$menu_type 	= '';
	if( $atts['menu_type'] == 'mobile' ):
		$menu_type = 'Bootstrap_Walker_Nav_Menu';
	else:
		$menu_type = 'Rand_Simple_Walker_Nav_Menu';
	endif;
	
	
	if( !empty( $menu ) )
		return wp_nav_menu( array('menu' => $menu, 'walker'=> new $menu_type, 'echo' => false ) );
	
}
add_shortcode( 'wp_menu' , 'rand_wp_menu_shortcode' );

function rand_box_btn_shortcode( $atts ) {
    $atts = shortcode_atts( array(
        'post' 		=> '',
		'link' 		=> '',
		'text' 		=> '',
		'logged_link' 		=> '',
		'logged_text' 		=> '',
		'target' 	=> '',
		'rel' 		=> '',
		'class' 	=> '',
		'vc_link' 	=> '',
		'vc_align' 	=> '',    
    ), $atts, 'box_btn' );
	
	$post_id 	= !empty( $atts['post'] ) 		? $atts['post']							: get_the_id();
	$text		= !empty( $atts['text'] ) 		? $atts['text'] 						: get_the_title();
	$target		= !empty( $atts['target'] )		? ' target="' . $atts['target'] . '"'	: '';
	$rel		= !empty( $atts['rel'] ) 		? ' rel="' . $atts['rel'] . '"'			: '';
	$vc_align	= !empty( $atts['vc_align'] ) 	? ' ' . $atts['vc_align']				: '';
	$class		= !empty( $atts['class'] ) 		? ' ' . $atts['class'] . $vc_align		: '' . $vc_align;
	//var_dump($atts['link']);
	if ( !empty( $atts['vc_link']) ):
		$vc_link	= vc_build_link($atts['vc_link']);
		$link 		= !empty( $vc_link['url'] )		? $vc_link['url'] 							: '';
		$text		= !empty( $vc_link['title'] )	? $vc_link['title']							: $text;
		$target		= !empty( $vc_link['target'] )	? ' target="' . $vc_link['target'] . '"'	: '';
		$rel		= !empty( $vc_link['rel'] ) 	? ' rel="' . $vc_link['rel'] . '"'			: '';
	elseif( !empty( $atts['link'] ) && !empty( $atts['text'] ) ):
		$text = $atts['text'];
		$link = do_shortcode( str_replace('#site_url#', get_site_url(),$atts['link']) );
	elseif ( empty( $atts['post'] ) && !empty( $atts['text'] ) ):
		$text = $atts['text'];
		$link = get_the_permalink();
	elseif( !empty( $atts['post'] ) && !empty( $atts['text'] ) ):
		$text = $atts['text'];
		$link = get_the_permalink($post_id);
	elseif( !empty( $atts['post'] ) && empty( $atts['text'] ) ):
		$text = get_the_title($post_id);
		$link = get_the_permalink($post_id);
	endif;
	
	if( is_user_logged_in() && !empty( $atts['logged_link'] ) && !empty( $atts['logged_text'] ) ):
		$text = $atts['logged_text'];
		$link = do_shortcode( str_replace('#site_url#', get_site_url(),$atts['logged_link']) );
	endif;
	
	$output = '<a href="'. $link .'" title="'. $text .'" class="box-btn' . $class . '"' . $target . $rel . '>'. $text .'</a>';
	
	if( !empty( $atts['vc_align'] ) ):
		switch( $atts['vc_align'] ):
			case 'center-block':
				$vc_align = 'text-center ';
			break;
			case 'pull-left':
				$vc_align = 'text-left ';
			break;
			case 'pull-right':
				$vc_align = 'text-right ';
			break;
			default:
				$vc_align = '';
			break;
		endswitch;
		$output =	'<div class="' . $vc_align . '">' . $output . '</div>';
	endif;
	
	return $output;
}
add_shortcode( 'box_btn', 'rand_box_btn_shortcode' );

function rand_icon_shortcode( $atts, $content = null ) {
	 $atts = shortcode_atts( array(
        'class' => '',
		'title'	=> '',
		'inner_icon_class' => '',
		'style' => '',
    ), $atts, 'icon' );
	
	$icon_text 	= !empty($atts['title'])			? '<span class="icon-box-title">' . $atts['title'] . '</span>' 						: '';
	$icon 		= !empty($atts['class'])			? '<div class="icon-holder"><div class="icon-container"><i class="icon ' . $atts['class'] . '" aria-hidden="true"></i>' . $icon_text . '</div></div>' : '<div class="icon-holder"><div class="icon-container">' . $icon_text . '</div></div>';
	$inner_icon = !empty($atts['inner_icon_class']) ? '<i class="inner-icon ' . $atts['inner_icon_class'] . '" aria-hidden="true"></i>' : '';
	$style		= !empty($atts['style'])			? ' ' . $atts['style']																: '';
	$template	= !empty($atts['style'])			? '-' . $atts['style'] : '';
	
	$output = '';
	ob_start();

	// check if template has been overriden
	if ( file_exists( CHILD_DIR . '/templates/shortcodes/icon' . $template . '.php' ) ) {
		
		include( CHILD_DIR . '/templates/shortcodes/icon' . $template . '.php' );

	}else if( file_exists( PARENT_DIR . '/templates/shortcodes/icon' . $template . '.php' ) ){
		
		include( PARENT_DIR . '/templates/shortcodes/icon' . $template . '.php' );
	
	}else {
		include( PARENT_DIR . '/templates/shortcodes/icon.php' );
	}

	$output .= ob_get_clean();
	
	return $output;
}
add_shortcode( 'icon', 'rand_icon_shortcode' );

function rand_row_shortcode( $atts, $content = null ) {
	 $atts = shortcode_atts( array(
        'class' => 'container'    
    ), $atts, 'row' );
	
	return '<div class="' . $atts['class'] . '"><div class="row">' . do_shortcode($content) . '</div></div>';
}
add_shortcode( 'row', 'rand_row_shortcode' );

function rand_col_shortcode( $atts, $content = null ) {
	 $atts = shortcode_atts( array(
        'class' => 'col-xs-12',
		'id' => ''  
    ), $atts, 'col' );
	$class = (!empty($atts['class']))? ' class="' . $atts['class'] . '" ': '';
	$id = (!empty($atts['id']))? ' id="' . $atts['id'] . '"' : '';
	return '<div'. $id . $class . '>' . wpautop(do_shortcode($content)) . '</div>';
}
add_shortcode( 'col', 'rand_col_shortcode' );

function rand_header_shortcode( $atts ) {
	 $atts = shortcode_atts( array(
	 	'featured_image_bg' => 'yes',
		'title' => '',
		'custom_title' => '',
        'class' => ''    
    ), $atts, 'header' );
	
	$class = ( !empty( $atts['class'] ) )? 'class="post-header ' . $atts['class'] .'" ' : 'class="post-header" '; 	
	$background = '';
	if ( $atts['featured_image_bg'] !== 'no' && has_post_thumbnail() ):
		$thumbnail_id = get_post_thumbnail_id();
		$img = wp_get_attachment_image_src( $thumbnail_id, 'full' );
		$width = $img[1];
		$height = $img[2];
		$background .= 'style=" background-image: url(\'' . $img[0] . '\');"';
	endif;
	
	if ( $atts['title'] == 'yes' || $atts['title'] == true   ):
		$title = '<h1>' . get_the_title() . '</h1>';
	elseif( !empty($atts['custom_title']) ):
		$title = '<h1>' . $atts['custom_title'] . '</h1>';
	else:
		$title = '';
	endif;

	$output = '<header ' . $class . $background . '>' . $title .'</header>';
	
	return $output;
}
add_shortcode( 'header', 'rand_header_shortcode' );

function rand_google_map_shortcode( $atts ) {
	 $atts = shortcode_atts( array(
	 	'map_link' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3579.820069928052!2d-80.18710004934906!3d26.202530796526997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d903b9471d0207%3A0x7397a340cee7d96!2sRand+Internet+Marketing!5e0!3m2!1sen!2sus!4v1469134952045',  
    ), $atts, 'google_map' );

	$output = '<div class="embed-responsive embed-responsive-16by9"><iframe src="' . esc_url_raw($atts['map_link']) . '" class="embed-responsive-item" allowfullscreen></iframe></div>';
	
	return $output;
}
add_shortcode( 'google_map', 'rand_google_map_shortcode' );

$carousel_id=0;
function rand_post_carousel_shortcode( $atts ){
	global $carousel_id;
	$carousel_id++;
	
	$atts = shortcode_atts( array(
	 	'post_type' => 'post',
		'featured_only'	=> 'no',
		'limit'			=> 10,
		'left_icon_class' => 'fa fa-chevron-left',
		'right_icon_class' => 'fa fa-chevron-right',
		'bullet_icon_class' => 'fa fa-circle',
		'template' 			=> ''  
    ), $atts, 'post_carousel' );
	
	$post_type		= explode(',', $atts['post_type']);
	$limit			= $atts['limit'];
	$bullet_icon 	= $atts['right_icon_class'];
	$template		= !empty($atts['template'])		? '-' . $atts['template'] : '';
	
	// The wp_query arguments
	$query_args	= array(
		'post_type'		=> $post_type,
		'posts_per_page'		=> $limit,
	);
	
	if ( $atts['featured_only'] != 'no' ):
		$query_args['meta_query'] =	array(
										array(
											'key'     => '_featured_box_is_featured',
											'value'   => 'on',
										),
									);	
	endif;
	
	//The wp_query
	$posts	= new WP_Query( $query_args );
	
	$output = '';
	//var_dump( $trustees->post_count );
	//var_dump( $trustees );
	ob_start();

	// check if template has been overriden
	if ( file_exists( CHILD_DIR . '/templates/shortcodes/post-carousel' . $template . '.php' ) ) {
		
		include( CHILD_DIR . '/templates/shortcodes/post-carousel' . $template . '.php' );

	} else  {
		include( PARENT_DIR . '/templates/shortcodes/post-carousel.php' );
	}

	$output .= ob_get_clean();
	
	return $output;
}
add_shortcode( 'post_carousel', 'rand_post_carousel_shortcode');

function rand_post_grid_shortcode( $atts ){	
	$atts = shortcode_atts( array(
		'id'		=> '',
	 	'post_type' => 'post',
		'featured_only'	=> 'no',
		'limit'			=> 10,
		'class' 			=> '',
		'template' 			=> ''  
    ), $atts, 'post_grid' );
	
	$id				= !empty($atts['id'])			? ' id="' . $atts['id'] . '"' 	: '';
	$post_type		= explode(',', $atts['post_type']);
	$limit			= $atts['limit'];
	$template		= !empty($atts['template'])		? '-' . $atts['template'] 		: '';
	
	// The wp_query arguments
	$query_args	= array(
		'post_type'		=> $post_type,
		'posts_per_page'		=> $limit,
	);
	
	if ( $atts['featured_only'] != 'no' ):
		$query_args['meta_query'] =	array(
										array(
											'key'     => '_featured_box_is_featured',
											'value'   => 'on',
										),
									);	
	endif;
	
	//The wp_query
	$posts	= new WP_Query( $query_args );
	
	$output = '';
	//var_dump( $trustees->post_count );
	//var_dump( $trustees );
	ob_start();

	// check if template has been overriden
	if ( file_exists( CHILD_DIR . '/templates/shortcodes/post-grid' . $template . '.php' ) ) {
		
		include( CHILD_DIR . '/templates/shortcodes/post-grid' . $template . '.php' );

	} else  {
		include( PARENT_DIR . '/templates/shortcodes/post-grid.php' );
	}

	$output .= ob_get_clean();
	
	return $output;
}
add_shortcode( 'post_grid', 'rand_post_grid_shortcode');

function rand_no_texturize_shortcodes( $list ) {
  $list[] = 'row';
  $list[] = 'col';
  $list[] = 'header';
  $list[] = 'icon';
  return $list;
}
add_filter( 'no_texturize_shortcodes', 'rand_no_texturize_shortcodes' );
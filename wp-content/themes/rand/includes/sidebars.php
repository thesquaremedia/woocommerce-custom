<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
 * Enqueue scripts and stylessheets for sidebars when in the widgets.php page.
 * triggered by each of the sidebars button.
 */
add_action( 'admin_enqueue_scripts', 'rand_theme_dynamic_sidebars_scripts', 10, 1);
function rand_theme_dynamic_sidebars_scripts($hook){
	if ( $hook === 'widgets.php' ) :
		wp_enqueue_script( 'rand-theme-sidebar-js', PARENT_URI . '/js/sidebars.js', array( 'jquery' ) );
		wp_localize_script( 'rand-theme-sidebar-js', 'rand_theme_options', array(
			'ajaxurl'		=> admin_url( 'admin-ajax.php' ),
			'rand_theme_load_nonce'	=> wp_create_nonce( 'rand_theme_load_nonce' ),
		) );
		
		wp_enqueue_style( 'rand-theme-sidebar-css', PARENT_URI . '/css/sidebars.css' );
		
		return;
	endif;
}

/*
 * Add user created sidebar.
 * triggered by add sidebar button under the add a new sidebar section on the widgets area.
 */
add_action( 'wp_ajax_rand_theme_add_widget_area', 'rand_theme_add_widget_area' );
function rand_theme_add_widget_area(){
	if ( ! wp_verify_nonce( $_POST['rand_theme_load_nonce'], 'rand_theme_load_nonce' ) ) die(-1);
	$rand_theme_widgets = get_option( 'rand_theme_widgets' );
	$number = $rand_theme_widgets ? intval( $rand_theme_widgets['number'] ) + 1 : 1;
	$rand_theme_widgets['areas']['rand-theme-widget-area-' . $number] = sanitize_text_field( $_POST['rand_theme_widget_area_name'] );
	$rand_theme_widgets['number'] = $number;
	update_option( 'rand_theme_widgets', $rand_theme_widgets, true );
	printf( '<strong>%1$s</strong> widget area has been created. You can create more areas, once you finish update the page to see all the areas.',
		esc_html( $_POST['rand_theme_widget_area_name'] )
	);
	die();
}

/*
 * Remove user created sidebar.
 * triggered by each of the sidebars button.
 */
add_action( 'wp_ajax_rand_theme_remove_widget_area', 'rand_theme_remove_widget_area' );
function rand_theme_remove_widget_area(){
	if ( ! wp_verify_nonce( $_POST['rand_theme_load_nonce'], 'rand_theme_load_nonce' ) ) die(-1);
	$rand_theme_widgets = get_option( 'rand_theme_widgets' );
	unset( $rand_theme_widgets['areas'][$_POST['rand_theme_widget_area_name']] );
	update_option( 'rand_theme_widgets', $rand_theme_widgets, true );
	die( $_POST['rand_theme_widget_area_name'] );
}

/*
 * Register All User created sidebars
 */
$rand_theme_widgets = get_option( 'rand_theme_widgets' );
if ( $rand_theme_widgets['areas'] ) :
	foreach ( $rand_theme_widgets['areas'] as $id => $name ):
		register_sidebar( array(
			'name' => sanitize_text_field( $name ),
			'id' => sanitize_text_field( $id ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="widget-title">',
			'after_title' => '</h4>',
		) );
	endforeach;
endif;
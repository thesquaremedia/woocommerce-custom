<?php
if ( !defined( 'RAND_VERSION' ) ):
define( 'RAND_VERSION' , '1.0' );
define( 'PARENT_DIR', get_template_directory() );
define( 'PARENT_URI', get_template_directory_uri() );
define( 'CHILD_DIR', get_stylesheet_directory() );
define( 'CHILD_URI', get_stylesheet_directory_uri() );
define( 'RM_TEXT_DOMAIN', 'rand_marketing' );
endif;

if (!function_exists('write_log')) {
    function write_log ( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }
}

/* 
 * ====================================================== SECURITY ==================================================
 */
remove_action('wp_head', 'wp_generator'); //Wordpress
if ( class_exists( 'WooCommerce' ) ) {
  // code that requires WooCommerce
  remove_action( 'wp_head', 'wc_generator_tag' );
}
if ( function_exists( 'visual_composer' ) ) {
  // code that requires Visual Composer
  add_action('init', 'rand_override_vc_generator_tag', 100);
}
function rand_override_vc_generator_tag() {
    remove_action('wp_head', array(visual_composer(), 'addMetaData'));
}

/* 
 * ====================================================== FILE INCLUDES ==================================================
 */
 
require_once( PARENT_DIR . "/includes/scripts.php" );	//require widgets scripts file use to add widgets
require_once( PARENT_DIR . "/includes/login.php" );	//require login file
require_once( PARENT_DIR . "/includes/nav.php" );	//require navigation scripts
require_once( PARENT_DIR . '/includes/tiny-mce.php' ); //require tinymce scripts

/* 
 * ====================================================== Removing Menus =================================================
 */

//from admin menu
add_action( 'admin_menu', 'rand_theme_remove_admin_menus' ); 

function rand_theme_remove_admin_menus(){
    global $submenu;
        // Appearance Menu
        unset($submenu['themes.php'][6]); // Customize
}
//from menu bar 
add_action( 'wp_before_admin_bar_render', 'rand_before_admin_bar_render' ); 

function rand_before_admin_bar_render(){
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('customize');
}

/* 
 * ====================================================== Setting Up The Theme ==========================================
 */
function rand_theme_setup(){
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ));
	add_editor_style(); // Adds CSS to the editor to match the front end of the site.
	add_theme_support( 'automatic-feed-links' );
	
	/*
	 * Add support for title-tag
	 */
	add_theme_support( 'title-tag' );
	add_filter('document_title_separator' , 'rand_theme_title_separator');
	function rand_theme_title_separator( $sep ){
		//set new separator
		$sep = '|';
		return $sep;
	}
	/*
	 * Fallback method for title-tag
	 */
	if ( ! function_exists( '_wp_render_title_tag' ) ) :
		function rand_theme_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
		}
		add_action( 'wp_head', 'rand_theme_render_title' );
		add_filter( 'wp_title', 'rand_extend_wp_title', 10, 2 );
		function rand_extend_wp_title( $title ){
			global $page, $paged;
		
			// Add the blog name.
			$name = get_bloginfo( 'name' );
		
			// Add the blog description for the home/front page.
			$site_description = get_bloginfo( 'description', 'display' );
			if ( is_home() || is_front_page() ):
				if ( $site_description ):
					return "$name | $site_description";
				else:
					return "$name";
				endif;
			endif;
			// Add a page number if necessary:
			if ( $paged >= 2 || $page >= 2 )
				return '$name | ' . sprintf( __( 'Page %s', RM_TEXT_DOMAIN ), max( $paged, $page ) );
			
			return $title;
		}
	endif;
	
	/*
	 * Add action for widgets init used to control sidebars and widgets.
	 */
	add_action( 'widgets_init', 'rand_theme_widgets_init' );
	
	add_theme_support( 'post-thumbnails' ); /* ===== ADDS FEATURED IMAGE TO PAGES ===== */
	
	add_theme_support( 'woocommerce' );//Add woocommerce support.
	load_theme_textdomain( RM_TEXT_DOMAIN, get_template_directory() .'/languages' );
	
	//Add content width (desktop default)
	if ( ! isset( $content_width ) ) {
		$content_width = 768;
	}
	
	remove_filter('the_content', 'wpautop');
	
	//Add menu support and register main menu
	if ( function_exists( 'register_nav_menus' ) ) {
		register_nav_menus(
			array(
			  'main_menu' => 'Main Menu'
			)
		);
	}

	
	/**
	 * Bootstrap_Walker_Nav_Menu setup.
	 */
	// 
	add_action( 'init', 'register_menu' );

	function register_menu(){
		register_nav_menu( 'top-bar', 'Bootstrap Top Menu' ); 
	}

	class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {


		function start_lvl( &$output, $depth = 0, $args = array() ) {

			$indent = str_repeat( "\t", $depth );
			$output	   .= "\n$indent<ul class=\"dropdown-menu\">\n";

		}

		function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
			
			if (!is_object($args) || ( is_user_logged_in() && $item->hide_show_on_logged_in === 'hide') || ( !is_user_logged_in() && $item->hide_show_on_logged_in === 'show')) {
				return; // menu has not been configured
			}

			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

			$li_attributes = '';
			$class_names = $value = '';

			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = ($args->has_children) ? 'dropdown' : '';
			$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
			$classes[] = 'menu-item-' . $item->ID;


			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			$class_names = ' class="' . esc_attr( $class_names ) . '"';

			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

			$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
			$attributes .= ($args->has_children) 	    ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';
			
			$link_classes = !empty( $item->link_classes )? ' class="' . $item->link_classes . '"' : '';
			
			$item_output = $args->before;
			$item_output .= '<a'. $attributes . $link_classes . '>';
			if( !empty( $item->icon_classes ) ):
				$item_output .= '<i class="' . $item->icon_classes . '" aria-hidden="true"></i>';
			endif;
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ($args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
			$item_output .= $args->after;

			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}

		function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

			if ( !$element )
				return;

			$id_field = $this->db_fields['id'];

			//display this element
			if ( is_array( $args[0] ) )
				$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
			else if ( is_object( $args[0] ) )
				$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );

			$cb_args = array_merge( array(&$output, $element, $depth), $args);
			call_user_func_array(array(&$this, 'start_el'), $cb_args);

			$id = $element->$id_field;

			// descend only when the depth is right and there are childrens for this element
			if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

				foreach( $children_elements[ $id ] as $child ){

					if ( !isset($newlevel) ) {
						$newlevel = true;
						//start the child delimiter
						$cb_args = array_merge( array(&$output, $depth), $args);
						call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
					}
					$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
				}
					unset( $children_elements[ $id ] );
			}

			if ( isset($newlevel) && $newlevel ){
				//end the child delimiter
				$cb_args = array_merge( array(&$output, $depth), $args);
				call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
			}

			//end this element
			$cb_args = array_merge( array(&$output, $element, $depth), $args);
			call_user_func_array(array(&$this, 'end_el'), $cb_args);
		}
	}
	
	class Rand_Simple_Walker_Nav_Menu extends Walker_Nav_Menu {

		function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
			
			if (!is_object($args) || ( is_user_logged_in() && $item->hide_show_on_logged_in === 'hide') || ( !is_user_logged_in() && $item->hide_show_on_logged_in === 'show')) {
				return; // menu has not been configured
			}

			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

			$li_attributes = '';
			$class_names = $value = '';

			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = ($args->has_children) ? 'dropdown' : '';
			$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
			$classes[] = 'menu-item-' . $item->ID;


			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			$class_names = ' class="' . esc_attr( $class_names ) . '"';

			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

			$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
			$attributes .= ($args->has_children) 	    ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';
			
			$link_classes = !empty( $item->link_classes )? ' class="' . $item->link_classes . '"' : '';
			
			$item_output = $args->before;
			$item_output .= '<a'. $attributes . $link_classes . '>';
			if( !empty( $item->icon_classes ) ):
				$item_output .= '<i class="' . $item->icon_classes . '" aria-hidden="true"></i>';
			endif;
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ($args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
			$item_output .= $args->after;

			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}

		function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

			if ( !$element )
				return;

			$id_field = $this->db_fields['id'];

			//display this element
			if ( is_array( $args[0] ) )
				$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
			else if ( is_object( $args[0] ) )
				$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );

			$cb_args = array_merge( array(&$output, $element, $depth), $args);
			call_user_func_array(array(&$this, 'start_el'), $cb_args);

			$id = $element->$id_field;

			// descend only when the depth is right and there are childrens for this element
			if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

				foreach( $children_elements[ $id ] as $child ){

					if ( !isset($newlevel) ) {
						$newlevel = true;
						//start the child delimiter
						$cb_args = array_merge( array(&$output, $depth), $args);
						call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
					}
					$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
				}
					unset( $children_elements[ $id ] );
			}

			if ( isset($newlevel) && $newlevel ){
				//end the child delimiter
				$cb_args = array_merge( array(&$output, $depth), $args);
				call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
			}

			//end this element
			$cb_args = array_merge( array(&$output, $element, $depth), $args);
			call_user_func_array(array(&$this, 'end_el'), $cb_args);
		}
	}
}
add_action( 'after_setup_theme', 'rand_theme_setup' );

/* 
 * Rand theme rewrite rule for theme switch
 */
function rand_theme_rewrite_flush() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'rand_theme_rewrite_flush' );

/* 
 * =================================== CUZTOM ===================================
 */
include( PARENT_DIR . '/includes/cuztom/cuztom.php' );

/* 
 * =================================== CUSTOM POST TYPES, TAXONOMIES AND META ===================================
 */
function rand_post_types()
{
    // Post types
	$testimonial = new Cuztom_Post_Type( 
										'Testimonial', 
										array(
												'menu_icon'	=> 'dashicons-format-quote',
												'supports'	=> array( 'title','editor'),
											)
										);
}
add_action( 'init', 'rand_post_types' );

/* 
 * =================================== Options Framework ===================================
 */

/*
 * Loads the Options Panel
 *
 * If you're loading from a child theme use stylesheet_directory
 * instead of template_directory
 */
define( 'OPTIONS_FRAMEWORK_DIRECTORY', PARENT_URI . '/includes/options-framework/' );
require_once PARENT_DIR . '/includes/options-framework/options-framework.php';
// Loads options.php from child or parent theme
$optionsfile = locate_template( 'options.php' );
load_template( $optionsfile );

// Check for Options Framework Plugin
of_check();

function of_check()
{
  if ( !function_exists('of_get_option') )
  {
    wp_enqueue_script('thickbox', null, array('jquery'));
    add_action('admin_notices', 'of_check_notice');
  }
}

// The Admin Notice
function of_check_notice()
{
?>
  <div class='updated fade'>
    <p><?php _e('The Options Framework plugin is required for this theme to function properly.', RM_TEXT_DOMAIN); ?> <a href="<?php echo network_admin_url('plugin-install.php?tab=plugin-information&plugin=options-framework&TB_iframe=true&width=640&height=517'); ?>" class="thickbox onclick"><?php _e('Install now.', RM_TEXT_DOMAIN); ?></a></p>
  </div>
<?php
}

if ( !function_exists( 'of_get_option' ) ) {
function of_get_option($name, $default = 'false') {
	
	$optionsframework_settings = get_option('optionsframework');
	
	// Gets the unique option id
	$option_name = $optionsframework_settings['id'];
	
	if ( get_option($option_name) ) {
		$options = get_option($option_name);
	}
		
	if ( !empty($options[$name]) ) {
		return $options[$name];
	} else {
		return $default;
	}
}
}

/* Toggles options on and off on click */
 /*
 * This is an example of how to add custom scripts to the options panel.
 * This one shows/hides the an option when a checkbox is clicked.
 *
 * You can delete it if you not using that option
 */

add_action('optionsframework_custom_scripts', 'optionsframework_custom_scripts');
 
function optionsframework_custom_scripts() { ?>
 
<script type="text/javascript">
jQuery(document).ready(function() {
	function toggleField(id, field, fields){
		//console.log(id+' '+field+' '+fields)
		if (field != ''){
			if ( jQuery('#'+id).is(':checkbox:checked') ){
				field.show();
			}else{
				field.hide();
			}
		}else{
			if ( jQuery('#'+id).is(':checkbox:checked') ){
				jQuery('.section[data-dependency="'+fields+'"]').show(500);
			}else{
				jQuery('.section[data-dependency="'+fields+'"]').hide(500);
			}
		}
	}
	jQuery('.section[data-dependency]').each(function(){
		id		= jQuery(this).data('dependency');
		field	= jQuery(this);
		toggleField( id, field, '' )
	});
	jQuery('.has-dependents').on('click',function(){
		id		= jQuery(this).attr('id').replace('section-','');
		fields	= id;
		toggleField( id,'', fields )
	});		

});
</script>
 
<?php
}
 
/* Removes the code stripping */
 
add_action('admin_init','optionscheck_change_santiziation', 100);
 
function optionscheck_change_santiziation() {
	remove_filter( 'of_sanitize_textarea', 'of_sanitize_textarea' );
	add_filter( 'of_sanitize_textarea', 'of_sanitize_textarea_custom' );
	remove_filter( 'of_sanitize_upload', 'of_sanitize_upload' );
	add_filter( 'of_sanitize_upload', 'sanitize_text_field' );
}
 
function of_sanitize_textarea_custom($input) {
    global $allowedposttags;
        $of_custom_allowedtags["embed"] = array(
			"src" => array(),
			"type" => array(),
			"allowfullscreen" => array(),
			"allowscriptaccess" => array(),
			"height" => array(),
			"width" => array()
		);
		$of_custom_allowedtags["script"] = array(
			"type" => array()
		);
		$of_custom_allowedtags["iframe"] = array(
			"height" => array(),
			"width" => array(),
			"src" => array(),
			"frameborder" => array(),
			"allowfullscreen" => array()
		);
		$of_custom_allowedtags["object"] = array(
			"height" => array(),
			"width" => array()
		);
		$of_custom_allowedtags["param"] = array(
			"name" => array(),
			"value" => array()
		);
 
	$of_custom_allowedtags = array_merge($of_custom_allowedtags, $allowedposttags);
	$output = wp_kses( $input, $of_custom_allowedtags);
	return $output;
}


/**
	Add order column to pages
 **/

add_filter('manage_pages_columns', 'rand_theme_columns');
function rand_theme_columns($columns) {
    $columns['order'] = 'Order';
    return $columns;
}
add_action('manage_pages_custom_column',  'rand_theme_show_columns');
function rand_theme_show_columns($name) {
    global $post;
    switch ($name) {
        case 'order':
            $views = $post->menu_order;
            echo $views;
    }
}

/* =================================== Add Fancybox to linked Images =================================== */

/**
 * Attach a class to linked images' parent anchors
 * e.g. a img => a.img img
 */
function give_linked_images_class($html, $id, $caption, $title, $align, $url, $size, $alt = '' ){
	$classes = 'lightbox'; // separated by spaces, e.g. 'img image-link'

	// check if there are already classes assigned to the anchor
	if ( preg_match('/<a.*? class=".*?">/', $html) ) {
		$html = preg_replace('/(<a.*? class=".*?)(".*?>)/', '$1 ' . $classes . '$2', $html);
	} else {
		$html = preg_replace('/(<a.*?)>/', '$1 class="' . $classes . '" >', $html);
	}
	return $html;
}
add_filter('image_send_to_editor','give_linked_images_class',10,8);

/* 
 * ====================================================== WIDGETS ======================================================
 */
 
require_once( PARENT_DIR . "/includes/sidebars.php");	//require sidebars script that adds the ability to add sidebars from the admin.
require_once( PARENT_DIR . "/includes/widgets.php");	//require widgets script use to add widgets

// Widgets Init Callback function used to control sidebars and widgets
function rand_theme_widgets_init() {
	if ( function_exists('register_sidebar') ) {
		register_sidebar(array(
			'name'          => __( 'Header Sidebar', RM_TEXT_DOMAIN ),
			'id'			=> 'header-sidebar',
		    'before_widget' => '<div id="%1$s" class="widget %2$s">',
		    'after_widget' 	=> '</div>',
		    'before_title' 	=> '<h4>',
		    'after_title' 	=> '</h4>',
		 ));
		register_sidebar(array(
			'name'          => __( 'Main Sidebar', RM_TEXT_DOMAIN ),
			'id'			=> 'blog-sidebar',
		    'before_widget' => '<div id="%1$s" class="widget %2$s">',
		    'after_widget' 	=> '</div>',
		    'before_title' 	=> '<h4>',
		    'after_title' 	=> '</h4>',
		 ));
		 register_sidebar(array(
			'name'          => __( 'Footer Sidebar', RM_TEXT_DOMAIN ),
			'id'			=> 'footer-sidebar',
		    'before_widget' => '<div id="%1$s" class="widget %2$s">',
		    'after_widget' 	=> '</div>',
		    'before_title' 	=> '<h4>',
		    'after_title' 	=> '</h4>',
		 ));
		 register_sidebar(array(
			'name'          => __( 'Footer Bottom Sidebar', RM_TEXT_DOMAIN ),
			'id'			=> 'footer-bottom-sidebar',
		    'before_widget' => '<div id="%1$s" class="widget %2$s">',
		    'after_widget' 	=> '</div>',
		    'before_title' 	=> '<h4>',
		    'after_title' 	=> '</h4>',
		 ));
	}
}

/* ========================================= Theme Images ========================================= */

//add_image_size( '', 2000, 720, false ); 
function wp_get_attachment( $attachment_id ) {

	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}

/* 
 * ====================================================== GRAVITY FORMS ===============================================
 */
add_filter('gform_submit_button', 'form_submit_button', 10, 2);
function form_submit_button($button, $form){
    return "<button class='button btn' id='gform_submit_button_{$form["id"]}'><span>{$form['button']['text']}</span></button>";
}

/* 
 * ====================================================== WOOCOMMERCE =================================================
 */
 
/**
* is_woocommerce_activated function to check if woocommerce is activated.
*
* @access public
* @return bool
*/
if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	function is_woocommerce_activated() {
		if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
	}
}

/**
* is_realy_woocommerce_page - Returns true if on a page which uses WooCommerce templates (cart and checkout are standard pages with shortcodes and which are also included)
*
* @access public
* @return bool
*/
function is_realy_woocommerce_page ( $postID ) {
        if(  function_exists ( "is_woocommerce_activated" ) && is_woocommerce_activated()){
                //return true;
        $return = false;
        $woocommerce_keys   =   array ( "woocommerce_shop_page_id" ,
                                        "woocommerce_terms_page_id" ,
                                        "woocommerce_cart_page_id" ,
                                        "woocommerce_checkout_page_id" ,
                                        "woocommerce_pay_page_id" ,
                                        "woocommerce_thanks_page_id" ,
                                        "woocommerce_myaccount_page_id" ,
                                        "woocommerce_edit_address_page_id" ,
                                        "woocommerce_view_order_page_id" ,
                                        "woocommerce_change_password_page_id" ,
                                        "woocommerce_logout_page_id" ,
                                        "woocommerce_lost_password_page_id" ) ;
        foreach ( $woocommerce_keys as $wc_page_id ) {
                if ( $postID == get_option ( $wc_page_id , 0 ) ) {
                        $return = true ;
                }
        }
        return $return;
	}
}

/* 
 * ====================================================== DUPLICATE POSTS FUNC =========================================
 */

/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function rand_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}
 
	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);
	/*
	 * and all the original post data then
	 */
	$post = get_post( $post_id );
 
	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {
 
		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );
 
		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/*
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 
 
		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rand_duplicate_post_as_draft' );
 
/*
 * Add the duplicate link to action list for post_row_actions
 */
function rand_duplicate_post_link( $actions, $post ) {
	if ( current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}
 
add_filter( 'post_row_actions', 'rand_duplicate_post_link', 10, 2 );
add_filter( 'page_row_actions', 'rand_duplicate_post_link', 10, 2 );

/* 
 * ====================================================== NAV VARIABLES =========================================
 */

function rand_dynamic_menu_items( $menu_items ) {

    foreach ( $menu_items as $menu_item ) {

        if ( strpos( $menu_item->url , '#site_url#' ) !== false ) {
			
			$url = str_replace("#site_url#", '', $menu_item->url );
            $menu_item->url = get_home_url().$url;
        }
    }

    return $menu_items;
}
add_filter( 'wp_nav_menu_objects', 'rand_dynamic_menu_items' );

/* 
 * ====================================================== SHORTCODES =========================================
 */
 
require_once( PARENT_DIR . "/includes/shortcodes.php" );	//require shortcodes file

/* 
 * ====================================================== VISUAL COMPOSER =========================================
 */
 
if ( is_plugin_active( 'js_composer/js_composer.php' ) ) :
	require_once( PARENT_DIR . "/includes/js_composer.php" );	//require shortcodes file
endif;
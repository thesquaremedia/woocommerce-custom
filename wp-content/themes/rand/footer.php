</main><!-- .site-main -->
<footer>
	<?php if ( is_active_sidebar( 'footer-sidebar' )  ) : ?>
	<aside id="footer-sidebar" class="sidebar widget-area container" role="complementary">
		<?php dynamic_sidebar( 'footer-sidebar' ); ?>
	</aside>
	<!-- .sidebar .widget-area -->
	<?php endif; ?>
	<?php if ( is_active_sidebar( 'footer-bottom-sidebar' )  ) : ?>
	<aside id="footer-bottom-sidebar" class="sidebar widget-area container-fluid" role="complementary">
		<?php dynamic_sidebar( 'footer-bottom-sidebar' ); ?>
	</aside>
	<!-- .sidebar .widget-area -->
	<?php endif; ?>
</footer>
<div id="bottom-fill" class="container-fluid"></div>
</div>
<!-- close wrapper -->
<?php wp_footer(); ?>
</body></html>
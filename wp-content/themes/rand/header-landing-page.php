<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php wp_head(); ?>
</head>

<body <?php body_class(isset($class) ? $class : ''); ?>>
<?php
if ( of_get_option('back_to_top') == '1' ):
?>
<a name="top" rel="nofollow"></a>
<?php
endif;
?>
<div id="wrapper">
<main id="main" class="site-main" role="main">

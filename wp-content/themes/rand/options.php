<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 * 
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet (lowercase and without spaces)
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "", strtolower($themename) );
	
	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = $themename;
	update_option('optionsframework', $optionsframework_settings);
	
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the "id" fields, make sure to use all lowercase and no spaces.
 *  
 */

function optionsframework_options() {

	$heading_fonts = array("bitter" => "Bitter","droidsans" => "Droid Sans","franchise" => "Franchise","marketingscript" => "Marketing Script","museo" => "Museo Slab","rokkitt" => "Rokkitt");
	
	// Pull all the categories into an array
	$options_categories = array();  
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
    	$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all the pages into an array
	$options_pages = array();  
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	//$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
    	$options_pages[$page->ID] = $page->post_title;
	}
	
	// Test data
	$effect_array = array(
		'slide' => __('Slide', RM_TEXT_DOMAIN),
		'fade' => __('Fade', RM_TEXT_DOMAIN)
	);
	$text_transform = array(
	'uppercase' => 'Uppercase',
	'lowercase' => 'Lowercase',
	'capitalize' => 'Capitalize',
	'inherit' => 'Inherit',
	'none' => 'None'
	);
	
	// Background Options
	$background_repeat_options = array(
		'repeat'	=> 'repeat',
		'repeat-x'	=> 'repeat-x',
		'repeat-y'	=> 'repeat-y',
		'no-repeat'	=> 'no-repeat',
		'initial'	=> 'initial',
		'inherit'	=> 'inherit',
	);
	$background_attachment_options = array(
		'scroll'	=> 'scroll',
		'fixed'		=> 'fixed',
		'local'		=> 'local',
		'initial'	=> 'initial',
		'inherit'	=> 'inherit',
	);
	
	$imagepath =  get_template_directory_uri() . '/images/';
		
	$options = array();
		
	$options[] = array( "name" =>  __('Branding', RM_TEXT_DOMAIN),
						"type" => "heading");
						
						/*$options[] = array( "name" => __('Logo Retina Display', RM_TEXT_DOMAIN),
						"desc" => __('Checking this box will display your logo at half size, making it look crisp on iDevices. Make your logo twice as big as you want it to display if you check this box.', RM_TEXT_DOMAIN),
						"id" => "retina_logo",
						"std" => "0",
						"type" => "checkbox");*/
						
		$options[] = array( "name" => __('Logo', RM_TEXT_DOMAIN),
							"desc" => __('Upload your logo.', RM_TEXT_DOMAIN),
							"id" => "logo",
							"type" => "upload");
		
		$options[] = array( "name" => __('Sticky Menu Logo', RM_TEXT_DOMAIN),
							"desc" => __('Upload your logo for the sticky menu.', RM_TEXT_DOMAIN),
							"id" => "logo-sticky",
							"type" => "upload");
		
		$options[] = array( "name" => __('Alternate Logo', RM_TEXT_DOMAIN),
							"desc" => __('Upload your logo for footer.', RM_TEXT_DOMAIN),
							"id" => "logo-alt",
							"type" => "upload");
							
		$options[] = array( "name" =>  __('Favicon', RM_TEXT_DOMAIN),
							"desc" =>  __('The Favicon is the little 16x16 icon that appears next to your URL in the browser. It is not required, but recommended.', RM_TEXT_DOMAIN),
							"id" => "favicon",
							"type" => "upload");
							
		$wp_editor_settings = array(
			'wpautop' => true, // Default
			'textarea_rows' => 5,
			'tinymce' => true
		);
	
	$options[] = array( "name" =>  __('Header', RM_TEXT_DOMAIN),
						"type" => "heading");
	
				
		$options[] = array( "name" =>  __('Sticky Menu', RM_TEXT_DOMAIN),
							"desc" =>  __('Would you like the menu to stick to the top of the browser as the user scrolls down the site?', RM_TEXT_DOMAIN),
							"id" => "sticky_header",
							"std" => "0",
							"type" => "radio",
							"options" => array("1" => "Yes","0" => "No"));
								
		$options[] = array( "name" =>  __('Full Width Menu', RM_TEXT_DOMAIN),
							"desc" =>  __('Would you like the menu to be centered or stretched accros the width of the page', RM_TEXT_DOMAIN),
							"id" => "full_width_header",
							"std" => "0",
							"type" => "radio",
							"options" => array("1" => "Yes","0" => "No"));	
							
	if(  function_exists ( "is_woocommerce_activated" ) && is_woocommerce_activated()){
		$options[] = array( "name" =>  __('WooCommerce Menu', RM_TEXT_DOMAIN),
							"type" => "subheading");
						
			$options[] = array(
				'name' => __( 'Show WooComerce Menu My Account Link', RM_TEXT_DOMAIN ),
				'desc' => __( 'check if you want to show the My Account Link', RM_TEXT_DOMAIN ),
				'id' => 'woo_myaccount_checkbox',
				'std' => '1',
				'type' => 'checkbox'
			);
			$options[] = array(
				'name' => __( 'Show WooComerce Menu Cart Link', RM_TEXT_DOMAIN ),
				'desc' => __( 'check if you want to show the Cart Link', RM_TEXT_DOMAIN ),
				'id' => 'woo_cart_checkbox',
				'std' => '1',
				'type' => 'checkbox'
			);
			$options[] = array(
				'name' => __( 'Show WooComerce Menu Cart Link Total Items', RM_TEXT_DOMAIN ),
				'desc' => __( 'check oif you want to show the total number of items in the cart link', RM_TEXT_DOMAIN ),
				'id' => 'woo_cart_items_checkbox',
				'std' => '1',
				'type' => 'checkbox'
			);
			$options[] = array(
				'name' => __( 'Show WooComerce Menu Cart Link Total Ammount', RM_TEXT_DOMAIN ),
				'desc' => __( 'check if you want to show the the total amount in dollars on the cart link', RM_TEXT_DOMAIN ),
				'id' => 'woo_cart_total_checkbox',
				'std' => '0',
				'type' => 'checkbox'
			);					
	
		$options[] = array( "name" =>  __('WooCommerce Menu End', RM_TEXT_DOMAIN),
							"type" => "subheadingend");
	}
	
	$options[] = array( "name" =>  __('Home Page', RM_TEXT_DOMAIN),
						"type" => "heading");
						
		$options[] = array(
			'name'	=> __( 'Use One Page Layout', RM_TEXT_DOMAIN ),
			'desc'	=> __( 'Allow homepage to display with a one page layout', RM_TEXT_DOMAIN ),
			'id'	=> 'one_page_layout',
			'std'	=> '0',
			'class'	=> 'has-dependents',
			'type'	=> 'checkbox',
		);
		$options[] = array(
			'name' => __( 'Select a Page', RM_TEXT_DOMAIN ),
			'desc' => __( 'Passed an pages with ID and post_title', RM_TEXT_DOMAIN ),
			'id' => 'home_pages',
			'type' => 'multicheck',
			'options' => $options_pages,
			'dependency'	=> 'one_page_layout'
		);
						
	$options[] = array( "name" =>  __('Lightbox', RM_TEXT_DOMAIN),
						"type" => "heading");
						
		$options[] = array( "name" =>  __('Lightbox', RM_TEXT_DOMAIN),
							"desc" =>  __('Choose which lightbox jQuery plugin to use for the theme.', RM_TEXT_DOMAIN),
							"id" => "lightbox",
							"std" => "fancybox",
							"type" => "radio",
							"options" => array("fancybox" => __('Fancybox', RM_TEXT_DOMAIN),"tosrus" => __('Touch Optimize Sliders "R" Us', RM_TEXT_DOMAIN)));
										
	$options[] = array( "name" => __('Social Networks', RM_TEXT_DOMAIN),
						"type" => "heading");
	
		$options[] = array( "name" => __('Instagram', RM_TEXT_DOMAIN),
							"desc" => __('Enter the URL to your Instagram Profile.', RM_TEXT_DOMAIN),
							"id" => "instagram",
							"type" => "text");	
		$options[] = array( "name" => __('Facebook', RM_TEXT_DOMAIN),
							"desc" => __('Enter the URL to your Facebook profile.', RM_TEXT_DOMAIN),
							"id" => "facebook",
							"type" => "text");
		
		
		$options[] = array( "name" => __('YouTube', RM_TEXT_DOMAIN),
							"desc" => __('Enter the URL to your YouTube Channel.', RM_TEXT_DOMAIN),
							"id" => "youtube",
							"type" => "text");			
		$options[] = array( "name" => __('Twitter', RM_TEXT_DOMAIN),
							"desc" => __('Enter the URL to your Twitter profile.', RM_TEXT_DOMAIN),
							"id" => "twitter",
							"type" => "text"
							); 
		$options = apply_filters( 'rand_social_icons_options', $options);
		
	$options[] = array( "name" => __('Footer Settings', RM_TEXT_DOMAIN),
						"type" => "heading");
		$options[] = array(
			'name'	=> __( 'Use Back to Top', RM_TEXT_DOMAIN ),
			'desc'	=> __( 'display back to top link', RM_TEXT_DOMAIN ),
			'id'	=> 'back_to_top',
			'std'	=> '1',
			'type'	=> 'checkbox',
		);
	$options[] = array( "name" => __('Login Page', RM_TEXT_DOMAIN),
						"type" => "heading");
						
						
		$options[] = array( "name" => __('Login Logo', RM_TEXT_DOMAIN),
			"desc" => __('Upload your logo for the login page.', RM_TEXT_DOMAIN),
			"id" => "login_logo",
			"type" => "upload"
		);
				
						
		$options[] = array( "name" =>  __('Body Settings', RM_TEXT_DOMAIN),
			"type" => "subheading");
			
			$options[] = array(
				'name' => __( 'Background Image', RM_TEXT_DOMAIN ),
				'desc' => __( 'Select an image for the body background.', RM_TEXT_DOMAIN ),
				'id' => 'login_body_background_image',
				'std' => '',
				'type' => 'upload'
			);
			
			$options[] = array(
				'name' => __( 'Background Repeat', RM_TEXT_DOMAIN ),
				'desc' => __( 'Background repeat setting.', RM_TEXT_DOMAIN ),
				'id' => 'login_body_background_repeat',
				'std' => '',
				'type' => 'select',
				'options' => $background_repeat_options
			);
			
			$options[] = array(
				'name' => __( 'Background Attachment', RM_TEXT_DOMAIN ),
				'desc' => __( 'Background attachement setting.', RM_TEXT_DOMAIN ),
				'id' => 'login_body_background_attachment',
				'std' => '',
				'type' => 'select',
				'options' => $background_attachment_options
			);
			
			$options[] = array(
				'name' => __( 'Background Position X', RM_TEXT_DOMAIN ),
				'desc' => __( 'Background repeat setting.', RM_TEXT_DOMAIN ),
				'id' => 'login_body_background_position_x',
				'std' => '',
				'type' => 'text',
				'class'	=> 'mini box two'
			);
			
			$options[] = array(
				'name' => __( 'Background Position Y', RM_TEXT_DOMAIN ),
				'desc' => __( 'Background repeat setting.', RM_TEXT_DOMAIN ),
				'id' => 'login_body_background_position_y',
				'std' => '',
				'type' => 'text',
				'class'	=> 'mini box two'
			);
			
		$options[] = array( "name" =>  __('Body Settings', RM_TEXT_DOMAIN),
			"type" => "subheadingend");
						
		/*$options[] = array( "name" =>  __('Box Settings', RM_TEXT_DOMAIN),
			"type" => "subheading");
		
			
			$options[] = array(
				'name' => __( 'Background Color', RM_TEXT_DOMAIN ),
				'desc' => __( 'No color selected by default.', RM_TEXT_DOMAIN ),
				'id' => 'login_box_background_color',
				'std' => '',
				'type' => 'color'
			);
			
		$options[] = array( "name" =>  __('Box Settings', RM_TEXT_DOMAIN),
			"type" => "subheadingend");*/
			
		
						
	$options = apply_filters( 'rand_before_support_options', $options);
				
	// Support
	$options[] = array( "name" => __('Support', RM_TEXT_DOMAIN),
						"type" => "heading");					
						
	$options[] = array( "name" => __('Theme Documentation & Support', RM_TEXT_DOMAIN),
						"desc" => "<p class='dc-text'>Theme support and documentation is available for all clients. Visit the <a target='blank' href='http://randmarketing.com'>Rand Marketing Support</a> to get started.</p>
						
						
						
						",
						"type" => "info");	
				
	return $options;
}
<?php
if ( !empty($content) ):
	$output .= '<div class="icon-box' . $style . '">' . $icon . '<div class="icon-box-content">' . $inner_icon . wpautop($content) . '</div></div>';
else:
	$output .= $icon;
endif;
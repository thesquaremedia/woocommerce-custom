<?php
$output		= '<div class="carousel-holder"><div class="carousel-prev" id="carousel-'. $carousel_id .'-prev"><div class="carousel-btn-holder"><div class="carousel-btn-inner"><i class="' . $atts['left_icon_class'] . '"></i></div></div></div><div class="owl-carousel" id="carousel-'. $carousel_id .'">';
// The Loop
while ( $posts->have_posts() ) {
	$posts->the_post();
	
	$image = '';
	if( has_post_thumbnail() ):
		$image			=  get_the_post_thumbnail(get_the_ID(), 'full') ;
	endif;
	
	$link 		= get_the_permalink();
	$title 		= get_the_title();
	$content	= get_the_content();
	
	//var_dump( $image );
	$output 	.= '<div class="owl-item-holder"><div class="owl-item-inner">' . $image . $content . '</div></div>';
}
/* Restore original Post Data 
 * NB: Because we are using new WP_Query we aren't stomping on the 
 * original $wp_query and it does not need to be reset with 
 * wp_reset_query(). We just need to set the post data back up with
 * wp_reset_postdata().
 */
wp_reset_postdata();
$output .= '</div><div class="carousel-next" id="carousel-'. $carousel_id .'-next"><div class="carousel-btn-holder"><div class="carousel-btn-inner"><i class="' . $atts['right_icon_class'] . '"></i></div></div></div><div class="clearfix"></div></div>';
wp_enqueue_script('owl-carousel');
wp_enqueue_style('owl-carousel');
$output	.= "
<script>
	jQuery(document).ready(function($){
		var owl$carousel_id = jQuery('#carousel-$carousel_id.owl-carousel');
		jQuery(window).load(function($){
			
			owl$carousel_id.owlCarousel({
				onResize : resetSizeforCarousel$carousel_id,
				onResized : updateSizeforCarousel$carousel_id,
				onInitialized : updateSizeforCarousel$carousel_id,
				loop:true,
				margin:10,
				stagePadding: 0,
				center: false,
				autoplay: true,
				nav:false,
				dots:false,
				dotsEach:false,
				autoWidth: false,
				autoHeight: false,
				responsiveClass:true,
				//responsiveBaseElement: '#carousel-$carousel_id',
				responsive:{
					0:{
						items:1,
					},
					768:{
						items:2,
					},
					992:{
						items:4,
					}
				}
			});
		});
		
		function resetSizeforCarousel$carousel_id(){
			//console.log('reset size function');
			jQuery('#carousel-$carousel_id .owl-item ').css({height: 'auto'});
			jQuery('#carousel-$carousel_id .owl-item .owl-item-holder').css({height: 'auto'});
		}
		function updateSizeforCarousel$carousel_id(){
			console.log('update size function');
			jQuery('#carousel-$carousel_id .owl-dot span').append('<i class=\"$bullet_icon\" aria-hidden=\"true\"></i>');
			var minHeight=parseInt(jQuery('#carousel-$carousel_id .owl-stage').eq(0).css('height'));
			
			//console.log(minHeight+' '+minWidth);
			jQuery('#carousel-$carousel_id .owl-item ').css({height:minHeight+'px'});
			jQuery('#carousel-$carousel_id .owl-item .owl-item-holder').css({height: '100%'});
		}
		jQuery('#carousel-$carousel_id-next').click(function() {
			owl$carousel_id.trigger('next.owl.carousel');
		})
		// Go to the previous item
		jQuery('#carousel-$carousel_id-prev').click(function() {
			owl$carousel_id.trigger('prev.owl.carousel');
		})
	});
</script>
";
<?php
$absolute_path = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
$wp_load = $absolute_path[0] . 'wp-load.php';
require_once($wp_load);

/**
Do stuff like connect to WP database and grab user set values
*/
header('Content-type: text/css');
header('Cache-control: must-revalidate');
require('../includes/options-framework/options-framework.php');
?>
@charset "UTF-8";
<!-- Custom CSS -->
html{}
html,body{ height: auto; min-height:100%;}
html{ 
background-color: #427ec6;
	<?php if( !empty( of_get_option('login_body_background_image') ) && of_get_option('login_body_background_image') != '' ) : ?> background-image: url("<?php echo wp_get_attachment_url( of_get_option('login_body_background_image') ); ?>");<?php endif; ?>
	background-repeat: no-repeat;
	background-position: center center;
	-moz-box-shadow: inset 0 0 100px rgba(0, 0, 0, .5);
	-webkit-box-shadow: inset 0 0 100px rgba(0, 0, 0, .5);
	box-shadow: inset 0 0 100px rgba(0, 0, 0, .5);}
body.login {
	background-color: transparent;		
}
body.interim-login{ height:100%;}
body.login .g-recaptcha{ clear:both; text-align:left;}
body.login .g-recaptcha div{ display:inline-block; }
body.login div#login {
	width: 35%;
	padding-bottom: 30px;
}
body.login div#login:after{ display:block; content:''; clear:both;}
body.interim-login div#login{ margin: 0px auto 20px; }
body.login div#login h1 {
	width: 90%;
	margin: 0 auto;
	display: block;
	padding-bottom: 4%;
}
body.login div#login h1 a {
	<?php if( of_get_option('login_logo') != '' ): ?> background-image: url("<?php echo wp_get_attachment_url( of_get_option('login_logo') ); ?>");
	<?php list($width, $height) = getimagesize( wp_get_attachment_url( of_get_option('login_logo') ) ) ; ?> width: 100%; height: <?php echo $height; ?>px; 
	background-size: contain;
	background-position: center bottom;
	<?php endif; ?>
	margin: 0 auto; 
}
body.login div#login_error,
body.login .message{
	-webkit-box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, .5);
	-moz-box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, .5);
	box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, .5); 
	z-index:1;
}
body.login div#login form#loginform,
body.login div#login form#lostpasswordform,
body.login div#login form#registerform {
	background-color: rgba(255, 255, 255, 0.5);
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	border-radius: 0px;
	-webkit-box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, .5);
	-moz-box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, .5);
	box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, .5);
	
	z-index: 0;
	margin-top: 5%;
	border: solid 1px #CCC;
}
body.login div#login form#loginform p,
body.login div#login form#registerform p { color:#000; }
body.login div#login form#registerform p { float:none; width: 100%; }
body.login div#login form#loginform p label,
body.login div#login form#lostpasswordform p label,
body.login div#login form#registerform p label { color:#000; font-weight: bold;}
body.login div#login form#loginform input,
body.login div#login form#loginform input {}
body.login div#login form#loginform input#user_login {  }
body.login div#login form#loginform input#user_pass {}
body.login div#login form#loginform p.forgetmenot {}
body.login div#login form#loginform p.forgetmenot input#rememberme {}
body.login div#login form#loginform p.submit {}
body.login div#login form#loginform p.submit input#wp-submit,
body.login div#login form#lostpasswordform p.submit input#wp-submit,
body.login div#login form#registerform p.submit input#wp-submit { 
	background-color: #999;
	color:#000;
	text-shadow: 0px 0px #000;
	font-weight: bolder;
	
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	-webkit-box-shadow: 0 0 0px 0px rgba(0, 0, 0, .5);
	-moz-box-shadow: 0 0 0px 0px rgba(0, 0, 0, .5);
	box-shadow: 0 0 0px 0px rgba(0, 0, 0, .5);
	
	border: solid 1px #999;
}
body.login div#login form#loginform p.submit input#wp-submit:hover,
body.login div#login form#lostpasswordform p.submit input#wp-submit:hover,
body.login div#login form#registerform p.submit input#wp-submit:hover { 
	background-color: #666;
	color:#000;
	
	border: solid 1px #666;
}
body.login div#login p#nav {float:right; width: 50%; margin: 24px 0px 0px; padding:0; text-align:right;}
body.login div#login p#nav a {color:#000; display:inline-block;}
body.login div#login p#backtoblog {float:left; width: 50%; margin: 24px 0px 0px; padding:0; text-align:left;}
body.login div#login p#backtoblog a { color:#000; display:inline-block;}
/* Extra small devices (phones, less than 768px) */
/* Small devices (tablets, 768px and up) */
@media (max-width: 992px) {
	body.login div#login {
		width: 50%;
	}
}
/* No media query since this is the default in Bootstrap */
@media (max-width: 768px) {
	body.login div#login {
		width: 75%;
	}
}
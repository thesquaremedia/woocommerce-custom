<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php wp_head(); ?>
</head>

<body <?php body_class(isset($class) ? $class : ''); ?>>
<?php
if ( of_get_option('back_to_top') == '1' ):
?>
<a name="top" rel="nofollow"></a>
<?php
endif;
?>
<div id="wrapper">
<header>
	<?php
		$container_class =  of_get_option( 'full_width_header' ) == '1' ? ' container-fluid' : ' container';
		?>
	<nav class="navbar navbar-default<?php echo $container_class; ?>" role="navigation"> 
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header col-xs-12 col-sm-4">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
			<a class="navbar-brand col-xs-3 col-sm-12" href="<?php echo home_url(); ?>">
			<?php 
				if( of_get_option( 'logo' ) ):
					$logo_id	= of_get_option( 'logo' );
					$size		= 'full';
					echo wp_get_attachment_image( $logo_id, $size, false, array( 'class' => "attachment-$size size-$size img-responsive logo" ) ); 
				else:	
					echo bloginfo( 'name' ); 
				endif;
			?>
			</a> </div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="nav-content col-xs-12 col-sm-8">
			<div class="display-table">
				<div class="display-table-cell">
					<div class="collapse navbar-collapse">
						<?php if ( is_active_sidebar( 'header-sidebar' )  ) : ?>
						<aside id="header-sidebar" class="sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'header-sidebar' ); ?>
						</aside>
						<!-- .sidebar .widget-area -->
						<?php endif; ?>
						<?php wp_nav_menu( array('theme_location' => 'main_menu', 'menu_class' => 'nav navbar-nav navbar-right', 'depth'=> 3, 'container'=> false, 'walker'=> new Bootstrap_Walker_Nav_Menu)); ?>
					</div>
					<!-- /.navbar-collapse --> 
				</div>
			</div>
		</div>
	</nav>
</header>

<main id="main" class="site-main" role="main">

// JavaScript Document
( function($) {

    $( document ).ready( function() {
        $( '.widget-liquid-right' ).append( '<div id="rand-theme-widget-area-create"><h2>Add A New Sidebar</h2><div class="sidebar-description"><p class="description">Here you can create new widget areas for use in the Sidebar module.</p></div><p><label for="rand-theme-new-widget-area-name">Sidebar Name</label><input id="rand-theme-new-widget-area-name" class="widefat" type="text" value="" /></p><p class="rand-theme-widget-area-result"></p><button class="button button-primary rand-theme-create-widget-area right">Create</button><div class="clear"></div></div>' );

        var $create_box = $( '#rand-theme-widget-area-create' ),
            $widget_name_input = $create_box.find( '#rand-theme-new-widget-area-name' ),
            $square_sidebars = $( 'div[id^=rand-theme-widget-area-]' );

        $create_box.find( '.rand-theme-create-widget-area' ).click( function( event ) {
            console.log($widget_name_input.val()+' '+square_options.ajaxurl+square_options.square_load_nonce);
			var $this_el = $(this);

            event.preventDefault();

            if ( $widget_name_input.val() === '' ) return;

            $.ajax( {
                type: "POST",
                url: square_options.ajaxurl,
                data:
                {
                    action : 'square_add_widget_area',
                    square_load_nonce : square_options.square_load_nonce,
                    square_widget_area_name : $widget_name_input.val()
                },
                success: function( data ){
					console.log(data);
                    $this_el.siblings( '.rand-theme-widget-area-result' ).hide().html( data ).slideToggle();
                }
            } );
        } );

        $square_sidebars.each( function() {
            if ( $(this).is( '#rand-theme-widget-area-create' ) || $(this).closest( '.inactive-sidebar' ).length ) return true;

            $(this).closest('.widgets-holder-wrap').find('.sidebar-name h2').before( '<a href="#" class="button rand-theme-widget-area-remove left">Delete</a>' );

            $( '.rand-theme-widget-area-remove' ).click( function( event ) {
                var $this_el = $(this);

                event.preventDefault();

                $.ajax( {
                    type: "POST",
                    url: square_options.ajaxurl,
                    data:
                    {
                        action : 'square_remove_widget_area',
                        square_load_nonce : square_options.square_load_nonce,
                        square_widget_area_name : $this_el.closest( '.widgets-holder-wrap' ).find( 'div[id^=rand-theme-widget-area-]' ).attr( 'id' )
                    },
                    success: function( data ){
                        $( '#' + data ).closest( '.widgets-holder-wrap' ).remove();
                    }
                } );
            } );
        } );
    } );

} )(jQuery);
// JavaScript Document
jQuery(document).ready(function($) {  
	$("select.enhanced, .enhanced-select select").select2({
		placeholder: "Select an Option",
		minimumResultsForSearch: 5,
		allowClear: true
	});
	$("select.enhanced-no-clear, .enhanced-select-no-clear select").select2({
		placeholder: "Select an Option",
		minimumResultsForSearch: 5,
		allowClear: false
	});
});
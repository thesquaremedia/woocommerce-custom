// JavaScript Document
(function() {
"use strict";
	// Register plugin
	tinymce.PluginManager.add('rand_shortcode_select', function( editor, url ){
		editor.addButton( 'rand_shortcode_select', {
			type: 'listbox',
			text: 'Shortcodes',
			icon: false,
			values: [
						{
							text: 'Box Button',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Insert Box Button',
									body: [
											{
												type: 'textbox',
												name: 'post',
												label: 'Post/Page',
												value: ''
											},
											{
												type: 'textbox',
												name: 'text',
												label: 'Text',
												value: ''
											},
											{
												type: 'checkbox',
												name: 'target',
												label: 'Open in a new window?',
												value: '_blank'
											},
											{
												type: 'textbox',
												name: 'rel',
												label: 'Rel',
												value: ''
											},
											{
												type: 'textbox',
												name: 'class',
												label: 'Class',
												value: ''
											}
										],
										onsubmit: function( e ) {
											var postVal, textVal, targetVal, relVal, classVal, shortcode;
											
											postVal		= e.data.post 				? ' post=' + e.data.post			: '';
											textVal 	= e.data.text 				? ' text="' + e.data.text + '"'		: '';
											targetVal 	= e.data.target == true 	? ' target="_blank"'				: '';
											relVal 		= e.data.rel 				? ' rel="' + e.data.rel + '"'		: '';
											classVal 	= e.data.class 				? ' class="' + e.data.class + '"'	: '';
											
											shortcode 	= '[box_btn' + postVal + textVal + targetVal + relVal + classVal + ']';
											
											editor.insertContent( shortcode );
										}
								});
							}
						}
					]
		});
	});
})();

<?php
$zipcode 	= !empty( $_GET['zipcode'] )		? $_GET['zipcode']		: '';
$email 		= !empty( $_GET['email'] )	? $_GET['email']	: '';
/**
 * Template Name: Housekeeper Search Results
 *
 * The template for Housekeeper Search Results
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<section id="main-container" class="stage">
	<?php
		
		
		
		if ( get_query_var( 'paged' ) )
			$paged = get_query_var('paged');
		else if ( get_query_var( 'page' ) )
			$paged = get_query_var( 'page' );
		else
			$paged = 1;
		
		$per_page 			= 1;
		//var_dump($per_page);
		$args = array(
					'hierarchical' => false,
					'hide_empty'   => false,
					'meta_query'	=> array(
											array(
												'key'	=> 'maid_approved',
												'value'	=> 1,
												'compare' => '='
											),
										),
				);
		$housekeeper_count	= count( get_terms( 'wcpv_product_vendors', $args ) );
		//var_dump($housekeeper_count);
		$offset      		= $per_page * ( $paged - 1) ;
		//var_dump($offset);
		$max_num_pages 		= ceil( $housekeeper_count / $per_page );	
		//var_dump($max_num_pages);
		$args['offset'] 	= $offset;
		$args['number'] 	= $per_page;
				
		$housekeepers 		= get_terms('wcpv_product_vendors', $args );
		//var_dump($housekeepers);
		// Start the loop.
			if ( ! empty( $housekeepers ) ) :
		?>
			<div class="pagination center-block text-center">
		<?php
				echo get_next_posts_link( __('Next Profile'), $max_num_pages ) ;
    			echo get_previous_posts_link( __('Previous Profile') );
		?>
			</div>
		<?php
				foreach( $housekeepers as $housekeeper ) :
		?>
					<?php include( locate_template( 'woocommerce/content-single-housekeeper.php' ) ); ?>
	<?php
		endforeach;// End the loop.				
	?>
	<div class="pagination center-block text-center">
	<?php
			echo get_next_posts_link( __('Next Profile'), $max_num_pages ) ;
    		echo get_previous_posts_link( __('Previous Profile') );
	?>
	</div>
	<?php
		else:
	?>
	<section class="no-results not-found container p-y-2">
		<header class="page-header">
			<h1 class="page-title">
				<?php _e( 'Nothing Found', HCC_TEXTDOMAIN ); ?>
			</h1>
		</header>
		<!-- .page-header -->
		
		<div class="page-content">
			<p>
				<?php _e( 'No maids match your search criteria.', HCC_TEXTDOMAIN ); ?>
			</p>
			<?php echo do_shortcode('[housekeeper_search]'); ?> </div>
		<!-- .page-content --> 
	</section>
	<!-- .no-results -->
	<?php
		endif;
		?>
</section>
<!-- Modal -->
<div class="modal fade" id="login-register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content p-y-2 p-x-2">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="myModalLabel"><strong>To Contact <?php echo esc_html( $housekeeper->name ); ?>,<br />
You Must Become a Free Trial Member<br />
<span class="blue-text uppercase">CREATE YOUR ACCOUNT</span></strong></h4>
			</div>
			<div class="modal-body gray-box p-y-2 p-x-2 form-horizontal"> <?php echo do_shortcode('[wp_register_form]'); ?> </div>
			<div class="modal-footer">
			<ul class="fa-ul text-left small">
				<li><i class="fa-li fa fa-asterisk" aria-hidden="true"></i>
 All items marked with an * are required.</li>
 				<li><i class="fa-li fa fa-asterisk" aria-hidden="true"></i>
 Email Address is used as User Id.</li>
				<li><i class="fa-li fa fa-asterisk" aria-hidden="true"></i>
 We will not sell, share, rent, or distribute your personal information to third parties, and will not send you any unauthorized emails.</li></ul>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content p-y-2 p-x-2">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="contactModalLabel">OH NO! There seems to be a problem.</h4>
			</div>
			<div class="modal-body gray-box p-y-2 p-x-2 form-horizontal">There was an error contacting <?php echo esc_html( $housekeeper->name ); ?> please try again later or contact us for support.</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<!-- close main container -->

<?php get_footer(); ?>

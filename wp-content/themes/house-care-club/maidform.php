<?php
$submit = is_user_logged_in()									? __('Search Profiles')	: __('Search Profiles Free');
$action = !empty( of_get_option('hcc_search_result_page') )		? get_permalink(of_get_option('hcc_search_result_page')) 		: get_home_url();
if( $atts['action'] == 'siteurl' ):
	$action = get_home_url();
endif;
?>
<form role="search" method="get" id="searchform" action="<?php echo $action; ?>">
<div class="form-group">
<input type="text" value="<?php if ( !empty( $_GET['zipcode'] ) ): echo $_GET['zipcode']; else: echo ''; endif; ?>" name="zipcode" id="zipcode" class="form-control" placeholder="Zip Code*" />
</div>
<div class="form-group">
<input type="text" value="<?php if ( !empty( $_GET['email'] ) ): echo $_GET['email']; else: echo ''; endif; ?>" name="email" id="email" class="form-control" placeholder="Email*" />
</div>
<h3 class="blue-text text-center"><?php _e( 'Select Services Housekeepers Should Provide:', HCC_TEXTDOMAIN ); ?></h3>
<div class="form-group">
	<?php
	if ( isset( $_GET['services'] ) && !empty( $_GET['services'] ) ): 
		$services = $_GET['services']; 
	else: 
		$services = array(); 
	endif;
	?>
	<div class="col-xs-12 col-md-6">
		<div class="checkbox">
			<label class="radio-inline">
				<input type="checkbox" name="services[]" id="services-house-cleaning" value="House Cleaning" <?php checked( true, in_array( 'House Cleaning',$services ) , true ); ?>> House Cleaning
			</label>
		</div>
		<div class="checkbox">
			<label class="radio-inline">
				<input type="checkbox" name="services[]" id="services-daily-errands" value="Daily Errands" <?php checked( true, in_array( 'Daily Errands',$services ) , true ); ?>> Daily Errands
			</label>
		</div>
		<div class="checkbox">
			<label class="radio-inline">
				<input type="checkbox" name="services[]" id="services-child-care" value="Child Care" <?php checked( true, in_array( 'Child Care',$services ) , true ); ?>> Child Care
			</label>
		</div>
	</div>
	<div class="col-xs-12 col-md-6">
		<div class="checkbox">
			<label class="radio-inline">
				<input type="checkbox" name="services[]" id="services-personal-assitant" value="Personal Assitant" <?php checked( true, in_array( 'Personal Assitant',$services ) , true ); ?>> Personal Assitant
			</label>
		</div>
		<div class="checkbox">
			<label class="radio-inline">
				<input type="checkbox" name="services[]" id="services-pet-care" value="Pet Care" <?php checked( true, in_array( 'Pet Care',$services ) , true ); ?>> Pet Care
			</label>
		</div>
		<div class="checkbox">
			<label class="radio-inline">
				<input type="checkbox" name="services[]" id="services-senior-companion" value="Senior Companion" <?php checked( true, in_array( 'Senior Companion',$services ) , true ); ?>> Senior Companion
			</label>
		</div>
	</div>
</div>
<input type="hidden" value="wcpv_product_vendors" name="taxonomy" />
<input type="submit" id="searchsubmit" value="<?php echo $submit; ?>" />

</form>
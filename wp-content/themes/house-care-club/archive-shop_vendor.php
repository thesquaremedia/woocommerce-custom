<?php
$zipcode 	= !empty( $_GET['zipcode'] )		? $_GET['zipcode']		: '';
$email 		= !empty( $_GET['email'] )	? $_GET['email']	: '';
/**
 * Template Name: Housekeeper Search Results
 *
 * The template for Housekeeper Search Results
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
		
		<section id="main-container" class="stage">
		hosuekeeper
		<?php
		if ( !empty( $_GET['zipcode'] ) && !empty( $_GET['email'] )):
		$args = array(
					'taxonomy' => 'wcpv_product_vendors'
				);
		$housekeepers = get_terms( $args );
		// Start the loop.
			if ( ! empty( $housekeepers ) ) :
				foreach( $housekeepers as $housekeeper ) {
					$vendor_data = get_term_meta( $housekeeper->term_id, 'vendor_data', true );
		
					?>
					<li>
						
							<a href="<?php echo esc_url( get_term_link( $housekeeper->term_id, 'wcpv_product_vendors' ) ); ?>" class="wcpv-vendor-name"><?php echo esc_html( $vendor->name ); ?></a>
					
		
						<?php if ( ! empty( $vendor_data['logo'] ) ) { ?>
							<a href="<?php echo esc_url( get_term_link( $housekeeper->term_id, 'wcpv_product_vendors' ) ); ?>" class="wcpv-vendor-logo"><?php echo  wp_get_attachment_image( absint( $vendor_data['logo'] ), 'full' ); ?></a>
						<?php } ?>
					</li>
		<?php
				} 
			endif; 
		?>
		</article>
		<?php
		// End the loop.
		else:
		?>
		<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentysixteen' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentysixteen' ); ?></p>

					
				</div><!-- .page-content -->
			</section><!-- .error-404 -->
		<?php
		endif;
		?>

		</section><!-- close main container -->

<?php get_footer(); ?>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function hcc_save_house_profile_form(){
	if ( isset( $_POST['action'] ) && $_POST['action'] === 'update-house-profile' && isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'house-profile' ) ):
		//var_dump( 'post and nonce verified ready to save and email' );
		
		$current_user					= wp_get_current_user();
		$user_id						= $current_user->ID;
		$housekeeper_id 				= get_user_meta( $user_id, 'housekeper_to_contact', true);
		$housekeeper_data				= get_term_meta( $housekeeper_id, 'vendor_data', true);
		
		$house_data						= get_user_meta( $user_id, 'house_data', true);
		$post_data						= $_POST['house_data'];
		$messsage						= !empty($_POST['house_email_message']) ? sanitize_text_field($_POST['house_email_message']) : '';
		
		$house_data['house_type']			 = !empty( $post_data['house_type'] ) 			? sanitize_text_field( $post_data['house_type'] ) :'';
		$house_data['house_gated']			 = !empty( $post_data['house_gated'] ) 			? sanitize_text_field( $post_data['house_gated'] ) :'';
		$house_data['house_size']			 = !empty( $post_data['house_size'] ) 			? sanitize_text_field( $post_data['house_size'] ) :'';
		$house_data['total_rooms']			 = !empty( $post_data['total_rooms'] ) 			? sanitize_text_field( $post_data['total_rooms'] ) :'';
		$house_data['total_bedrooms']		 = !empty( $post_data['total_bedrooms'] )		? sanitize_text_field( $post_data['total_bedrooms'] ) :'';
		$house_data['total_bathrooms']		 = !empty( $post_data['total_bathrooms'] )		? sanitize_text_field( $post_data['total_bathrooms'] ) :'';
		$house_data['garage_size']			 = !empty( $post_data['garage_size'] ) 			? sanitize_text_field( $post_data['garage_size'] ) :'';
		$house_data['floor_number']			 = !empty( $post_data['floor_number'] ) 		? sanitize_text_field( $post_data['floor_number'] ) :'';
		$house_data['total_people_living']	 = !empty( $post_data['total_people_living'] )	? sanitize_text_field( $post_data['total_people_living'] ) :'';
		$house_data['assistant_services']	 = !empty( $post_data['assistant_services'] ) 	? array_map( 'sanitize_text_field', $post_data['assistant_services'] ) : array();
		$house_data['children_age']			 = !empty( $post_data['children_age'] ) ? array_map( 'sanitize_text_field', $post_data['children_age'] ) : array();
		$house_data['dogs_number']			 = !empty( $post_data['dogs_number'] ) ? sanitize_text_field( $post_data['dogs_number'] ) :'';
		$house_data['cats_number']			 = !empty( $post_data['cats_number'] ) ? sanitize_text_field( $post_data['cats_number'] ) :'';
		$house_data['last_time_cleaned']	 = !empty( $post_data['last_time_cleaned'] ) ? sanitize_text_field( $post_data['last_time_cleaned'] ) :'';
		$house_data['outside_cleaning']		 = !empty( $post_data['outside_cleaning'] ) ? sanitize_text_field( $post_data['outside_cleaning'] ) :'';
		$house_data['outside_areas']		 = !empty( $post_data['outside_areas'] ) ? array_map( 'sanitize_text_field', $post_data['outside_areas'] ) : array();
		$house_data['regular_supplies']		 = !empty( $post_data['regular_supplies'] ) ? sanitize_text_field( $post_data['regular_supplies'] ) :'';
		$house_data['green_supplies']		 = !empty( $post_data['green_supplies'] ) ? sanitize_text_field( $post_data['green_supplies'] ) :'';
		$house_data['cleaning_equipment']	 = !empty( $post_data['cleaning_equipment'] ) ? sanitize_text_field( $post_data['cleaning_equipment'] ) :'';
		$house_data['house_condition']		 = !empty( $post_data['house_condition'] ) ? array_map( 'sanitize_text_field', $post_data['house_condition'] ) : array();
		$house_data['times_cleaned']		 = !empty( $post_data['times_cleaned'] ) ? sanitize_text_field( $post_data['times_cleaned'] ) :'';
		$house_data['time_needed']			 = !empty( $post_data['time_needed'] ) ? sanitize_text_field( $post_data['time_needed'] ) :'';
		$house_data['start_time']			 = !empty( $post_data['start_time'] ) ? array_map( 'sanitize_text_field', $post_data['start_time'] ) : array();
		$house_data['attendance_preference'] = !empty( $post_data['attendance_preference'] ) ? sanitize_text_field( $post_data['attendance_preference'] ) :'';
		$house_data['days_of_week']			 = !empty( $post_data['days_of_week'] ) ? array_map( 'sanitize_text_field', $post_data['days_of_week'] ) : array();
		$to = get_option( 'admin_email' );
		$subject = __( 'House Care Club House Profile Submission', HCC_TEXTDOMAIN );
		$email_message 	=	"User ID: ".$current_user->ID."\n".
							"User Name: ".$current_user->user_firstname." ".$current_user->user_lastname."\n".
							"User Display Name: ".$current_user->display_name."\n\n".
							"Desired Housekeeper to Contact: ".$housekeeper_data['name']."\n\n".
							"House Profile\n\n".
							"House Type: ".$house_data['house_type']."\n".
							'Gated Community: '.$house_data['house_gated']."\n".
							'Size of House: '.hcc_replace_underscore( $house_data['house_size'] )."\n".
							'Total Rooms: '.hcc_replace_underscore( $house_data['total_rooms'] )."\n".
							'Total Bedrooms: '.hcc_replace_underscore( $house_data['total_bedrooms'] )."\n".
							'Total Bathrooms: '.hcc_replace_underscore( $house_data['total_bathrooms'] )."\n".
							'Garage Size: '.hcc_replace_underscore( $house_data['garage_size'] )."\n".
							'Number of Floors: '.hcc_replace_underscore( $house_data['floor_number'] )."\n".
							'Total People living in the House: '.hcc_replace_underscore( $house_data['total_people_living'] )."\n".
							'Personal Assistant Services Needed: '.implode( ', ', array_map ( 'hcc_replace_underscore', $house_data['assistant_services'] ) )."\n".
							'Ages of Children in the House: '.implode( ', ', array_map ( 'hcc_replace_underscore', $house_data['children_age'] ) )."\n".
							'Dogs in the House: '.hcc_replace_underscore( $house_data['dogs_number'] )."\n".
							'Cats in the House: '.hcc_replace_underscore( $house_data['cats_number'] )."\n".
							'Last Time House Was Professionally Cleaned? '.hcc_replace_underscore( $house_data['last_time_cleaned'] )."\n".
							'Outside Cleaning Needed? '.hcc_replace_underscore( $house_data['outside_cleaning'] )."\n".
							'Outside Areas To Clean '.implode( ', ', array_map ( 'hcc_replace_underscore', $house_data['outside_areas'] ) )."\n".
							'Regular Cleaning Supplies: '.hcc_replace_underscore( $house_data['regular_supplies'] )."\n".
							'Green Cleaning Supplies: '.hcc_replace_underscore( $house_data['green_supplies'] )."\n".
							'Cleaning Equipment: '.hcc_replace_underscore( $house_data['cleaning_equipment'] )."\n".
							'Repairs or Maintenance Possibly Needed Within the Next Year: '. implode( ', ', array_map ( 'hcc_replace_underscore', $house_data['house_condition'] ) )."\n".
							'How Often Do You Want Your House Cleaned? '.hcc_replace_underscore( $house_data['times_cleaned'] )."\n".
							'Maximum Number of Hours Wanted Each Cleaning Visit: '.hcc_replace_underscore( $house_data['time_needed'] )."\n".
							'Preferred Start Time: '.implode( ', ', array_map ( 'hcc_replace_underscore', $house_data['start_time'] ) )."\n".
							'Attendance Preferences: '.hcc_replace_underscore( $house_data['attendance_preference'] )."\n".
							'Preferred Days of the Week: '.implode( ', ', array_map ( 'hcc_replace_underscore', $house_data['days_of_week'] ) )."\n".
							"Message \n\n".$messsage;
							
		
		$updating = update_user_meta( $user_id, 'house_data', $house_data );
		if ( is_wp_error($updating) ):
			wc_add_notice( __( 'There was an error saving your information please contact the admin or try again later', HCC_TEXTDOMAIN ), 'error');
		else:
			if ( wp_mail( $to, $subject, $email_message ) ):
				wc_add_notice( __( 'Your message was sent someone will contact you as soon as possible', HCC_TEXTDOMAIN ), 'success');
			else:
				wc_add_notice( __( 'The email couldn\'t be sent. Please try again later.', HCC_TEXTDOMAIN ), 'error');
			endif;
		endif;
	endif;
	
}
add_action( 'init', 'hcc_save_house_profile_form' );

function hcc_house_profile_form_func( $housekeeper_data = array() ){
	if ( isset( $_POST['action'] ) && $_POST['action'] === 'update-house-profile' && isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'house-profile' ) ):
		$house_data						= $_POST['house_data'];
	else:
		$current_user					= wp_get_current_user();
		$user_id						= $current_user->ID;
		$house_data						= get_user_meta( $user_id, 'house_data', true);
	endif;
		
	$house_type = !empty( $house_data['house_type'] ) ? $house_data['house_type'] :'';
	$house_gated = !empty( $house_data['house_gated'] ) ? $house_data['house_gated'] :'';
	$house_size = !empty( $house_data['house_size'] ) ? $house_data['house_size'] : '';
	$total_rooms = !empty( $house_data['total_rooms'] ) ? $house_data['total_rooms'] : '';
	$total_bedrooms = !empty( $house_data['total_bedrooms'] ) ? $house_data['total_bedrooms'] : '';
	$total_bathrooms = !empty( $house_data['total_bathrooms'] ) ? $house_data['total_bathrooms'] : '';
	$garage_size = !empty( $house_data['garage_size'] ) ? $house_data['garage_size'] : '';
	$floor_number = !empty( $house_data['floor_number'] ) ? $house_data['floor_number'] : '';
	$total_people_living = !empty( $house_data['total_people_living'] ) ? $house_data['total_people_living'] : '';
	$assistant_services = !empty( $house_data['assistant_services'] ) ? $house_data['assistant_services'] : array();
	$children_age = !empty( $house_data['children_age'] ) ? $house_data['children_age'] : array();
	$dogs_number = !empty( $house_data['dogs_number'] ) ? $house_data['dogs_number'] : '';
	$cats_number = !empty( $house_data['cats_number'] ) ? $house_data['cats_number'] : '';
	$last_time_cleaned = !empty( $house_data['last_time_cleaned'] ) ? $house_data['last_time_cleaned'] : '';
	$outside_cleaning = !empty( $house_data['outside_cleaning'] ) ? $house_data['outside_cleaning'] : '';
	$outside_areas = !empty( $house_data['outside_areas'] ) ? $house_data['outside_areas'] : array();
	$regular_supplies = !empty( $house_data['regular_supplies'] ) ? $house_data['regular_supplies'] : '';
	$green_supplies = !empty( $house_data['green_supplies'] ) ? $house_data['green_supplies'] : '';
	$cleaning_equipment = !empty( $house_data['cleaning_equipment'] ) ? $house_data['cleaning_equipment'] : '';
	$house_condition = !empty( $house_data['house_condition'] ) ? $house_data['house_condition'] : array();
	$times_cleaned = !empty( $house_data['times_cleaned'] ) ? $house_data['times_cleaned'] : '';
	$time_needed = !empty( $house_data['time_needed'] ) ? $house_data['time_needed'] : '';
	$start_time = !empty( $house_data['start_time'] ) ? $house_data['start_time'] : array();
	$attendance_preference = !empty( $house_data['attendance_preference'] ) ? $house_data['attendance_preference'] : '';
	$days_of_week = !empty( $house_data['days_of_week'] ) ? $house_data['days_of_week'] : array();
	?>
	<form method="post" id="house-profile-form" action="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ) . 'house-profile'; ?>">
			<div class="col-xs-12 p-y-2 p-x-2 gray-box">
				<div class="panel-group" id="house-profile-accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> 1. Type of House? <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<div class="form-group"> In Development:
									<label class="radio-inline">
										<input type="radio" name="house_data[house_type]" value="apartment" <?php checked( $house_type, 'apartment' , true); ?> />
										Apartment </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[house_type]" value="condominium" <?php checked( $house_type, 'condominium' , true); ?> />
										Condominium </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[house_type]" value="house" <?php checked( $house_type, 'house' , true); ?> />
										House </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[house_type]" value="townhouse" <?php checked( $house_type, 'townhouse' , true); ?> />
										Townhouse </label>
								</div>
								<div class="form-group"> Gated Community:
									<label class="radio-inline">
										<input type="radio" name="house_data[house_gated]" value="yes" <?php checked( $house_gated, 'yes' , true); ?> />
										Yes </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[house_gated]" value="no" <?php checked( $house_gated, 'no' , true); ?> />
										No </label>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> 2. Size of House? <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
								<div class="radio">
									<label>
										<input type="radio" name="house_data[house_size]" value="Under 1,000 Sq Ft." <?php checked( $house_size, 'Under 1,000 Sq Ft.' , true); ?> />
										Under 1,000 Sq Ft. </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[house_size]" value="1,001 - 2,000 Sq. Ft." <?php checked( $house_size, '1001_2000_sq_ft' , true); ?> />
										1,001 - 2,000 Sq. Ft. </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[house_size]" value="2,001 - 3,000 Sq. Ft." <?php checked( $house_size, '2,001 - 3,000 Sq. Ft.' , true); ?> />
										2,001 - 3,000 Sq. Ft. </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[house_size]" value="3,001 – 4,000 Sq. Ft." <?php checked( $house_size, '3,001 – 4,000 Sq. Ft.' , true); ?> />
										3,001 – 4,000 Sq. Ft. </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[house_size]" value="4001 - 5,000 Sq. Ft." <?php checked( $house_size, '4001 - 5,000 Sq. Ft.' , true); ?> />
										4001 - 5,000 Sq. Ft. </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[house_size]" value="Over 5,000 Sq. Ft." <?php checked( $house_size, 'Over 5,000 Sq. Ft.' , true); ?> />
										Over 5,000 Sq. Ft. </label>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingThree">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> 3. Rooms in House? <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
								<div class="form-group"> Total Rooms:
									<label class="radio-inline">
										<input type="radio" name="house_data[total_rooms]" value="1-3" <?php checked( $total_rooms, '1-3' , true); ?> />
										1-3 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_rooms]" value="3-5" <?php checked( $total_rooms, '3-5' , true); ?> />
										3-5 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_rooms]" value="6-10" <?php checked( $total_rooms, '6-10' , true); ?> />
										6-10 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_rooms]" value="over 10" <?php checked( $total_rooms, 'over 10' , true); ?> />
										Over 10 </label>
								</div>
								<div class="form-group"> Total Bedrooms:
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bedrooms]" value="1" <?php checked( $total_bedrooms, '1' , true); ?> />
										1 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bedrooms]" value="2" <?php checked( $total_bedrooms, '2' , true); ?> />
										2 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bedrooms]" value="3" <?php checked( $total_bedrooms, '3' , true); ?> />
										3 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bedrooms]" value="4" <?php checked( $total_bedrooms, '4' , true); ?> />
										4 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bedrooms]" value="5" <?php checked( $total_bedrooms, '5' , true); ?> />
										5 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bedrooms]" value="over 5" <?php checked( $total_bedrooms, 'over 5' , true); ?> />
										Over 5 </label>
								</div>
								<div class="form-group"> Total Bathrooms:
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bathrooms]" value="1" <?php checked( $total_bathrooms, '1' , true); ?> />
										1 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bathrooms]" value="2" <?php checked( $total_bathrooms, '2' , true); ?> />
										2 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bathrooms]" value="3" <?php checked( $total_bathrooms, '3' , true); ?> />
										3 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bathrooms]" value="4" <?php checked( $total_bathrooms, '4' , true); ?> />
										4 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bathrooms]" value="5" <?php checked( $total_bathrooms, '5' , true); ?> />
										5 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_bathrooms]" value="over 5" <?php checked( $total_bathrooms, 'over 5' , true); ?> />
										Over 5 </label>
								</div>
								<div class="form-group"> Garage:
									<label class="radio-inline">
										<input type="radio" name="house_data[garage_size]" value="1 car" <?php checked( $garage_size, '1 car' , true); ?> />
										1 Car </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[garage_size]" value="2 cars" <?php checked( $garage_size, '2 cars' , true); ?> />
										2 Cars </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[garage_size]" value="3 cars" <?php checked( $garage_size, '3 cars' , true); ?> />
										3 Cars </label>
								</div>
								<div class="form-group"> Number of Floors:
									<label class="radio-inline">
										<input type="radio" name="house_data[floor_number]" value="1" <?php checked( $floor_number, '1' , true); ?> />
										1 Floor </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[floor_number]" value="2 or more" <?php checked( $floor_number, '2 or more' , true); ?> />
										2 or More Floors </label>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingFour">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"> 4. How Many Total People Live in Your House? <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseTwo" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
							<div class="panel-body">
								<div class="form-group">
									<label class="radio-inline">
										<input type="radio" name="house_data[total_people_living]" value="1" <?php checked( $total_people_living, '1' , true); ?>/>
										1 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_people_living]" value="2" <?php checked( $total_people_living, '2' , true); ?> />
										2 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_people_living]" value="3" <?php checked( $total_people_living, '3' , true); ?> />
										3 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_people_living]" value="4" <?php checked( $total_people_living, '4' , true); ?> />
										4 </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[total_people_living]" value="5 or more" <?php checked( $total_people_living, '5 or more' , true); ?> />
										5 or Over </label>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingFive">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"> 5. Personal Assistant Services That May Be Needed Periodically <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
							<div class="panel-body">
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[assistant_services][]" value="Cook Dinner / Clean up" <?php checked( true, in_array( 'Cook Dinner / Clean up', $assistant_services) , true); ?> />
											Cook Dinner / Clean up </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[assistant_services][]" value="Dry Cleaning Drop Off / Pick Up" <?php checked( true, in_array( 'Dry Cleaning Drop Off / Pick Up', $assistant_services) , true); ?> />
											Dry Cleaning Drop Off / Pick Up </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[assistant_services][]" value="Errand Running" <?php checked( true, in_array( 'Errand Running', $assistant_services) , true); ?> />
											Errand Running </label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[assistant_services][]" value="Grocery Shop" <?php checked( true, in_array( 'Grocery Shop', $assistant_services) , true); ?> />
											Grocery Shop </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[assistant_services][]" value="Iron Clothes" <?php checked( true, in_array( 'Iron Clothes', $assistant_services) , true); ?> />
											Iron Clothes </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[assistant_services][]" value="Wash Laundry" <?php checked( true, in_array( 'Wash Laundry', $assistant_services) , true); ?> />
											Wash Laundry </label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingSix">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"> 6. What Are The Ages of Any Children living in Your Home? <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
							<div class="panel-body">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="house_data[children_age][]" value="Infant/Toddler 1-2" <?php checked( true, in_array( 'Infant/Toddler 1-2', $children_age) , true); ?> />
										Infant/Toddler 1-2 </label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" name="house_data[children_age][]" value="Pre-School 3-5" <?php checked( true, in_array( 'Pre-School 3-5', $children_age) , true); ?> />
										Pre-School 3-5 </label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" name="house_data[children_age][]" value="Pre-teens" <?php checked( true, in_array( 'Pre-teens', $children_age) , true); ?> />
										Pre-teens </label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" name="house_data[children_age][]" value="Teens" <?php checked( true, in_array( 'Teens', $children_age) , true); ?> />
										Teens </label>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingSeven">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"> 7. Animals in House? <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
							<div class="panel-body">
								<div class="form-group">
									Number of Dogs
									<label class="radio-inline">
										<input type="radio" name="house_data[dogs_number]" value="none" <?php checked( $dogs_number, 'none' , true); ?> />
										None </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[dogs_number]" value="1 dog" <?php checked( $dogs_number, '1 dog' , true); ?> />
										1 Dog </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[dogs_number]" value="2 dogs" <?php checked( $dogs_number, '2 dogs' , true); ?> />
										2 Dog </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[dogs_number]" value="3 dogs or over" <?php checked( $dogs_number, '3 dogs or over' , true); ?> />
										3 Dogs or Over </label>
								</div>
								<div class="form-group">
									Number of Cats
									<label class="radio-inline">
										<input type="radio" name="house_data[cats_number]" value="none" <?php checked( $cats_number, 'none' , true); ?> />
										None </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[cats_number]" value="1 cat" <?php checked( $cats_number, '1 cat' , true); ?> />
										1 Cat </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[cats_number]" value="2 cats" <?php checked( $cats_number, '2 cats' , true); ?> />
										2 Cats </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[cats_number]" value="3 cats or over" <?php checked( $cats_number, '3 cats or over' , true); ?> />
										3 Cats or Over</label>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingEight">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight"> 8. Last Time House Was Professionally Cleaned? <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
							<div class="panel-body">
								<div class="radio">
								<label>
										<input type="radio" name="house_data[last_time_cleaned]" value="within 30 days" <?php checked( $last_time_cleaned, 'within 30 days' , true); ?> />
										Within 30 days </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[last_time_cleaned]" value="31 to 61 days" <?php checked( $last_time_cleaned, '31 to 61 days' , true); ?> />
										31 to 60 days ago </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[last_time_cleaned]" value="61 to 90 days" <?php checked( $last_time_cleaned, '61 to 90 days' , true); ?> />
										61 to 90 days ago </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[last_time_cleaned]" value="over 90 days" <?php checked( $last_time_cleaned, 'over 90 days' , true); ?> />
										Over 90 days ago </label>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingNine">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine"> 9. Outside Cleaning Needed <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseNine" aria-expanded="true" aria-controls="collapseNine" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
							<div class="panel-body">
								<div class="radio">
								<label>
										<input type="radio" name="house_data[outside_cleaning]" value="not needed" <?php checked( $outside_cleaning, 'not needed' , true); ?> />
										Not Needed </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[outside_cleaning]" value="every time" <?php checked( $outside_cleaning, 'every time' , true); ?> />
										To Be Done Every Time </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[outside_cleaning]" value="periodically" <?php checked( $outside_cleaning, 'periodically' , true); ?> />
										To Be Done Periodically As Directed </label>
								</div>
								<p>Outside Areas To Clean</p>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[outside_areas][]" value="barbecue" <?php checked( true, in_array( 'barbecue', $outside_areas) , true); ?> />
											Barbecue </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[outside_areas][]" value="driveway" <?php checked( true, in_array( 'driveway', $outside_areas) , true); ?> />
											Driveway </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[outside_areas][]" value="furniture" <?php checked( true, in_array( 'furniture', $outside_areas) , true); ?> />
											Furniture </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[outside_areas][]" value="grass mowing" <?php checked( true, in_array( 'grass mowing', $outside_areas) , true); ?> />
											Grass Mowing </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[outside_areas][]" value="outside kitchen" <?php checked( true, in_array( 'outside kitchen', $outside_areas) , true); ?> />
											Outside Kitchen </label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[outside_areas][]" value="patios" <?php checked( true, in_array( 'patios', $outside_areas) , true); ?> />
											Patios </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[outside_areas][]" value="poop scoop yard" <?php checked( true, in_array( 'poop scoop yard', $outside_areas) , true); ?> />
											Poop Scoop Yard </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[outside_areas][]" value="swimming pool" <?php checked( true, in_array( 'swimming pool', $outside_areas) , true); ?> />
											Swimming Pool </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[outside_areas][]" value="yard cleanup" <?php checked( true, in_array( 'yard cleanup', $outside_areas) , true); ?> />
											Yard Cleanup </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[outside_areas][]" value="walkways" <?php checked( true, in_array( 'walkways', $outside_areas) , true); ?> />
											Walkways </label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTen">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen"> 10. Will You Provide All Cleaning Supplies and Equipment? <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseTen" aria-expanded="true" aria-controls="collapseTen" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
							<div class="panel-body">
								<div class="form-group">
									Regular Cleaning Supplies
									<label class="radio-inline">
										<input type="radio" name="house_data[regular_supplies]" value="yes" <?php checked( $regular_supplies, 'yes' , true); ?> />
										Yes </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[regular_supplies]" value="no" <?php checked( $regular_supplies, 'no' , true); ?>/>
										No </label>
								</div>
								<div class="form-group">
									Green Cleaning Supplies
									<label class="radio-inline">
										<input type="radio" name="house_data[green_supplies]" value="yes" <?php checked( $green_supplies, 'yes' , true); ?>/>
										Yes </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[green_supplies]" value="no" <?php checked( $green_supplies, 'no' , true); ?>/>
										No </label>
								</div>
								<div class="form-group">
									Cleaning Equipment
									<label class="radio-inline">
										<input type="radio" name="house_data[cleaning_equipment]" value="yes" <?php checked( $cleaning_equipment, 'yes' , true); ?>/>
										Yes </label>
									<label class="radio-inline">
										<input type="radio" name="house_data[cleaning_equipment]" value="no" <?php checked( $cleaning_equipment, 'no' , true); ?>/>
										No </label>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingEleven">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven"> 11. Condition of House <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseEleven" aria-expanded="true" aria-controls="collapseEleven" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
							<div class="panel-body">
								<p>Repairs or Maintenance Possibly Needed Within the Next Year</p>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[house_condition][]" value="bathroom rennovation" <?php checked( true, in_array( 'bathroom rennovation', $house_condition) , true); ?>/>
											Bathroom Renovation </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[house_condition][]" value="carpet shampooing" <?php checked( true, in_array( 'carpet shampooing', $house_condition) , true); ?> />
											Carpet Shampooing </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[house_condition][]" value="floor repairs" <?php checked( true, in_array( 'floor repairs', $house_condition) , true); ?> />
											Floor Repairs (Tile/Wood/Marble) </label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[house_condition][]" value="kitchen rennovation" <?php checked( true, in_array( 'kitchen rennovation', $house_condition) , true); ?> />
											Kitchen Renovation </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[house_condition][]" value="marble polishing" <?php checked( true, in_array( 'marble polishing', $house_condition) , true); ?> />
											Marble Polishing </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[house_condition][]" value="painting" <?php checked( true, in_array( 'painting', $house_condition) , true); ?> />
											Painting </label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[house_condition][]" value="tile grout cleaning" <?php checked( true, in_array( 'tile grout cleaning', $house_condition) , true); ?> />
											Tile Grout Cleaning </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[house_condition][]" value="upholstery shampooing" <?php checked( true, in_array( 'upholstery shampooing', $house_condition) , true); ?> />
											Upholstery shampooing </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[house_condition][]" value="wood floor waxing" <?php checked( true, in_array( 'wood floor waxing', $house_condition) , true); ?> />
											Wood Floor Waxing </label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTwelve">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve"> 12. How Often Do You Want Your House Cleaned? <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseTwelve" aria-expanded="true" aria-controls="collapseTwelve" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
							<div class="panel-body">
								<div class="col-xs-6 col-sm-3">
									<div class="radio">
										<label>
											<input type="radio" name="house_data[times_cleaned]" value="1 time a week" <?php checked( $times_cleaned, '1 time a week' , true); ?>/>
											1 time a week </label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="house_data[times_cleaned]" value="2 times a week" <?php checked( $times_cleaned, '2 times a week' , true); ?>/>
											2 times a week </label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="house_data[times_cleaned]" value="3 times a week" <?php checked( $times_cleaned, '3 times a week' , true); ?>/>
											3 times a week </label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="house_data[times_cleaned]" value="4 times a week" <?php checked( $times_cleaned, '4 times a week' , true); ?>/>
											4 times a week </label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="radio">
										<label>
											<input type="radio" name="house_data[times_cleaned]" value="5 times a week" <?php checked( $times_cleaned, '5 times a week' , true); ?>/>
											5 times a week </label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="house_data[times_cleaned]" value="every other week" <?php checked( $times_cleaned, 'every other week' , true); ?>/>
											Every other week </label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="house_data[times_cleaned]" value="monthly" <?php checked( $times_cleaned, 'monthly' , true); ?>/>
											Monthly </label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="house_data[times_cleaned]" value="will vary" <?php checked( $times_cleaned, 'will vary' , true); ?>/>
											Will Vary </label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingThirteen">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen"> 13. Maximum Number of Hours Wanted Each Cleaning Visit <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseThirteen" aria-expanded="true" aria-controls="collapseThirteen" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
							<div class="panel-body">
								<div class="col-xs-6 col-sm-3">
									<div class="radio">
										<label>
											<input type="radio" name="house_data[time_needed]" value="2 hours" <?php checked( $time_needed, '2 hours' , true); ?>/>
											2 hours </label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="house_data[time_needed]" value="3 hours" <?php checked( $time_needed, '3 hours' , true); ?>/>
											3 hours </label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="radio">
										<label>
											<input type="radio" name="house_data[time_needed]" value="4 hours" <?php checked( $time_needed, '4 hours' , true); ?>/>
											4 hours </label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="house_data[time_needed]" value="over 4 hours" <?php checked( $time_needed, 'over 4 hours' , true); ?>/>
											Over 4 hours is needed to do my requests </label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingFourteen">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen"> 14. Preferred Start Time <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseFourteen" aria-expanded="true" aria-controls="collapseFourteen" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourteen">
							<div class="panel-body">
								<p>Start Time For Each Cleaning Session</p>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="7 am" <?php checked( true, in_array( '7 am', $start_time) , true); ?> />
											7 am </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="8 am" <?php checked( true, in_array( '8 am', $start_time) , true); ?> />
											8 am </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="9 am" <?php checked( true, in_array( '9 am', $start_time) , true); ?> />
											9 am </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="10 am" <?php checked( true, in_array( '10 am', $start_time) , true); ?> />
											10 am </label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="11 am" <?php checked( true, in_array( '11 am', $start_time) , true); ?> />
											11 am </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="12 pm" <?php checked( true, in_array( '12 pm', $start_time) , true); ?> />
											12 pm </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="1 pm" <?php checked( true, in_array( '1 pm', $start_time) , true); ?> />
											1 pm </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="2 pm" <?php checked( true, in_array( '2 pm', $start_time) , true); ?> />
											2 pm </label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="3 pm" <?php checked( true, in_array( '3 pm', $start_time) , true); ?> />
											3 pm </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="4 pm" <?php checked( true, in_array( '4 pm', $start_time) , true); ?> />
											4 pm </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="5 pm" <?php checked( true, in_array( '5 pm', $start_time) , true); ?> />
											5 pm </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="6 pm" <?php checked( true, in_array( '6 pm', $start_time) , true); ?> />
											6 pm </label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="7 pm" <?php checked( true, in_array( '7 pm', $start_time) , true); ?> />
											7 pm </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="8 pm" <?php checked( true, in_array( '8 pm', $start_time) , true); ?> />
											8 pm </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="9 pm" <?php checked( true, in_array( '9 pm', $start_time) , true); ?> />
											9 pm </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[start_time][]" value="will vary" <?php checked( true, in_array( 'will vary', $start_time) , true); ?> />
											Will Vary </label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingFifteen">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen"> 15. Attendance Preferences <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseFifteen" aria-expanded="true" aria-controls="collapseFifteen" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen">
							<div class="panel-body">
								<div class="radio">
									<label>
										<input type="radio" name="house_data[attendance_preference]" value="yes" <?php checked( $attendance_preference, 'yes' , true); ?>/>
										Want to be at home when house is cleaned </label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="house_data[attendance_preference]" value="no" <?php checked( $attendance_preference, 'no' , true); ?>/>
										Don't need to be at home when house is cleaned </label>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingSixteen">
							<h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen"> 16. Preferred Days of the Week <span class="thin">(Select)</span> </a> <a role="button" data-toggle="collapse" data-parent="#house-profile-accordion" href="#collapseSixteen" aria-expanded="true" aria-controls="collapseSixteen" class="pull-right"> <i class="fa fa-angle-double-down" aria-hidden="true"></i> </a> </h4>
						</div>
						<div id="collapseSixteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSixteen">
							<div class="panel-body">
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[days_of_week][]" value="monday" <?php checked( true, in_array( 'monday', $days_of_week) , true); ?> />
											Monday </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[days_of_week][]" value="tuesday" <?php checked( true, in_array( 'tuesday', $days_of_week) , true); ?> />
											Tuesday </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[days_of_week][]" value="wednesday" <?php checked( true, in_array( 'wednesday', $days_of_week) , true); ?> />
											Wednesday </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[days_of_week][]" value="thursday" <?php checked( true, in_array( 'thursday', $days_of_week) , true); ?> />
											Thursday </label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[days_of_week][]" value="friday" <?php checked( true, in_array( 'friday', $days_of_week) , true); ?> />
											Friday </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[days_of_week`23][]" value="saturday" <?php checked( true, in_array( 'saturday', $days_of_week) , true); ?> />
											Saturday </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[days_of_week][]" value="sunday" <?php checked( true, in_array( 'sunday', $days_of_week) , true); ?> />
											Sunday </label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="house_data[days_of_week][]" value="will vary" <?php checked( true, in_array( 'will vary', $days_of_week) , true); ?> />
											Will Vary </label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				</div>
				
				<div class="col-xs-12 p-y-2 p-x-2">
					<h2 class="text-center uppercase"><?php _e( 'After You Submit The Above Information.', 'woocommerce' ); ?></h2>
					<p class="text-center"><?php printf(  __( 'Please text %s your phone number so she can call you to answer any questions you may have and,
if you’re satisfied, set up a first cleaning so you can see the quality of her work.', 'woocommerce' ), $housekeeper_data['name'] ); ?></p>
					<div class="col-xs-12 col-sm-6 p-y-2 p-x-2 gray-box center-block no-float">
						<div class="form-group">
							<textarea type="text" name="house_email_message" id="house-email-message" class="form-control" placeholder="Enter Message" rows="5"></textarea>
						</div>
						<p class="form-submit text-center">
							<input name="updatehouseprofile" type="submit" id="updatehouseprofile" class="submit button" value="<?php _e('Send Message', 'profile'); ?>" />
							<?php wp_nonce_field( 'house-profile' ) ?>
							<input name="action" type="hidden" id="action" value="update-house-profile" />
						</p>
					</div>
				</div>
				<!-- .form-submit -->
			</form>
	<?php
}
add_action( 'hcc_house_profile_form' , 'hcc_house_profile_form_func', 10 , 2 );
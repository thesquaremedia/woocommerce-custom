<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="container p-y-2">
	<?php
if ( $order ) : 
//var_dump( $order );

$items = $order->get_items();


$memberships_in_cart = false;
foreach ( $items as $cart_item_key => $cart_item ) :
	//var_dump($cart_item);
	$_product = $cart_item;
	$terms = get_the_terms( $_product['product_id'], 'product_cat' );

	foreach ($terms as $term) {
		$_category_slug = $term->slug;
	}
	
	//var_dump( $terms );
	if ( $_category_slug == 'memberships' ) {
		//book is in cart!
		$memberships_in_cart = true;
		break;
	}
endforeach;
?>
	<?php if ( $order->has_status( 'failed' ) ) : ?>
	<p class="woocommerce-thankyou-order-failed">
		<?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?>
	</p>
	<p class="woocommerce-thankyou-order-failed-actions"> <a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay">
		<?php _e( 'Pay', 'woocommerce' ) ?>
		</a>
		<?php if ( is_user_logged_in() ) : ?>
		<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay">
		<?php _e( 'My Account', 'woocommerce' ); ?>
		</a>
		<?php endif; ?>
	</p>
	<?php else : ?>
	<h3 class="text-center"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Your Credit Card Has Been Approved', 'woocommerce' ), $order ); ?></h3>
	<?php 
			//var_dump( $order->user_id );
			if ( $memberships_in_cart ):
			?>
	<p class="woocommerce-thankyou-order-received">
		<?php _e( 'Hi ', 'woocommerce' ); echo $order->billing_first_name . ' ' . $order->billing_last_name . ',';  ?>
	</p>
	<p class="woocommerce-thankyou-order-received">
		<?php _e( 'Welcome to House Care Club. We look forward to working with you to create a Personalized Plan.  My name is Carrie, and I’m your contact person regarding any questions, or services you may need.  You can reach me at: <a href="mailto:AF@housecareclub.com">AF@housecareclub.com</a>.', 'woocommerce' ); ?>
	</p>
	<p class="woocommerce-thankyou-order-received">
		<?php _e( 'Sincerely,<br />Carrie Newton<br />House Care Club Advisor', 'woocommerce' ); ?>
	</p>
	<div class="row box">
		<div class="col-xs-12 p-y-2 p-x-2">
			<?php 
						$housekeeper_id = get_user_meta($order->user_id, 'housekeper_to_contact', true);
						$housekeeper_data = get_term_meta( $housekeeper_id, 'vendor_data', true);
						//var_dump( $housekeeper_data );
					?>
			<h2 class="text-center"><?php printf( __( 'Thank You For Your Interest In %s\'s Profile.' ), $housekeeper_data['name'] ); ?></h2>
			<p class="text-center">
				<?php _e( 'Please take a few minutes to answer the questions below, so she has more information about your home before she speaks with you.', 'woocommerce' ); ?>
			</p>
			<h4 class="text-center uppercase">
				<?php _e('Please Answer the Following Questions:', 'woocommerce' ); ?>
			</h4>
		</div>
			<?php 
			
			do_action( 'hcc_house_profile_form', $housekeeper_data ); 
			
			?>
			
			<!-- #adduser --> 
		
	</div>
	<?php
			else:
			?>
	<p class="woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); ?></p>
	<ul class="woocommerce-thankyou-order-details order_details">
		<li class="order">
			<?php _e( 'Order Number:', 'woocommerce' ); ?>
			<strong><?php echo $order->get_order_number(); ?></strong> </li>
		<li class="date">
			<?php _e( 'Date:', 'woocommerce' ); ?>
			<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong> </li>
		<li class="total">
			<?php _e( 'Total:', 'woocommerce' ); ?>
			<strong><?php echo $order->get_formatted_order_total(); ?></strong> </li>
		<?php if ( $order->payment_method_title ) : ?>
		<li class="method">
			<?php _e( 'Payment Method:', 'woocommerce' ); ?>
			<strong><?php echo $order->payment_method_title; ?></strong> </li>
		<?php endif; ?>
	</ul>
	<div class="clear"></div>
	<?php
				do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); 
				do_action( 'woocommerce_thankyou', $order->id );
			endif;
			?>
	<?php endif; ?>
	<?php else : ?>
	<p class="woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>
	<?php endif; ?>
</div>

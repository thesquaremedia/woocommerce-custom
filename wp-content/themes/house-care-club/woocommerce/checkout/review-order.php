<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$memberships_in_cart = false;
foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) :
	$_product = $cart_item['data'];
	$terms = get_the_terms( $_product->id, 'product_cat' );
	
	if( !empty( $terms) ):
		foreach ($terms as $term) {
			$_category_slug = $term->slug;
			//var_dump( $terms );
			if ( $_category_slug == 'memberships' ) {
				//book is in cart!
				$memberships_in_cart = true;
				break;
			}
		}
	else:
		$memberships_in_cart = false;
	endif;
	
	if( $memberships_in_cart == true)
		break;
endforeach;
?>
<table class="shop_table woocommerce-checkout-review-order-table box p-y-1 p-x-1">
	<?php
	if( !$memberships_in_cart ):
	?>
	<thead>
		<tr>
			<th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<?php
	endif;
	?>
	<tbody>
		<?php
			do_action( 'woocommerce_review_order_before_cart_contents' );

			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
						$terms = get_the_terms( $_product->id, 'product_cat' );
							
						$_category_slugs = array();
						if ( !empty( $terms ) ):
							foreach ($terms as $term):
								$_category_slugs[] = $term->slug;
							endforeach;
						endif;
					?>
					<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?> <?php echo implode( ' ', $_category_slugs ); ?>">
						<td class="product-name">
							<?php 
							echo get_the_post_thumbnail( $_product->id , 'medium', array( 'class' => 'img-reponsive '. implode( ' ', $_category_slugs ) ) ); 
							?>
							<?php 
							if ( ! in_array( 'memberships', $_category_slugs ) ):
								echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
								echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times; %s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); 
							endif;
							?>
							<?php echo WC()->cart->get_item_data( $cart_item ); ?>
						</td>
						<td class="product-total text-center">
							<?php 
							if ( in_array( 'memberships', $_category_slugs ) ):
								echo apply_filters( 'woocommerce_cart_item_name', '<span class="product-name">' . $_product->get_title(), $cart_item, $cart_item_key ) . ' Membership</span>'; 
							endif;
							?>
							<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
						</td>
					</tr>
					<?php
				}
			}

			do_action( 'woocommerce_review_order_after_cart_contents' );
		?>
	</tbody>
	<tfoot>
		<tr>
		<td colspan="2">
		<table class="order-review-footer <?php echo implode( ' ', $_category_slugs ); ?>">
			<tbody>
				<tr class="cart-subtotal">
					<th><?php _e( 'Subtotal', 'woocommerce' ); ?></th>
					<td><?php wc_cart_totals_subtotal_html(); ?></td>
				</tr>
				
				<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
					<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
						<th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
						<td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
					</tr>
				<?php endforeach; ?>
		
				<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
		
					<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>
		
					<?php wc_cart_totals_shipping_html(); ?>
		
					<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>
		
				<?php endif; ?>
		
				<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
					<tr class="fee">
						<th><?php echo esc_html( $fee->name ); ?></th>
						<td><?php wc_cart_totals_fee_html( $fee ); ?></td>
					</tr>
				<?php endforeach; ?>
		
				<?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) : ?>
					<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
						<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
							<tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
								<th><?php echo esc_html( $tax->label ); ?></th>
								<td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
							</tr>
						<?php endforeach; ?>
					<?php else : ?>
						<tr class="tax-total">
							<th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
							<td><?php wc_cart_totals_taxes_total_html(); ?></td>
						</tr>
					<?php endif; ?>
				<?php endif; ?>
		
				<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>
		
				<tr class="order-total">
					<th><?php _e( 'Total', 'woocommerce' ); ?></th>
					<td><?php wc_cart_totals_order_total_html(); ?></td>
				</tr>
				
				<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>
			</tbody>		
		</table>
		</td></tr>
		<?php
		if( $memberships_in_cart ):
		?>
		<tr>
			<td colspan="2"><small class="block text-center"><?php _e( 'NO contracts to sign.<br/>You may cancel at any time.', 'woocommerce' ); ?></small></td>
		</tr>
		<?php
		endif;
		?>
	</tfoot>
</table>




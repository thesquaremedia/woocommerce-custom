<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$memberships_in_cart = false;
foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) :
	$_product = $cart_item['data'];
	$terms = get_the_terms( $_product->id, 'product_cat' );
	
	if( !empty( $terms) ):
		foreach ($terms as $term) {
			$_category_slug = $term->slug;
			//var_dump( $terms );
			if ( $_category_slug == 'memberships' ) {
				//book is in cart!
				$memberships_in_cart = true;
				break;
			}
		}
	else:
		$memberships_in_cart = false;
	endif;
	
	if( $memberships_in_cart == true)
		break;
endforeach;
?>
<div class="container p-y-2">
<?php
	wc_print_notices();
	
	do_action( 'woocommerce_before_checkout_form', $checkout );
	
	// If checkout registration is disabled and not logged in, the user cannot checkout
	if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
		echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
		return;
	}
	
	if ( $memberships_in_cart ):
	?>
	<div class="row">
		<div class="col-xs-12">
			<h2 class="text-center"><?php _e('You Will Not Be Billed During Your 14 Day Free Trial Period!'); ?></h2>
			<p class="text-center"><?php _e('Your information is required because our housekeepers do not want us to provide their contact information unless we are certain that our site visitor is seriously looking for a housekeeper.'); ?></p>
		</div>
	</div>
	<?php 
	endif;
	?>
	<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
		<div class="row mobile-reverse-sm">
			<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>
			<div class="col-xs-12 col-sm-7 box no-padding">
				<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
		
				<div class="col-xs-12 no-padding" id="customer_details">
					<div class="col-xs-12 p-x-2">
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
					</div>
		
					<div class="col-xs-12 p-x-2">
						<?php do_action( 'woocommerce_checkout_shipping' ); ?>
					</div>
					
				</div>
		
				<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
			</div>
			<?php endif; ?>
			<div class="col-xs-12 col-sm-5 p-x-3">
				<?php				
				if ( $memberships_in_cart ):
				?>
					<h3 class="order-details-title text-center"><?php _e( 'Memberships Details', 'woocommerce' ); ?></h3>
					<p class="text-center"><?php _e('After Free Trial Expires <br/>Unless Cancelled or Changed'); ?></p>
				<?php
				else:
				?>
					<h3 class="order-details-title text-center"><?php _e( 'Your order', 'woocommerce' ); ?></h3>
				<?php
				endif; 
				?>
				
			
				<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>
			
				<div id="order_review" class="woocommerce-checkout-review-order">
					<?php do_action( 'woocommerce_checkout_order_review' ); ?>
				</div>
				<h3 class="order-details-title text-center green-text"><?php _e( 'Your Information is Safe', 'woocommerce' ); ?></h3>
				<p class="text-center"><?php _e('We will not sell or rent your personal contact information for any marketing purposes.'); ?></p>
				<h3 class="order-details-title text-center green-text"><?php _e( 'Secure Checkout', 'woocommerce' ); ?></h3>
				<p class="text-center"><?php _e('All your information is encrypted and transmitted without risk using a 256-bit encryption SSL (Secure Sockets Layer) certificate.'); ?></p>
				<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
			</div>
		</div>
	</form>
	<?php
	if ( $memberships_in_cart ):
	?>
	<div class="row">
		<div class="col-xs-12">
			<h2 class="text-center"><?php _e('You Will Not Be Billed During Your 14 Day Free Trial Period!'); ?></h2>
			<p class="text-center"><?php _e('You may search and speak to our housekeepers free, and your credit card will not be billed during your 14 Day Free Trial Period, unless you schedule our housekeeper during the Free Trial, and it will also not be billed if you select to cancel at any time before the Free Trial Period ends.Your information is required because our housekeepers do not want us to provide their contact information unless we are certain that our site visitor is seriously looking for a housekeeper.'); ?></p>
			<h2 class="text-center"><?php _e('No Contracts to Sign. You May Cancel Any Time.'); ?></h2>
			<p class="text-center"><?php _e('After your Free Trial Period ends you agree, unless you cancel, that your credit card will be charged according to the Plan you selected. You also agree that your subscription will automatically renew, unless you cancel your membership before it renews, and you authorize us to charge your credit card on file, plus any applicable sales tax.'); ?></p>
		</div>
	</div>
	<?php 
	endif;
	?>
	<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
</div>

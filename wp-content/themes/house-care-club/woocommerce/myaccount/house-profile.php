<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account-dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woothemes.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $current_user;

//var_dump( $_POST );
?>

<div class="col-xs-12 box">
				<div class="col-xs-12 p-y-2 p-x-2">
					<?php 
						$housekeeper_id = get_user_meta($current_user->ID, 'housekeper_to_contact', true);
						$housekeeper_data = get_term_meta( $housekeeper_id, 'vendor_data', true);
						//var_dump( $housekeeper_data );
						
					?>
					<h2 class="text-center"><?php if ( !empty( $housekeeper_data['name'] ) ) : printf( __( 'Thank You For Your Interest In %s\'s Profile.', HCC_TEXTDOMAIN ), $housekeeper_data['name'] ); else: _e( 'Thank You For Your Interest.', HCC_TEXTDOMAIN ); endif; ?></h2>
					<p class="text-center"><?php _e( 'Please take a few minutes to answer the questions below, so we have more information about your home before someone speaks with you.', HCC_TEXTDOMAIN ); ?></p>
					<h4 class="text-center uppercase"><?php _e('Please Answer the Following Questions:', HCC_TEXTDOMAIN ); ?></h4>
				</div>
				<?php do_action( 'hcc_house_profile_form', $housekeeper_data ); ?>
			</div>

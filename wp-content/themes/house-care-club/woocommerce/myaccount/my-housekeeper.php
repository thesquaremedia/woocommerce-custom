<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account-dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woothemes.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $current_user;

//var_dump( $_POST );
?>

<div class="col-xs-12 housekeeper-profile">
				<?php 
				$housekeeper_id = get_user_meta($current_user->ID, 'housekeper_assigned', true);
				$housekeeper_data = get_term_meta( $housekeeper_id, 'vendor_data', true);
				//var_dump( $housekeeper_data );
				if ( !empty($housekeeper_id) ):
				?>
				<div class="col-xs-12 box">
				<div class="col-xs-12 p-y-2 p-x-2">
					<h2 class="text-center"><?php printf( __( 'Hello I\'m %s.' ), $housekeeper_data['name'] ); ?></h2>
					<p class="text-center"><?php _e( 'Please select your booking.', 'woocommerce' ); ?></p>
				</div>					
				<?php
					/**
					 * woocommerce_before_single_product hook.
					 *
					 * @hooked wc_print_notices - 10
					 */
					 do_action( 'woocommerce_before_single_product' );
				
					 if ( post_password_required() ) {
						echo get_the_password_form();
						return;
					 }
					
					$args = array(
						'post_type'             => 'product',
						'post_status'           => 'publish',
						'ignore_sticky_posts'   => true,
						'posts_per_page'        => -1,
						'meta_query'            => array(
							array(
								'key'           => '_visibility',
								'value'         => array('catalog', 'visible'),
								'compare'       => 'IN'
							)
						),
						'tax_query'             => array(
							array(
								'taxonomy'      => 'wcpv_product_vendors',
								'field'			=> 'term_id', //This is optional, as it defaults to 'term_id'
								'terms'         => $housekeeper_id,
								'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
							)
						)
					);
					$booking_product = new WP_Query($args);
					//var_dump( $booking_product );
					if ( $booking_product->have_posts() ) :
						while ( $booking_product->have_posts() ) : $booking_product->the_post();
					?>
					
					<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
						
						<?php
							/**
							 * woocommerce_before_single_product_summary hook.
							 *
							 * @hooked woocommerce_show_product_sale_flash - 10
							 * @hooked woocommerce_show_product_images - 20
							 */
							do_action( 'woocommerce_before_single_product_summary' );
						?>
					
						<div class="entry-summary col-xs-12 col-sm-8 gray-box p-2">
					
							<?php
								/**
								 * woocommerce_single_product_summary hook.
								 *
								 * @hooked woocommerce_template_single_title - 5
								 * @hooked woocommerce_template_single_rating - 10
								 * @hooked woocommerce_template_single_price - 10
								 * @hooked woocommerce_template_single_excerpt - 20
								 * @hooked woocommerce_template_single_add_to_cart - 30
								 * @hooked woocommerce_template_single_meta - 40
								 * @hooked woocommerce_template_single_sharing - 50
								 */
								do_action( 'woocommerce_single_product_summary' );
							?>
					
						</div><!-- .summary -->
					
						<?php
							/**
							 * woocommerce_after_single_product_summary hook.
							 *
							 * @hooked woocommerce_output_product_data_tabs - 10
							 * @hooked woocommerce_upsell_display - 15
							 * @hooked woocommerce_output_related_products - 20
							 */
							do_action( 'woocommerce_after_single_product_summary' );
						?>
					
						<meta itemprop="url" content="<?php the_permalink(); ?>" />
					
					</div><!-- #product-<?php the_ID(); ?> -->
					
					<?php do_action( 'woocommerce_after_single_product' ); ?>
				</div>
				<?php
				endwhile;
			else:
			?>
			<div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
				<?php esc_html_e( 'Your assigned housekeeper isn\'t bookable yet', 'woocommerce-bookings' ); ?>
			</div>
		<?php
			endif;
			wp_reset_postdata();
		?>
		</div>
		<?php
		else:
		?>
			<div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
			<?php
			$action = !empty( of_get_option('hcc_search_result_page') )		? get_permalink(of_get_option('hcc_search_result_page')) 		: get_home_url();
			?>
				<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'hcc_return_to_housekeepers_redirect', $action ) ); ?>">
					<?php esc_html_e( 'Contact A Housekeeper', 'woocommerce-bookings' ) ?>
				</a>
				<?php esc_html_e( 'A Housekeper hasn\'t been assigned yet.', 'woocommerce-bookings' ); ?>
			</div>
		<?php
		endif;
		?>
			
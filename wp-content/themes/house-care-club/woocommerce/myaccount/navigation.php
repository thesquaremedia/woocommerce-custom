<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>

<nav class="woocommerce-MyAccount-navigation box-shadow">
	<div class="list-group">
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
				<?php
				switch( $endpoint ):
					case 'dashboard':
						$icon_class = 'fa-tachometer';
					break;
					case 'house-profile':
						$icon_class = 'fa-home';
					break;
					case 'my-housekeeper':
						$icon_class = 'fa-calendar-plus-o';
					break;
					case 'my-favorites':
						$icon_class = 'fa-heart';
					break;
					case 'orders':
						$icon_class = 'fa-list-alt';
					break;
					case 'subscriptions':
						$icon_class = 'fa-calendar';
					break;
					case 'downloads':
						$icon_class = 'fa-download';
					break;
					case 'edit-address':
						$icon_class = 'fa-map-marker';
					break;
					case 'edit-account':
						$icon_class = 'fa-pencil-square-o';
					break;
					case 'bookings':
						$icon_class = 'fa-calendar-check-o';
					break;
					case 'customer-logout':
						$icon_class = 'fa-sign-out';
					break;
					default:
						$icon_class = 'fa-circle';
					break;
				endswitch; 
				
				$icon_class = apply_filters( 'hcc_woocommerce_my_account_nav_{$endpoint}_icon_class', $icon_class, $endpoint );
				?>
				
				<a class="list-group-item <?php echo wc_get_account_menu_item_classes( $endpoint ); ?>" href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><i class="fa <?php echo $icon_class; ?> fa-fw" aria-hidden="true"></i><?php echo esc_html( $label ); ?></a>
		<?php endforeach; ?>
	</div>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>

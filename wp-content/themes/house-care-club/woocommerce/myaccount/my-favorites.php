<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account-dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woothemes.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $current_user;

//var_dump( $_POST );
?>

<div class="col-xs-12 housekeeper-profile">
				<?php 
				$housekeepers = get_user_meta($current_user->ID, 'housekeper_favorites', true);
				//var_dump( $housekeepers );
				if ( !empty($housekeepers) ):
				?>
				<div class="col-xs-12 box">
				<div class="col-xs-12 p-y-2 p-x-2">
					<h2 class="text-center"><?php _e( 'My Favorite Housekeepers', HCC_TEXTDOMAIN ); ?></h2>
				</div>					
				<?php
				
					 if ( post_password_required() ) {
						echo get_the_password_form();
						return;
					 }
					
					
					foreach ( $housekeepers as $housekeeper ) :
					?>
					<ul class="housekeepers container-fluid">
					<?php include( locate_template( 'woocommerce/content-housekeeper.php' ) ); ?>
					</ul>
				<?php
				endforeach;
			wp_reset_postdata();
		?>
		</div>
		<?php
		else:
		?>
			<div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
			<?php
			$action = !empty( of_get_option('hcc_search_result_page') )		? get_permalink(of_get_option('hcc_search_result_page')) 		: get_home_url();
			?>
				<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'hcc_return_to_housekeepers_redirect', $action ) ); ?>">
					<?php esc_html_e( 'Add a Housekeeper', HCC_TEXTDOMAIN ) ?>
				</a>
				<?php esc_html_e( 'No Housekeepers favorited yet.', HCC_TEXTDOMAIN ); ?>
			</div>
		<?php
		endif;
		?>
			
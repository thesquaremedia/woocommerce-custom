<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

//global $housekeeper;

//var_dump( $housekeeper );
$housekeeper_data = get_term_meta( $housekeeper, 'vendor_data', true);
//var_dump( $housekeeper_data, $housekeeper );
?>

<li class="col-xs-12 col-sm-4">
	<a href="<?php echo esc_url( get_term_link( (int)$housekeeper, 'wcpv_product_vendors' ) ); ?>" >
	<?php if ( ! empty( $housekeeper_data['logo'] ) ) { ?>
	<figure class="col-xs-12 col-sm-11 no-float center-block no-padding"><?php echo wp_get_attachment_image( absint( $housekeeper_data['logo'] ), 'medium', false, array( 'class' => "attachment-medium size-medium img-responsive block center-block box profile-logo no-padding" ) ); ?></figure>
	<?php } ?>
	<span class="housekeeper-name"><?php echo esc_html( $housekeeper_data['name'] ); ?></span>
	</a>
</li>

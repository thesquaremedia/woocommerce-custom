<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<article id="taxonomy-<?php echo $housekeeper->term_id; ?>" class="housekeeper-profile hentry" >
		<div class="box container">
			<?php
					$vendor_data = get_term_meta( $housekeeper->term_id, 'vendor_data', true );
					//var_dump($vendor_data);
					$maid_data = get_term_meta( $housekeeper->term_id, 'maid_data', true );
					//var_dump($maid_data);
					
					$link = get_term_link( $housekeeper, 'wcpv_product_vendors' );
					?>
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<h1 class="housekeeper-title">
						<?php _e("Hello, My Name is:", HCC_TEXTDOMAIN); ?>
						<span class="housekeeper-name"><?php echo esc_html( $housekeeper->name ); ?></span> </h1>
						<figure class="col-xs-12 col-sm-11 no-float center-block">
					<?php 
					if ( ! empty( $vendor_data['logo'] ) ) : 
						echo wp_get_attachment_image( absint( $vendor_data['logo'] ), 'medium', false, array( 'class' => "attachment-medium size-medium img-responsive block center-block box profile-logo" ) );
					else:
					?>
						<img src="<?php echo get_stylesheet_directory_uri().'/images/hcc-default-profile.jpg'; ?>" class="img-responsive block center-block box profile-logo" alt="default profile" />
					<?php
					endif;
					?>
					</figure>
					<?php
							 
					$contact_text		 = __("Contact Me", HCC_TEXTDOMAIN);
					$favorites_text 	 = __("Add to Favorites", HCC_TEXTDOMAIN);
					$favorites_icon		 = ' fa-heart-o';
					$favorites_btn_class = ' blue';
					$contact_btn_class	 = '';
					$modal				 = '';
					$favData			 = '';
					$link				 = !empty( of_get_option( 'hcc_plans_page' ) ) ? get_the_permalink( of_get_option( 'hcc_plans_page' ) ) : get_site_url();
					$favlink			 = !empty( of_get_option( 'hcc_plans_page' ) ) ? get_the_permalink( of_get_option( 'hcc_plans_page' ) ) : get_site_url();
					$favorites 			 = array();
					if ( !is_user_logged_in() ):
						$link = '#login-register-popup';
						$contact_text = __("Contact Me", HCC_TEXTDOMAIN);
						$modal = ' data-toggle="modal" data-target="#login-register-modal"';
						$favData = ' data-toggle="modal" data-target="#login-register-modal"';
						$favlink = '#login-register-popup';
					else:
						$user_id = get_current_user_id();
						$assigned_housekeeper = !empty(get_user_meta($current_user->ID, 'housekeper_assigned', true)) ? get_user_meta($current_user->ID, 'housekeper_assigned', true) : false;
						$desired_housekeeper = !empty(get_user_meta($current_user->ID, 'housekeper_to_contact', true)) ? get_user_meta($current_user->ID, 'housekeper_assigned', true) : false;
						
						$favorites = !empty(get_user_meta( $user_id, 'housekeper_favorites', true )) ? get_user_meta( $user_id, 'housekeper_favorites', true ) : array();
						$favlink = '#favorites-popup';
						$favData = ' data-action="add" data-hcc-id="' . $housekeeper->term_id . '"';
						
						if( hcc_is_member() ):
							if ( $assigned_housekeeper !== FALSE ):
								$link = get_the_permalink( get_option( 'woocommerce_myaccount_page_id' ) ).'my-housekeeper/';
								$contact_text = __("Book Me", HCC_TEXTDOMAIN );
							elseif( $desired_housekeeper !== FALSE ):
								$link = get_the_permalink( get_option( 'woocommerce_myaccount_page_id' ) ).'house-profile/';
							else:
								$link = '#contact-housekeeper';
								$modal = ' data-hcc-id="' . $housekeeper->term_id . '"';
								$contact_btn_class = ' ajax-action';
							endif;
						else:
							$link = get_the_permalink( get_option( 'woocommerce_myaccount_page_id' ) );
						endif;
						
						if ( in_array( $housekeeper->term_id, $favorites ) ):
							$favorites_text		 = __("Added to Favorites", HCC_TEXTDOMAIN).'<br/><small>click again to remove from favorites</small>';
							$favorites_icon		 = ' fa-heart';
							$favorites_btn_class = ' green';
							$favData = ' data-action="remove" data-hcc-id="' . $housekeeper->term_id . '"';
						else:	
						endif;
					endif;
					
					?>
					<a id="favorites-btn" href="<?php echo esc_url( $favlink ); ?>" class="box-btn block center-block col-xs-12 col-sm-8 text-center<?php echo $favorites_btn_class; ?>"<?php echo $favData; ?>><i class="fa<?php echo $favorites_icon; ?>"></i>
					<span class="text"><?php echo $favorites_text; ?></span>
					</a>
					<a id="contact-btn" href="<?php echo esc_url( $link ); ?>" class="box-btn block center-block col-xs-12 col-sm-8 text-center<?php echo $contact_btn_class; ?>"<?php echo $modal; ?>> <?php echo $contact_text; ?> </a> </div>
				<div class="col-xs-12 col-sm-8 gray-box p-y-1">
					<div class="col-xs-12 col-sm-3">
						<div class="white-box">
							<h4 class="green-text">$66 a Visit</h4>
							<p class="description">3 hours<br />
								1 Day a Week</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="white-box">
							<h4 class="green-text">$66 a Visit</h4>
							<p class="description">3 hours<br />
								1 Day a Week</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="white-box">
							<h4 class="green-text">$66 a Visit</h4>
							<p class="description">3 hours<br />
								1 Day a Week</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3">
						<div class="white-box">
							<h4 class="green-text">$66 a Visit</h4>
							<p class="description">3 hours<br />
								1 Day a Week</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="white-box">
							<h4 class="green-text">All Payroll Taxes Paid</h4>
							<p class="description">50% - 70% lower cost Than Cleaning Companies</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="white-box">
							<h4 class="green-text">$1,000,000 Insurance</h4>
							<p class="description">For Property Damage, Worker Injury or Theft</p>
						</div>
					</div>
					<div class="col-xs-12">
						<ul class="fa-ul no-margin-left checks">
							<?php
							$language = !empty( $maid_data['language_fluency'] ) ? $maid_data['language_fluency'] : false;
							if( !empty($language) && $language && $language != 'neither'):
							?>
							<li class="col-xs-12 col-sm-4"><i class="fa fa-check" aria-hidden="true"></i>Fluent in <?php echo $language; ?></li>
							<?php
							endif;
							$max_home_size = !empty( $maid_data['max_home_size'] ) ? $maid_data['max_home_size'] : false;
							if( !empty($max_home_size) && $max_home_size ):
							?>
							<li class="col-xs-12 col-sm-4"><i class="fa fa-check" aria-hidden="true"></i>Clean houses up to <?php echo $max_home_size; ?></li>
							<?php
							endif;
							$citizen = !empty( $maid_data['citizen'] ) ? $maid_data['citizen'] : false;
							if( !empty($citizen) && $citizen == 'yes' ):
							?>
							<li class="col-xs-12 col-sm-4"><i class="fa fa-check" aria-hidden="true"></i>US Citizen</li>
							<?php
							endif;
							?>
						</ul>
					</div>
					<?php 
					$certs = !empty( $maid_data['certifications'] ) ? explode(',', $maid_data['certifications']) : false;
					if( !empty($certs) && $certs):
					?>
					<div class="col-xs-12">
						<div id="certifications" class="white-box p-y-2">
							<h3 class="text-center">Certifications & Licenses</h3>
							<div id="certifications-grid">
								<?php
								
								$how_many = count($certs);
								foreach( $certs as $cert):
									switch($cert):
										case 'First Aid Certified':
											$icon = of_get_option('hcc_fa_cert');
										break;
										case 'CPR Certified':
											$icon = of_get_option('hcc_cpr_cert');
										break;
										case 'Registered Nurse':
											$icon = of_get_option('hcc_hn_cert');
										break;
										case 'Teaching Degree':
											$icon = of_get_option('hcc_ta_cert');
										break;
										case 'Pet first Aid':
											$icon = of_get_option('hcc_pcpr_cert');
										break;
										case 'Pet CPR Certified':
											$icon = of_get_option('hcc_pcpr_cert');
										break;
									endswitch;
									$size		= 'full';
									$image = wp_get_attachment_image( $icon, $size, false, array( 'class' => "attachment-$size size-$size img-responsive" ) );
									$image_data = get_post($icon);
									//var_dump($image_data);
								?>
								<div class="<?php echo hcc_get_column_class( $how_many, 'col-xs-6' ); ?>">
									<figure><?php echo $image; ?>
										<figcaption><?php echo $image_data->post_excerpt; ?></figcaption>
									</figure>
								</div>
								<?php
								endforeach;
								?>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<?php 
					endif;
					?>
				</div>
			</div>
			<div class="row">
				<div id="background-screening" class="col-xs-12 green-box p-y-2 p-x-2">
					<h3 class="text-center">
						<?php _e( 'Background Screening Results', HCC_TEXTDOMAIN); ?>
					</h3>
					<div class="row">
						<div class="col-xs-12 col-md-5 pull-left">
							<table border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="65%"><p>Home Address</p></td>
										<td width="10%"><i class="fa fa-check" aria-hidden="true"></i></td>
										<td width="25%"><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Phone Number</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Email Account</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Social Security Number</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Work References</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Previous Employment</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Company Certified Trained</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Driver Insurance Policy</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Driving Accidents Last 3 Years</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-xs-12 col-md-5 pull-right">
							<table border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="65%"><p>Driving Violations for DUI</p></td>
										<td width="10%"><i class="fa fa-check" aria-hidden="true"></i></td>
										<td width="25%"><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Lawsuits Last 3 Years</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Arrest History</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>National Background Check</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Sex Offender Register</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>State/County Criminal Record</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Federal Courthouse Records</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Motor Vehicle Records</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
									<tr>
										<td><p>Driver License Number</p></td>
										<td><i class="fa fa-check" aria-hidden="true"></i></td>
										<td><strong>Verified</strong></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 p-y-2 p-x-2">
					<?php 
					if( !empty( $vendor_data['profile'] ) ):
					?>
					<h3>
						<?php _e( 'About ', HCC_TEXTDOMAIN); echo esc_html( $housekeeper->name ); ?>
					</h3>
					<?php 
						echo wpautop( $vendor_data['profile']); 
					endif;
					?></div>
				<div class="col-xs-12 col-sm-6 p-y-2 p-x-2">
					<h3>
						<?php _e( 'Reviews ', HCC_TEXTDOMAIN); ?>
					</h3>
					<?php
					$args = array(
						'post_type'             => 'product',
						'post_status'           => 'publish',
						'ignore_sticky_posts'   => true,
						'posts_per_page'        => -1,
						'meta_query'            => array(
							array(
								'key'           => '_visibility',
								'value'         => array('catalog', 'visible'),
								'compare'       => 'IN'
							)
						),
						'tax_query'             => array(
							array(
								'taxonomy'      => 'wcpv_product_vendors',
								'field'			=> 'term_id', //This is optional, as it defaults to 'term_id'
								'terms'         => $housekeeper->term_id,
								'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
							)
						)
					);
					$booking_product = new WP_Query($args);
					//var_dump( $booking_product );
					if ( $booking_product->have_posts() ) :
						while ( $booking_product->have_posts() ) : $booking_product->the_post();
							$args = array( 
									'number'		=> 1, 
									'status'		=> 'approve', 
									'post_status'	=> 'publish', 
									'post_id' 		=> get_the_ID(),
									'post_type'		=> 'product' 
							);
					
					 		$comments = get_comments( $args );
							//wp_list_comments( array( 'callback' => 'woocommerce_comments' ), $comments);
							//var_dump( $comments );
							if ( $comments ):
								foreach( $comments as $comment ):
								if ( !empty( $comment->user_id ) ):
									$comment_user = $comment->user_id;
									$user_city = !empty( get_user_meta( $comment_user, 'billing_city', true ) ) ? get_user_meta( $comment_user, 'billing_city', true ) : '';
									$user_state = !empty( get_user_meta( $comment_user, 'billing_state', true ) ) ? ', ' . get_user_meta( $comment_user, 'billing_state', true ) : '';
									
								endif;
							?>
							<div class="comment p-2">
								<p><?php echo $comment->comment_content; ?></p>
								<p class="text-right"><strong> - <?php echo $comment->comment_author; ?><br /><?php echo $user_city.$user_state; ?> - <?php echo get_comment_date( '', $comment->comment_ID ); ?></strong></p>
							</div>
							<?php
								endforeach;
							else:
							?>
								<div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
									<?php esc_html_e( 'No Reviews available', 'woocommerce-bookings' ); ?>
								</div>
							<?php
							endif;
					?>
						
					
					<?php
						endwhile;
					else:
					?>
					<div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
						<?php esc_html_e( 'No Reviews available', 'woocommerce-bookings' ); ?>
					</div>
				<?php
					endif;
					wp_reset_postdata();
				?>
				</div>
			</div>
			<div class="row">
				<div id="household" class="col-xs-12 gray-box p-y-2 p-x-2 service-row">
					<h3 class="text-center">
						<?php _e( 'Household Cleaning Services', HCC_TEXTDOMAIN); ?>
					</h3>
					<ul class="fa-ul">
						<li class="col-xs-12 col-sm-3">Green Cleaning</li>
						<li class="col-xs-12 col-sm-3">Standard Cleaning</li>
						<li class="col-xs-12 col-sm-3">Deep Cleaning</li>
						<li class="col-xs-12 col-sm-3">Inside and Outside Cleaning</li>
					</ul>
				</div>
			</div>
            <?php
			$assistant_services = !empty( $maid_data['assistant_services'] ) ? $maid_data['assistant_services'] : false;
			if( !empty($assistant_services) && $assistant_services ):
			?>
			<div class="row">
				<div id="personal-assistant" class="col-xs-12 gray-box p-y-2 p-x-2 service-row">
					<h3 class="text-center">
						<?php _e( 'Personal Assistant Services', HCC_TEXTDOMAIN); ?>
					</h3>
					<div class="row">
                    <div class="cols-xs-12 col-sm-6">
					<ul>
                    <?php
					$column_limit = 7;
					$i=0;
					//var_dump( $assistant_services );
					foreach( $assistant_services as $assistant_service ):
					//var_dump( $assistant_service );
					$i++;
					?>
						<li><?php echo $assistant_service; ?></li>
					<?php
						if( $i % $column_limit ):
						?>
                    </ul>
					</div>
					<div class="cols-xs-12 col-sm-6">
					<ul>
                        <?php
						endif;
					endforeach;
					?>
                    </ul>
					</div>
					</div>
				</div>
			</div>
            <?php
			endif;
			$transportation_services = !empty( $maid_data['transportation_services'] ) ? $maid_data['transportation_services'] : false;
			if( !empty($transportation_services) && $transportation_services ):
			?>
			<div class="row">
				<div id="transportation" class="col-xs-12 gray-box p-y-2 p-x-2 service-row">
					<h3 class="text-center">
						<?php _e( 'Services Using Housekeeper’s Car', HCC_TEXTDOMAIN); ?>
					</h3>
					<div class="col-xs-12 col-sm-8 center-block no-float">
					<div class="cols-xs-12 col-sm-6">
					<ul>
                    <?php
					$column_limit = 4;
					$i=0;
					//var_dump( $transportation_services );
					foreach( $transportation_services as $transportation_service ):
					//var_dump( $transportation_service );
					$i++;
					?>
						<li><?php echo $transportation_service; ?></li>
                    <?php
						if( $i % $column_limit ):
						?>
					</ul>
					</div>
					<div class="cols-xs-12 col-sm-6">
					<ul>
					<?php
						endif;
					endforeach;
					?>
					</ul>
					</div>
					</div>
				</div>
			</div>
            <?php
			endif;
			$child_care_services = !empty( $maid_data['child_care_services'] ) ? $maid_data['child_care_services'] : false;
			if( !empty($child_care_services) && $child_care_services ):
			?>
			<div class="row">
				<div id="mothers-helper" class="col-xs-12 gray-box p-y-2 p-x-2 service-row">
					<h3 class="text-center">
						<?php _e( 'Mother’s Helper and Child Care Services', HCC_TEXTDOMAIN); ?>
					</h3>
					<div class="row">
					<div class="cols-xs-12 col-sm-6">
						<ul>
                        <?php
						$column_limit = 4;
						$i=0;
						//var_dump( $child_care_services );
						foreach( $child_care_services as $child_care_service ):
						//var_dump( $child_care_services );
						$i++;
						?>
							<li><?php echo $child_care_service; ?></li>
                        <?php
							if( $i % $column_limit ):
							?>
						</ul>
					</div>
					<div class="cols-xs-12 col-sm-6">
						<ul>
                       		<?php
							endif;
						endforeach;
						?>
						</ul>
					</div>
					</div>
                    <?php
					$special_child_care_xp = !empty( $maid_data['special_child_care_xp'] ) ? $maid_data['special_child_care_xp'] : false;
					if( !empty($special_child_care_xp) && $special_child_care_xp && !in_array( 'None', $special_child_care_xp) ):
					?>
					<h4 class="text-center">
						<?php _e( 'Special Child Care Needs Experience', HCC_TEXTDOMAIN); ?>
					</h4>
					<div class="col-xs-12 col-sm-8 center-block no-float">
					<div class="cols-xs-12 col-sm-6">
						<ul>
                        <?php
						$column_limit = 6;
						$i=0;
						//var_dump( $special_child_care_xp );
						foreach( $special_child_care_xp as $special_child_care_xp_item ):
						//var_dump( $special_child_care_xp_item );
						$i++;
						?>
							<li><?php echo $special_child_care_xp_item; ?></li>
                        <?php
							if( $i % $column_limit ):
							?>
						</ul>
					</div>
					<div class="cols-xs-12 col-sm-6">
						<ul>
						<?php
							endif;
						endforeach;
						?>
						</ul>
					</div>
					</div>
                    <?php
					endif;
					?>
				</div>
			</div>
            <?php
			endif;
			$senior_care_services = !empty( $maid_data['senior_care_services'] ) ? $maid_data['senior_care_services'] : false;
			if( !empty($senior_care_services) && $senior_care_services && !in_array( 'None', $senior_care_services) ):
			?>
			<div class="row">
				<div id="senior-services" class="col-xs-12 gray-box p-y-2 p-x-2 service-row">
					<h3 class="text-center">
						<?php _e( 'Senior Services', HCC_TEXTDOMAIN); ?>
					</h3>
					<div class="row">
					<div class="cols-xs-12 col-sm-6">
						<ul>
                        <?php
						$column_limit = 3;
						$i=0;
						//var_dump( $senior_care_services );
						foreach( $senior_care_services as $senior_care_service ):
						//var_dump( $senior_care_service );
						$i++;
						?>
                        	<li><?php echo $senior_care_service; ?></li>
						<?php
							if( $i % $column_limit ):
							?>
						</ul>
					</div>
					<div class="cols-xs-12 col-sm-6">
						<ul>
                        <?php
							endif;
						endforeach;
						?>
						</ul>
					</div>
					</div>
                    <?php
					$special_senior_care_xp = !empty( $maid_data['special_senior_care_xp'] ) ? $maid_data['special_senior_care_xp'] : false;
					if( !empty($special_senior_care_xp) && $special_senior_care_xp && !in_array( 'None', $special_senior_care_xp) ):
					?>
					<h4 class="text-center">
						<?php _e( 'Special Senior Care Needs Experience', HCC_TEXTDOMAIN); ?>
					</h4>
					<div class="col-xs-12 col-sm-8 center-block no-float">
					<div class="cols-xs-12 col-sm-6">
						<ul>
                        <?php
						$column_limit = 7;
						$i=0;
						//var_dump( $senior_care_services );
						foreach( $special_senior_care_xp as $special_senior_care_xp_item ):
						//var_dump( $special_senior_care_xp_item );
						$i++;
						?>
                        	<li><?php echo $special_senior_care_xp_item; ?></li>
                        <?php
							if( $i % $column_limit ):
							?>
						</ul>
					</div>
					<div class="cols-xs-12 col-sm-6">
						<ul>
						<?php
							endif;
						endforeach;
						?>
						</ul>
					</div>
					</div>
                    <?php
					endif;
					?>
				</div>
			</div>
            <?php
			endif;
			$pet_care_services = !empty( $maid_data['pet_care_services'] ) ? $maid_data['pet_care_services'] : false;
			if( !empty($pet_care_services) && $pet_care_services ):
			?>
			<div class="row">
				<div id="pet-care" class="col-xs-12 gray-box p-y-2 p-x-2 service-row">
					<h3 class="text-center">
						<?php _e( 'Pet Care Services', HCC_TEXTDOMAIN); ?>
					</h3>
					<div class="row">
						<div class="cols-xs-12 col-sm-6">
							<ul>
							<?php
							$column_limit = 3;
							$i=0;
							//var_dump( $pet_care_services );
							foreach( $pet_care_services as $pet_care_service ):
							//var_dump( $pet_care_service );
							$i++;
							?>
								<li><?php echo $pet_care_service; ?></li>
							<?php
								if( $i % $column_limit ):
								?>
							</ul>
						</div>
						<div class="cols-xs-12 col-sm-6">
							<ul>
							<?php
								endif;
							endforeach;
							?>
							</ul>
						</div>
					</div>
					<h4 class="text-center">
						<?php _e( 'Added Details Regarding Pet Boarding In My Home', HCC_TEXTDOMAIN); ?>
					</h4>
					<div class="col-xs-12 col-sm-8 center-block no-float">
						<div class="cols-xs-12 col-sm-6">
							<ul>
                            	<?php
								$max_pet_size = !empty( $maid_data['max_pet_size'] ) ? $maid_data['max_pet_size'] : false;
								if( !empty($max_pet_size) && $max_pet_size ):
								?>
								<li>Will board dogs up to <?php echo $max_pet_size; ?></li>
                                <?php
								endif;
								$fenced_yard = !empty( $maid_data['fenced_yard'] ) ? $maid_data['fenced_yard'] : 'no';
								if( !empty($fenced_yard) && $fenced_yard == 'no' ):
								?>
								<li>Have fenced outside yard</li>
								<?php
								else:
								?>
                                <li>Don't Have fenced outside yard</li>
                                <?php
								endif;
								$board_pets_same_time = !empty( $maid_data['board_pets_same_time'] ) ? $maid_data['board_pets_same_time'] : 'no';
								if( !empty($board_pets_same_time) && $board_pets_same_time == 'no' ):
								?>
                                <li>Only 1 dog/cat at the same time</li>
                                <?php
								else:
								?>
                                <li>Allow more than 1 dog/cat at the same time</li>
                                <?php
								endif;
								?>
							</ul>
						</div>
						<div class="cols-xs-12 col-sm-6">
							<ul>
                            	<?php
								$house_broken = !empty( $maid_data['house_broken'] ) ? $maid_data['house_broken'] : 'no';
								if( !empty($house_broken) && $house_broken == 'no' ):
								?>
								<li>Does not have to be housebroken</li>
								<?php
								else:
								?>
                                <li>Does have to be housebroken</li>
                                <?php
								endif;
								$children_under_seven = !empty( $maid_data['children_under_seven'] ) ? $maid_data['children_under_seven'] : 'no';
								if( !empty($children_under_seven) && $children_under_seven == 'no' ):
								?>
                                <li>Don't have children under age 7</li>
								<?php
								else:
								?>
                                <li>Have children under age 7</li>
                                <?php
								endif;
								?>
                            </ul>
						</div>
					</div>
				</div>
			</div>
            <?php
			endif;
			$house_watch_services = !empty( $maid_data['house_watch_services'] ) ? $maid_data['house_watch_services'] : 'no';
			if( !empty($house_watch_services) && $house_watch_services == 'yes' ):
			?>
			<div class="row">
				<div id="house-watch" class="col-xs-12 gray-box p-y-2 p-x-2 service-row">
					<h3 class="text-center">
						<?php _e( 'House Watch Services', HCC_TEXTDOMAIN); ?>
					</h3>
					<div class="cols-xs-12 col-sm-4">
						<ul>
							<li>Monitor weather warnings</li>
							<li>Put up and take down storm shutters</li>
							<li>Check for damage</li>
							<li>Check outside lights and systems</li>
						</ul>
					</div>
					<div class="cols-xs-12 col-sm-4">
						<ul>
							<li>Pick up mail</li>
							<li>Respond to security alarms</li>
							<li>Start autos to charge batteries</li>
							<li>Run water through plumbing</li>
						</ul>
					</div>
					<div class="cols-xs-12 col-sm-4">
						<ul>
							<li>Make house look lived in</li>
							<li>Wait for service providers and supervise</li>
							<li>Check electrical and plumbing systems</li>
							<li>Periodic care while you are away</li>
						</ul>
					</div>
				</div>
			</div>
			<?php
			endif;
			?>
        </div>
		<div class="box container">
			<div class="row">
				<div id="standard-cleaning" class="col-xs-12 gray-box p-y-2 service-row">
					<h3 class="text-center">Standard Cleaning Services</h3>
					<div class="col-xs-12 col-sm-3">
						<h4>Pre-Cleaning</h4>
						<ul>
							<li>Clutter collected and placed</li>
							<li>Trash collected</li>
							<li>Laundry collected</li>
						</ul>
						<h4>All Rooms</h4>
						<ul>
							<li>Flat surface items straightened</li>
							<li>Door handles / light switches</li>
							<li>Mirrors / table tops / chairs</li>
							<li>Floors sweep / vacuum / mop</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<h4>Kitchen</h4>
						<ul>
							<li>Appliance exteriors cleaned</li>
							<li>Cabinet fronts cleaned</li>
							<li>Counter tops / backsplash cleaned</li>
							<li>Dishwasher unload and load</li>
							<li>Dish drainer washed</li>
							<li>Microwave inside / outside</li>
							<li>Pet food bowls cleaned</li>
							<li>Pots / pans hand washed</li>
							<li>Stove top, grates / knobs washed</li>
							<li>Trash emptied</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<h4>Bathroom</h4>
						<ul>
							<li>Bathtub / Jacuzzi</li>
							<li>Cabinet front cleaned</li>
							<li>Counter tops / back splash</li>
							<li>Shower and doors cleaned</li>
							<li>Mirrors</li>
							<li>Sink and faucet</li>
							<li>Toilets &amp; Bidet disinfected</li>
							<li>Toilet paper &amp; soap restocked</li>
							<li>Towels folded and hung</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<h4>Bedrooms</h4>
						<ul>
							<li>Beds - make</li>
							<li>Furniture dusted</li>
							<li>Mirrors cleaned</li>
						</ul>
						<h4>Foyer</h4>
						<ul>
							<li>Stair hand rails wiped down</li>
							<li>Stairs - sweep / vacuum stairs</li>
							<li>Sink sanitized</li>
							<li>Washer / dryer wiped down</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div id="special-cleaning" class="col-xs-12 gray-box p-y-2 service-row">
					<h3 class="text-center">Special Cleaning Services</h3>
					<h5 class="text-center">Done as needed by <?php echo esc_html( $housekeeper->name ); ?> or a House Care Club team member when requested.</h5>
					<div class="col-xs-12 col-sm-3">
						<h4>All Rooms</h4>
						<ul>
							<li>High / low dusting</li>
							<li>Clean door frames</li>
							<li>Dust all baseboards</li>
							<li>Cabinets - clean inside</li>
							<li>Computers</li>
							<li>Door frames hand wash</li>
							<li>Nic nacs - dust</li>
							<li>Clean and organize shelving</li>
							<li>Dust Ceiling fan(s)</li>
							<li>Wipe down furniture</li>
							<li>Wipe down tables / chairs</li>
							<li>Upholstery - Vacuum</li>
						</ul>
						<h4>Bathrooms</h4>
						<ul>
							<li>Cabinets – clean outside</li>
							<li>Cabinets – clean inside</li>
							<li>Closets - organize</li>
							<li>Clean medicine cabinet</li>
							<li>Shower tile - and grout</li>
							<li>Shower curtain - clean</li>
							<li>Shower door track - clean</li>
							<li>Wipe down toiletry items</li>
						</ul>
						<h4>Floors</h4>
						<ul>
							<li>Area rugs</li>
							<li>Carpet(s) shampoo</li>
							<li>Tile / marble and grout</li>
							<li>Wood floors - wax</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<h4>Bedrooms</h4>
						<ul>
							<li>Bed linens - change</li>
							<li>Drawers - clean and organize</li>
							<li>Closets – clean and organize</li>
							<li>Hang clothing</li>
							<li>Clean closet floor</li>
						</ul>
						<h4>Ceilings</h4>
						<ul>
							<li>Alarm systems / batteries</li>
							<li>Carbon monoxide / batteries</li>
							<li>Ceiling fan(s)</li>
							<li>Chandelier(s)</li>
							<li>Corners and molding</li>
							<li>Exhaust fan</li>
							<li>Light fixtures</li>
							<li>Smoke detector / batteries</li>
						</ul>
						<h4>Windows</h4>
						<ul>
							<li>Blinds – wipe down</li>
							<li>Drapes - vacuum</li>
							<li>Sliding glass inside / outside</li>
							<li>Sliding door tracks</li>
							<li>Spot clean interior windows</li>
							<li>Tracks and window sills</li>
							<li>Windows inside 1st floor</li>
						</ul>
						<h4>Dining Room</h4>
						<ul>
							<li>Clean china cabinet</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<h4>Laundry</h4>
						<ul>
							<li>Bed linens - wash</li>
							<li>Bed linens - iron</li>
							<li>Dining linens - wash</li>
							<li>Dining linens - iron</li>
							<li>Laundry – wash and dry</li>
							<li>Laundry - iron / steam as directed</li>
							<li>Laundry – fold and put away</li>
						</ul>
						<h4>Kitchen</h4>
						<ul>
							<li>Appliances - scrub clean</li>
							<li>Cabinets - wipe down</li>
							<li>Cabinets – clean inside</li>
							<li>Clean outside of appliances</li>
							<li>Dishwasher sanitize</li>
							<li>Freezer - defrost and clean</li>
							<li>Garbage disposal - sanitize</li>
							<li>Oven scrub clean / degrease</li>
							<li>Pantry - clean and organize</li>
							<li>Refrigerator - shelves / drawers</li>
							<li>Set aside expired food items</li>
							<li>Clean condiment jars</li>
						</ul>
						<h4>Garage</h4>
						<ul>
							<li>Clean cars</li>
							<li>Floors – sweep / mop / vacuum</li>
							<li>Shelving – clean and organize</li>
							<li>Walls - wash</li>
						</ul>
					</div>
					<div  class="col-xs-12 col-sm-3">
						<h4>Outside</h4>
						<ul>
							<li>Appliances</li>
							<li>Barbecue grill - clean</li>
							<li>Balcony – sweep / mop</li>
							<li>Driveways - sweep</li>
							<li>Furniture – wipe down</li>
							<li>Kitchen outside area</li>
							<li>Patio – dust ceilings</li>
							<li>Patio – dust ceiling fans</li>
							<li>Patio – clean ceiling fans</li>
							<li>Pool - add water</li>
							<li>Pool – balance chemicals</li>
							<li>Pool - clean</li>
							<li>Pool – clean filter</li>
							<li>Pool - pool toys / floats</li>
							<li>Yard – poop scoop</li>
							<li>Yard - remove minor debris</li>
							<li>Walkways - sweep</li>
						</ul>
						<h4>Exercise Room</h4>
						<ul>
							<li>Equipment - wipe down</li>
							<li>Mats - wipe down</li>
						</ul>
					</div>
					<div class="col-xs-12">
						<p>* The amount of time, equipment and specialized experience depends on the work requested. A separate proposal will be provided for approval before the above work is started.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div id="handywork" class="col-xs-12 gray-box p-y-2 service-row">
					<h3 class="text-center">This Housekeeper Will Do These Handyman Services<br />
<small>Will Be Done As Requested By Patricia or another Club Team Member at Above Hourly Rate.</small></h3>
					<h4 class="text-center">Scheduled Handyman Services</h4>
					<div class="col-xs-12 col-sm-3">
						<h4>Clean or Replace</h4>
						<ul>
							<li>AC / furnace / water filters</li>
							<li>Central Vacuum</li>
							<li>Dishwasher screens</li>
							<li>Dehumidifiers</li>
							<li>Garbage disposal</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<h4>Batteries</h4>
						<ul>
							<li>Burglar alarm</li>
							<li>CO detectors</li>
							<li>Smoke detector</li>
							<li>Garage door</li>
							<li>Thermostat</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<h4>Inspect</h4>
						<ul>
							<li>Attic for animals/leaks</li>
							<li>Drains (test/clean)</li>
							<li>Fireplace(s) (test)</li>
							<li>Light bulbs (replace)</li>
							<li>Appliance fresheners</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<h4>Miscellaneous</h4>
						<ul>
							<li>Child proof living space</li>
							<li>Furniture Assembly</li>
							<li>Picture hanging</li>
							<li>Sliding doors (lubricate)</li>
							<li>Sprinkler system (test)</li>
						</ul>
					</div>
					<div class="clearfix"></div>
					<h4 class="text-center">Inspect and Recommend Repairs Where Needed*</h4>
					<div class="col-xs-12 col-sm-3">
						<ul>
							<li>Caulking (sink / tubs / counters)</li>
							<li>Grout in showers</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<ul>
							<li>Doors and cabinets</li>
							<li>Gutters</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<ul>
							<li>Interior leaks</li>
							<li>Hose bibs</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<ul>
							<li>Sink and toilets</li>
							<li>Weather stripping</li>
						</ul>
					</div>
					<div class="clearfix"></div>
					<h4 class="text-center">*A free estimate will be provided for any repairs needed by another team member that you may want to have done</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 gray-box p-y-2 p-x-2 service-row">
					<div class="col-xs-12 col-sm-4">
						<div class="service-box green needs text-center ">
							<div class="service-box-inner"><strong>We are here to take care of all your households needs</strong>
								<hr />
								<strong>if you don't see something you need just ask and</strong><strong class="orange-text">we will get it done!</strong></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8" id="let-us-do">
						<h4 class="blue-text text-center">LET US DO - What You Don’t Want To Do!</h4>
						<div class="col-xs-12 col-sm-6">
							<h5 class="blue-text">You Can Stop…</h5>
							<ul>
								<li>Stressing about unfinished tasks</li>
								<li>Worrying about your to-do list</li>
								<li>Having your home be a second job</li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-6">
							<h5 class="blue-text">We Will Help Free You…</h5>
							<ul>
								<li>To spend time with friends or family</li>
								<li>To improve your career</li>
								<li>To do what you love</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>

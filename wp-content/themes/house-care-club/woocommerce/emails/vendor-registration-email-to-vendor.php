<?php
/**
 * Vendor registration email to vendor
 *
 * @author 		WooThemes
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<h3><?php esc_html_e( 'Hello! Thank you for registering to become a House Care Club housekeeper.', 'woocommerce-product-vendors' ); ?></h3>

<p><?php esc_html_e( 'Please Login to your account using the information below.', 'woocommerce-product-vendors' ); ?></p>

<p><?php esc_html_e( 'Once logged into your account, you will be prompted to complete your application.', 'woocommerce-product-vendors' ); ?></p>

<p><?php esc_html_e( 'Below please find your account information:', 'woocommerce-product-vendors' ); ?></p>

<ul>
	<li><?php printf( esc_html__( 'Login Address: %s', 'woocommerce-product-vendors' ), admin_url() ); ?></li>
	<li><?php printf( esc_html__( 'Login Name: %s', 'woocommerce-product-vendors' ), $user_login ); ?></li>
	<li><?php printf( esc_html__( 'Login Password: %s', 'woocommerce-product-vendors' ), $user_pass ); ?></li>
</ul>

<?php do_action( 'woocommerce_email_footer', $email ); ?>

<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $current_user;
$housekeeper_id = get_user_meta($current_user->ID, 'housekeper_assigned', true);
$housekeeper_data = get_term_meta( $housekeeper_id, 'vendor_data', true);

if ( ! empty( $housekeeper_data['logo'] ) ) : ?>
<div class="col-xs-12 col-sm-4">
	<figure class="col-xs-12 col-sm-11 no-float center-block no-padding"><?php echo wp_get_attachment_image( absint( $housekeeper_data['logo'] ), 'medium', false, array( 'class' => "attachment-medium size-medium img-responsive block center-block box profile-logo" ) ); ?></figure>
</div>
<?php
endif;
?>

<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header(); ?>

<section id="main-container" class="stage">
	<?php
				
		$housekeeper		= get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		//var_dump($housekeeper);
		// Start the loop.
			if ( ! empty( $housekeeper ) ) :
		?>
				<?php include( locate_template( 'woocommerce/content-single-housekeeper.php' ) ); ?>
	<?php
		else:
	?>
	<section class="no-results not-found container p-y-2">
		<header class="page-header">
			<h1 class="page-title">
				<?php _e( 'Nothing Found', HCC_TEXTDOMAIN ); ?>
			</h1>
		</header>
		<!-- .page-header -->
		
		<div class="page-content">
			<p>
				<?php _e( 'No Housekeepers match your search criteria.', HCC_TEXTDOMAIN ); ?>
			</p>
			<?php echo do_shortcode('[housekeeper_search]'); ?> </div>
		<!-- .page-content --> 
	</section>
	<!-- .no-results -->
	<?php
		endif;
		?>
</section>
<!-- Modal -->
<div class="modal fade" id="login-register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content p-y-2 p-x-2">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="myModalLabel"><strong>To Contact <?php echo esc_html( $housekeeper->name ); ?>,<br />
You Must Become a Free Trial Member<br />
<span class="blue-text uppercase">CREATE YOUR ACCOUNT</span></strong></h4>
			</div>
			<div class="modal-body gray-box p-y-2 p-x-2 form-horizontal"> <?php echo do_shortcode('[wp_register_form]'); ?> </div>
			<div class="modal-footer">
			<ul class="fa-ul text-left small">
				<li><i class="fa-li fa fa-asterisk" aria-hidden="true"></i>
 All items marked with an * are required.</li>
 				<li><i class="fa-li fa fa-asterisk" aria-hidden="true"></i>
 Email Address is used as User Id.</li>
				<li><i class="fa-li fa fa-asterisk" aria-hidden="true"></i>
 We will not sell, share, rent, or distribute your personal information to third parties, and will not send you any unauthorized emails.</li></ul>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content p-y-2 p-x-2">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="contactModalLabel">OH NO! There seems to be a problem.</h4>
			</div>
			<div class="modal-body gray-box p-y-2 p-x-2 form-horizontal">There was an error contacting <?php echo esc_html( $housekeeper->name ); ?> please try again later or contact us for support.</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<!-- close main container -->

<?php get_footer(); ?>

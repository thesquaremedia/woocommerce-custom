<?php
/**
 * Vendor Registration Form Template
 *
 * @version 2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<h2><?php esc_html_e( 'Housekeeper Registration', 'woocommerce-product-vendors' ); ?></h2>
<?php if( is_user_logged_in() ): ?>
<p><?php _e( 'Please confirm that you desire to become a House Care Club candidate. You will be granted access to the House Care Club registration process.', HCC_TEXTDOMAIN ); ?></p>
<?php else: ?>
<p><?php _e( 'Please fill out this registration form to register as a House Care Club candidate. You will get a confirmation email that will grant you access to the House Care Club registration process.', HCC_TEXTDOMAIN ); ?></p>
<?php endif; ?>
<p><?php _e( 'We look forward to learning more about you!', HCC_TEXTDOMAIN ); ?></p>

<form class="wcpv-shortcode-registration-form">
	
	<?php do_action( 'wcpv_registration_form_start' ); ?>

	<?php if ( ! is_user_logged_in() ) { ?>
		<div class="row">
		<p class="col-xs-12 col-md-4">
			<label for="wcpv-firstname"><?php esc_html_e( 'First Name', 'woocommerce-product-vendors' ); ?> <span class="required">*</span></label>
			<input type="text" class="input-text" name="firstname" id="wcpv-firstname" value="<?php if ( ! empty( $_POST['firstname'] ) ) echo esc_attr( trim( $_POST['firstname'] ) ); ?>" tabindex="1" />
		</p>
		
		<p class="col-xs-12 col-md-4">
			<label for="wcpv-middlename"><?php esc_html_e( 'Middle Name', 'woocommerce-product-vendors' ); ?></label>
			<input type="text" class="input-text" name="middlename" id="wcpv-middlename" value="<?php if ( ! empty( $_POST['middlename'] ) ) echo esc_attr( trim( $_POST['middlename'] ) ); ?>" tabindex="1" />
		</p>

		<p class="col-xs-12 col-md-4">
			<label for="wcpv-lastname"><?php esc_html_e( 'Last Name', 'woocommerce-product-vendors' ); ?> <span class="required">*</span></label>
			<input type="text" class="input-text" name="lastname" id="wcpv-lastname" value="<?php if ( ! empty( $_POST['lastname'] ) ) echo esc_attr( trim( $_POST['lastname'] ) ); ?>" tabindex="2" />
		</p>
		</div>
		
		
		<p class="form-row form-row-wide">
			<label for="wcpv-username"><?php esc_html_e( 'Login Username', 'woocommerce-product-vendors' ); ?> <span class="required">*</span></label>
			<input type="text" class="input-text" name="username" id="wcpv-username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( trim( $_POST['username'] ) ); ?>" tabindex="3" />
		</p>

		<p class="form-row form-row-first">
			<label for="wcpv-email"><?php esc_html_e( 'Email', 'woocommerce-product-vendors' ); ?> <span class="required">*</span></label>
			<input type="email" class="input-text" name="email" id="wcpv-email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( trim( $_POST['email'] ) ); ?>" tabindex="4" />
		</p>

		<p class="form-row form-row-last">
			<label for="wcpv-confirm-email"><?php esc_html_e( 'Confirm Email', 'woocommerce-product-vendors' ); ?> <span class="required">*</span></label>
			<input type="email" class="input-text" name="confirm_email" id="wcpv-confirm-email" value="<?php if ( ! empty( $_POST['confirm_email'] ) ) echo esc_attr( trim( $_POST['confirm_email'] ) ); ?>" tabindex="5" />
		</p>
	
	<?php } ?>
	
	<div class="clear"></div>

	<p class="form-row form-row-wide hidden-xs hidden-sm hidden-md hidden-lg">
		<label for="wcpv-vendor-vendor-name"><?php esc_html_e( 'Housekeeper Profile Name', 'woocommerce-product-vendors' ); ?> <span class="required">*</span></label>
		<input class="input-text" type="text" name="vendor_name" id="wcpv-vendor-name" value="<?php if ( is_user_logged_in() ): 
			$current_user = wp_get_current_user();
			echo $current_user->user_firstname.' '.$current_user->user_lastname;
		elseif ( ! empty( $_POST['vendor_name'] ) ): echo esc_attr( trim( $_POST['vendor_name'] ) ); endif; ?>" tabindex="6" />
		<em class="wcpv-field-note"><?php esc_html_e( 'Important: This is the name that customers see when looking at your profile.  Please choose carefully.', 'woocommerce-product-vendors' ); ?></em>
	</p>

	<p class="form-row form-row-wide  hidden-xs hidden-sm hidden-md hidden-lg">
		<label for="wcpv-vendor-description"><?php esc_html_e( 'Please write a short description of why you want to work for House Care Club.', 'woocommerce-product-vendors' ); ?> <span class="required">*</span></label>
		<textarea class="input-text" name="vendor_description" id="wcpv-vendor-description" rows="4" tabindex="7"><?php if ( ! empty( $_POST['vendor_description'] ) ) echo trim( $_POST['vendor_description'] ); ?></textarea>
	</p>

	<?php do_action( 'wcpv_registration_form' ); ?>
	
	<input type="hidden" id="wcpv-enable-bookings" name="enable_bookings" value="<?php echo esc_attr( 'yes' ); ?>" tabindex="8" />
	
	
	<p class="form-row">
		<input type="submit" class="button" name="register" value="<?php if( is_user_logged_in() ): esc_attr_e( 'I want to Become a Housekeeper', HCC_TEXTDOMAIN ); else: esc_attr_e( 'I want to Register!', 'woocommerce-product-vendors' ); endif; ?>" tabindex="8" />
	</p>

	<?php do_action( 'wcpv_registration_form_end' ); ?>
	
	<script>
	jQuery(document).ready(function(){
		jQuery('#wcpv-firstname, #wcpv-middlename, #wcpv-lastname').change(function() {
			if( jQuery('#wcpv-firstname').val() != '' ){
				jQuery( '#wcpv-vendor-name').val( jQuery( '#wcpv-firstname' ).val() );
			}
			if( jQuery('#wcpv-middlename').val() != '' ){
				jQuery( '#wcpv-vendor-name').val( jQuery( '#wcpv-firstname' ).val() + ' ' + jQuery( '#wcpv-middlename' ).val() );
			}
			if( jQuery('#wcpv-lastname').val() != '' ){
				jQuery( '#wcpv-vendor-name').val( jQuery( '#wcpv-vendor-name').val() + ' ' + jQuery( '#wcpv-lastname' ).val() );
			}
			console.log( jQuery( '#wcpv-vendor-name').val() );
		});	
		
		jQuery(document.body).on( 'wcpv_vendor_registration_on_success', function(){
			console.log( 'success triggered' );
			window.location.href = "<?php echo esc_url_raw( admin_url() ); ?>";
		});
	});
	</script>

</form>

<?php
/**
 * Setup Wizard Class
 *
 * Takes new users through some basic steps to setup their store.
 *
 * @author      WooThemes
 * @category    Admin
 * @package     WooCommerce/Admin
 * @version     2.6.0
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Rand_Admin_Setup_Wizard class.
 */
class Rand_Admin_Setup_Wizard {

	/** @var string Currenct Step */
	private $step   = '';

	/** @var array Steps for the setup wizard */
	private $steps  = array();
	
	private $current_user_id = '';
	private $maids = array();
	private $maid_id = '';
	private $vendor_data = array();
	private $maid_data = array();

	/**
	 * Hook in tabs.
	 */
	public function __construct() {
		
		if ( apply_filters( 'rand_enable_setup_wizard', true ) && current_user_can( 'manage_product' ) ) {
			add_action( 'admin_menu', array( $this, 'admin_menus' ) );
			add_action( 'admin_init', array( $this, 'setup_wizard' ) );
		}
		
		$this->current_user_id 	= get_current_user_id();
		if( !empty($this->current_user_id) && $this->current_user_id ):
			$this->maids 			= WC_Product_Vendors_Utils::get_all_vendor_data( $this->current_user_id );
			$arrayKeys				= array_keys( $this->maids );
			
			$this->maid_id 			= $this->maids[$arrayKeys[0]]['term_id'];
			$this->vendor_data 		= get_term_meta( $this->maid_id, 'vendor_data', true );
			$this->maid_data 		= get_term_meta( $this->maid_id, 'maid_data', true );
		endif;
	}

	/**
	 * Add admin menus/screens.
	 */
	public function admin_menus() {
		add_dashboard_page( '', '', 'manage_product', 'maid-setup', '' );
	}

	/**
	 * Show the setup wizard.
	 */
	public function setup_wizard() {
		if ( empty( $_GET['page'] ) || 'maid-setup' !== $_GET['page'] ) {
			return;
		}
		

		// gets the author role
		$role = get_role( 'wc_product_vendors_pending_vendor' );
	
		// This only works, because it accesses the class instance.
		// would allow the author to edit others' posts for current theme only
		$role->add_cap( 'upload_files' ); 


		
		$this->steps = array(
			'introduction' => array(
				'name'    =>  __( 'Introduction', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_introduction' ),
				'handler' => '',
			),
			'contact_info' => array(
				'name'    =>  __( 'Contact', 'house_care_club' ),
				'view'    => array( $this, 'maid_contact_info' ),
				'handler' => array( $this, 'maid_contact_info_save' )
			),
			'background_info' => array(
				'name'    =>  __( 'Background', 'house_care_club' ),
				'view'    => array( $this, 'maid_background_info' ),
				'handler' => array( $this, 'maid_background_info_save' )
			),
			'personal_info' => array(
				'name'    =>  __( 'Personal', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_personal_info' ),
				'handler' => array( $this, 'maid_setup_personal_info_save' ),
			),
			'services_info' => array(
				'name'    =>  __( 'Services', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_services_info' ),
				'handler' => array( $this, 'maid_setup_services_info_save' ),
			),
			'assistant_info' => array(
				'name'    =>  __( 'Personal Assitant', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_assistant_info' ),
				'handler' => array( $this, 'maid_setup_assistant_info_save' ),
			),
			'child_care_info' => array(
				'name'    =>  __( 'Child Care', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_child_care_info' ),
				'handler' => array( $this, 'maid_setup_child_care_info_save' ),
			),
			'senior_care_info' => array(
				'name'    =>  __( 'Senior Services', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_senior_care_info' ),
				'handler' => array( $this, 'maid_setup_senior_care_info_save' ),
			),
			'pet_care_info' => array(
				'name'    =>  __( 'Pet Care', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_pet_care_info' ),
				'handler' => array( $this, 'maid_setup_pet_care_info_save' ),
			),
			'house_watch_info' => array(
				'name'    =>  __( 'House Watch', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_house_watch_info' ),
				'handler' => array( $this, 'maid_setup_house_watch_info_save' ),
			),
			'photo_info' => array(
				'name'    =>  __( 'Photo', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_photo_info' ),
				'handler' => array( $this, 'maid_setup_photo_info_save' ),
			),
			'work_history' => array(
				'name'    =>  __( 'Work History', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_work_history_info' ),
				'handler' => array( $this, 'maid_setup_work_history_info_save' ),
			),
			'authorization' => array(
				'name'    =>  __( 'Authorization', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_authorization' ),
				'handler' => array( $this, 'maid_setup_authorization_save' ),
			),
			'ready' => array(
				'name'    =>  __( 'Ready!', 'house_care_club' ),
				'view'    => array( $this, 'maid_setup_ready' ),
				'handler' => array( $this, 'maid_setup_ready_save' ),
			)
		);
		$this->step = isset( $_GET['step'] ) ? sanitize_key( $_GET['step'] ) : current( array_keys( $this->steps ) );
		$suffix     = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		
		wp_register_script( 'jquery-block-ui', CHILD_URI . '/js/jquery.blockUI' . $suffix . '.js', array( 'jquery' ), '2.70', true );
		//wp_register_script( 'select2', WC()->plugin_url() . '/assets/js/select2/select2' . $suffix . '.js', array( 'jquery' ), '3.5.2' );
		//wp_register_script( 'rand-enhanced-select', WC()->plugin_url() . '/assets/js/admin/rand-enhanced-select' . $suffix . '.js', array( 'jquery', 'select2' ), RAND_VERSION );
		/*wp_localize_script( 'rand-enhanced-select', 'rand_enhanced_select_params', array(
			'i18n_matches_1'            => _x( 'One result is available, press enter to select it.', 'enhanced select', 'house_care_club' ),
			'i18n_matches_n'            => _x( '%qty% results are available, use up and down arrow keys to navigate.', 'enhanced select', 'house_care_club' ),
			'i18n_no_matches'           => _x( 'No matches found', 'enhanced select', 'house_care_club' ),
			'i18n_ajax_error'           => _x( 'Loading failed', 'enhanced select', 'house_care_club' ),
			'i18n_input_too_short_1'    => _x( 'Please enter 1 or more characters', 'enhanced select', 'house_care_club' ),
			'i18n_input_too_short_n'    => _x( 'Please enter %qty% or more characters', 'enhanced select', 'house_care_club' ),
			'i18n_input_too_long_1'     => _x( 'Please delete 1 character', 'enhanced select', 'house_care_club' ),
			'i18n_input_too_long_n'     => _x( 'Please delete %qty% characters', 'enhanced select', 'house_care_club' ),
			'i18n_selection_too_long_1' => _x( 'You can only select 1 item', 'enhanced select', 'house_care_club' ),
			'i18n_selection_too_long_n' => _x( 'You can only select %qty% items', 'enhanced select', 'house_care_club' ),
			'i18n_load_more'            => _x( 'Loading more results&hellip;', 'enhanced select', 'house_care_club' ),
			'i18n_searching'            => _x( 'Searching&hellip;', 'enhanced select', 'house_care_club' ),
			'ajax_url'                  => admin_url( 'admin-ajax.php' ),
			'search_products_nonce'     => wp_create_nonce( 'search-products' ),
			'search_customers_nonce'    => wp_create_nonce( 'search-customers' )
		) );*/
		wp_enqueue_style( 'open-sans-style');
		wp_enqueue_style( 'rand_admin_styles', CHILD_URI . '/css/admin.css', array(), RAND_VERSION );
		wp_enqueue_style( 'bootstrap-style', PARENT_URI . '/css/bootstrap.min.css' );
		wp_enqueue_style( 'maid-setup', CHILD_URI . '/css/maid-setup.css', array( 'dashicons', 'install' ), RAND_VERSION );
		wp_enqueue_style( 'bootstrap-datetimepicker', CHILD_URI . '/css/bootstrap-datetimepicker.min.css', array( 'dashicons', 'install' ), RAND_VERSION );
		
		if ( function_exists( 'wp_enqueue_media' ) ):
			//var_dump( did_action( 'wp_enqueue_media' ) );
			wp_enqueue_media();
			//var_dump( did_action( 'wp_enqueue_media' ) );
			add_thickbox();
		endif;
		
		wp_register_script( 'bootstrap', PARENT_URI . '/js/bootstrap.min.js', array( 'jquery' ), RAND_VERSION );
		wp_register_script( 'moment', CHILD_URI . '/js/moment.min.js', array( 'jquery','bootstrap' ), RAND_VERSION );
		wp_register_script( 'bootstrap-datetimepicker', CHILD_URI . '/js/bootstrap-datetimepicker.min.js', array( 'jquery','bootstrap','moment' ), RAND_VERSION );
		wp_register_script( 'masked-input', CHILD_URI . '/js/jquery.maskedinput.min.js', array( 'jquery','bootstrap','moment' ), RAND_VERSION );
		wp_register_script( 'maid-setup', CHILD_URI . '/js/maid-setup.js', array( 'jquery','media-upload','media-editor','thickbox','bootstrap','moment','bootstrap-datetimepicker', 'masked-input','jquery-block-ui' ), RAND_VERSION );
		wp_localize_script( 'maid-setup', 'maid_setup_local', array(
			'modalLogoTitle'            => __( 'Add Profile Photo', RM_TEXT_DOMAIN ),
			'buttonLogoText'            => __( 'Add Profile Photo', RM_TEXT_DOMAIN ),
			'CHILD_URI'					=> CHILD_URI,
		) );
		
		if ( ( !empty($_POST['save_step']) || !empty($_POST['save_step_skip']) || !empty($_POST['save_step_next']) || !empty($_POST['save_step_prev']) ) && isset( $this->steps[ $this->step ]['handler'] ) ) {
			call_user_func( $this->steps[ $this->step ]['handler'] );
		}
		
		$this->vendor_data 		= get_term_meta( $this->maid_id, 'vendor_data', true );
		$this->maid_data 		= get_term_meta( $this->maid_id, 'maid_data', true );
		
		ob_start();
		$this->setup_wizard_header();
		$this->setup_wizard_steps();
		$this->setup_wizard_content();
		$this->setup_wizard_footer();
		exit;
	}

	public function get_step_link( $step ){
		$keys = array_keys( $this->steps );
		return add_query_arg( 'step', $keys[ array_search( $step, array_keys( $this->steps ) ) ] );	
	}
	public function get_next_step_link( ) {
		$keys = array_keys( $this->steps );
		return add_query_arg( 'step', $keys[ array_search( $this->step, array_keys( $this->steps ) ) + 1 ] );
	}
	public function get_previous_step_link() {
		$keys = array_keys( $this->steps );
		return add_query_arg( 'step', $keys[ array_search( $this->step, array_keys( $this->steps ) ) - 1 ] );
	}

	/**
	 * Setup Wizard Header.
	 */
	public function setup_wizard_header() {
		?>
		<!DOCTYPE html>
		<html <?php language_attributes(); ?>>
		<head>
			<meta name="viewport" content="width=device-width" />
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title><?php _e( 'Maid &rsaquo; Setup Wizard', 'house_care_club' ); ?></title>
			<?php do_action( 'admin_print_styles' ); ?>
			<?php wp_print_scripts( 'maid-setup' ); ?>
			<?php do_action( 'admin_head' ); ?>
		</head>
		<body class="maid-setup wp-core-ui">
		<div id="wrapper">
			<header id="header" class="container">
				<h1 id="maid-setup-logo"><a href="#"><?php if( of_get_option( 'logo' ) ):
						$logo_id	= of_get_option( 'logo' );
						$size		= 'full';
						echo wp_get_attachment_image( $logo_id, $size, false, array( 'class' => "attachment-$size size-$size img-responsive logo" ) ); 
					else:	
						echo bloginfo( 'name' ); 
					endif; ?></a></h1>
			</header>
		<?php
	}

	/**
	 * Setup Wizard Footer.
	 */
	public function setup_wizard_footer() {
		?>
			<footer id="footer">
				<p class="text-center">© 2016 HOUSE CARE CLUB. ALL RIGHTS RESERVED.</p>
			</footer>
			</div>
			<?php 
			do_action( 'admin_footer', '' );
			do_action( 'admin_print_footer_scripts' );
			?>
			</body>
		</html>
		<?php
	}

	/**
	 * Output the steps.
	 */
	public function setup_wizard_steps() {
		$ouput_steps = $this->steps;
		array_shift( $ouput_steps );
		?>
		<div id="maid-setup-steps" class="row-fluid">
			<ol class="container">
				<?php 
				$i=0;
				foreach ( $ouput_steps as $step_key => $step ) : 
					$i++;
					$class 	= '';
					$icon	= $i;
					if ( $step_key === $this->step ) {
						$class 	= 'active';
					} elseif ( array_search( $this->step, array_keys( $this->steps ) ) > array_search( $step_key, array_keys( $this->steps ) ) ) {
						$class 	= 'done';
						$icon	='<i class="dashicons dashicons-yes" aria-hidden="true"></i>';
					}
				?>
					<li class="<?php echo $class; ?>">
						<a href="<?php echo esc_url( $this->get_step_link( $step_key ) ); ?>">
							<div class="title-holder">
								<span class="title"><?php echo esc_html( $step['name'] ); ?></span>
							</div>
							<div class="icon-holder">
								<span class="icon"><?php echo $icon; ?></span>
							</div>
						</a>
					</li>
				<?php endforeach; ?>
			</ol>
		</div>
		<?php
	}

	/**
	 * Output the content for the current step.
	 */
	public function setup_wizard_content() {
		echo '<div class="maid-setup-content"><div class="container">';
		call_user_func( $this->steps[ $this->step ]['view'] );
		echo '</div></div>';
	}
	
	public function dependent_class( $current_value, $value, $dependent_selector ){
		if( $current_value != $value): 
			return ' hidden-xs hidden-sm hidden-md hidden-lg dependent-'.$dependent_selector; 
		else:
			return ' dependent-'.$dependent_selector;
		endif;		
	}
	
	public function yes_no_field( $field_name, $current_value, $dependency_value = '' ){
		$yes_no_arr	= array(
						'Yes'	=> 'yes',
						'No'	=> 'no',
						);
		$classes = '';
		$data = '';
		if( $dependency_value != '' ):
			$classes .= ' has-dependencies';
			$data .= ' data-dependency-value="'.$dependency_value.'" data-selector="'.$field_name.'"';
		endif;
		foreach( $yes_no_arr as $key=>$val ):
		?>
		<label class="radio-inline">
			<input type="radio" name="maid_data[<?php echo $field_name; ?>]" id="maid-<?php echo str_replace('_','-',$field_name); ?>-<?php echo str_replace('_','-',$val); ?>"<?php if( $classes != '' ): ?>class="<?php echo $classes; ?>"<?php endif; if( $data != '' ): echo $data; endif;?> value="<?php echo $val; ?>" <?php checked( $current_value, $val , true); ?>> <?php echo $key; ?>
		</label>
		<?php
		endforeach;
	}
	
	public function radio_field( $field_name, $current_value, $options = array(), $inline = 'no' ){
		foreach( $options as $key=>$val ):
			if( $inline == 'no' ):
		?>
		<div class="radio">
		<?php
			endif;
		?>
			<label class="radio-inline">
				<input type="radio" name="maid_data[<?php echo $field_name; ?>]" id="maid-<?php echo str_replace('_','-',$field_name); ?>-<?php echo str_replace('_','-',$val); ?>" value="<?php echo $key; ?>" <?php checked( $current_value, $key , true); ?>> <?php echo $key; ?>
			</label>
		<?php
			if( $inline == 'no' ):
		?>
		</div>
		<?php
			endif;
		endforeach;
	}
	
	public function checkbox_field( $field_name, $current_value, $options = array(), $inline = 'no', $all = 'no' ){
		if( $all == 'yes' ):
			if( $options == $current_value ):
				$all_value = true;
			else:
				$all_value = false;
			endif;
			if( $inline == 'no' ):
				$label_class = 'checkbox';
		?>
		<div class="checkbox">
		<?php
			else:
				$label_class = 'radio';
			endif;
		?>
			<label class="<?php echo $label_class; ?>-inline">
				<input type="checkbox" name="maid_data_<?php echo $field_name; ?>_all" id="maid-<?php echo str_replace('_','-',$field_name); ?>-all" class="select-all" value="all" <?php checked( true, $all_value , true); ?>> Select All
			</label>
		<?php
			if( $inline == 'no' ):
		?>
		</div>
		<?php
			endif;
		endif;
		foreach( $options as $key=>$val ):
			if( $inline == 'no' ):
				$label_class = 'checkbox';
		?>
		<div class="checkbox">
		<?php
			else:
				$label_class = 'radio';
			endif;
		?>
			<label class="<?php echo $label_class; ?>-inline">
				<input type="checkbox" name="maid_data[<?php echo $field_name; ?>][]" id="maid-<?php echo str_replace('_','-',$field_name); ?>-<?php echo str_replace('_','-',$val); ?>" <?php if( $all == 'yes' || $all == 'yes-no' ): ?>data-all="maid_data_<?php echo $field_name; ?>_all"<?php endif; ?> value="<?php echo $key; ?>" <?php checked( true, in_array( $key, $current_value ) , true); ?>> <?php echo $key; ?>
			</label>
		<?php
			if( $inline == 'no' ):
		?>
		</div>
		<?php
			endif;
		endforeach;
	}
	
	public function textarea_field( $field_name, $current_value, $rows = 5 ){
		?>
		<textarea type="text" name="maid_data[<?php echo $field_name; ?>]" id="maid-<?php echo str_replace('_','-',$field_name); ?>" class="form-control" rows="<?php echo $rows; ?>" ><?php echo $current_value; ?></textarea>
		<?php
	}
	
	public function text_field( $field_name, $current_value, $type = 'text', $datepicker = 'no' ){
		?>
		<input type="<?php echo $type; ?>" name="maid_data[<?php echo $field_name; ?>]" id="maid-<?php echo str_replace('_','-',$field_name); ?>" class="form-control <?php echo $type; if($datepicker == 'yes'):?> datepicker<?php endif; ?>" value="<?php echo $current_value; ?>" />
		<?php
	}
	
	public function select_field( $field_name, $current_value, $options = array(), $class = '', $container_class = '' ){
		if ( $container_class !== '' ):
		?>
		<div class="<?php echo $container_class; ?>">
		<?php
		endif;
		?>
		<select name="maid_data[<?php echo $field_name; ?>]" id="maid-<?php echo str_replace('_','-',$field_name); ?>" class="form-control <?php echo $class; ?>" >
		<?php
		foreach( $options as $key=>$val ):
		?>
			<option value="<?php echo $val; ?>" <?php selected( $current_value, $val , true); ?>><?php echo $key; ?></option>
		<?php
		endforeach;
		?>
		</select>
		<?php
		if ( $container_class !== '' ):
		?>
		</div>
		<?php
		endif;
	}
	
	public function detect_is_ip($ip, $noIpv6 = false) {
		$flags = FILTER_FLAG_IPV4;
		if ( !$noIpv6 ):
			$flags = $flags | FILTER_FLAG_IPV6;
		endif;
		return filter_var($ip, FILTER_VALIDATE_IP, $flags) !== false;
	}
	
	public function get_the_user_ip() {
	  $ip = $_SERVER['REMOTE_ADDR'];
	
	   if (!empty($_SERVER['HTTP_CLIENT_IP'])) :
		   $ip = $_SERVER['HTTP_CLIENT_IP'];
	   elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) :
		   $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	   endif;
	   
	   return $ip;
	}
	
	/**
	 * Introduction step.
	 */
	public function maid_setup_introduction() {
		?>
		<h1><?php _e( 'Thank You for Your Interest in Joining the House Care Club Team!', 'house_care_club' ); ?></h1>
		<p><?php _e( 'Please fill out each section of this employee application. Please reference our progress bar at the top of this application to track your completion progress. Our admin staff will review your application and get back to you shortly.', 'house_care_club' ); ?></p>
		<p><?php _e( 'Thank you for your interest in joining the House Care Club Team.', 'house_care_club' ); ?></p>
		<form method="post">
			<p class="maid-setup-actions step">
				<!--<input type="submit" class="button-primary button button-large button-next" value="<?php //esc_attr_e( 'Complete', 'house_care_club' ); ?>" name="save_step" />
				<a href="<?php //echo esc_url_raw( admin_url() ); ?>" class="button button-large button-next"><?php //_e( 'Cancel', 'house_care_club' ); ?></a>-->
				<a href="<?php echo esc_url( $this->get_next_step_link() ); ?>" class="button-primary button button-large button-next"><?php _e( 'Let\'s Go!', 'maid-setup' ); ?></a>
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}
	
	public function maid_setup_introduction_save() {
		check_admin_referer( 'maid-setup' );
		$user_id = get_current_user_id();
		$maids = WC_Product_Vendors_Utils::get_all_vendor_data( $user_id );
		$arrayKeys = array_keys($maids);
		
		$term_id = $maids[$arrayKeys[0]]['term_id'];
		if ( update_term_meta( $term_id, 'maid_setup_completed', 1 ) !== FALSE ):
			wp_redirect( esc_url_raw( admin_url() ) );
			exit;
		else:
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&error=maid_setup_complete_error' ) );
			exit;
		endif;
	}
	
	
	/**
	 * Contact Info
	 */
	public function maid_contact_info() {
		//write_log( $this->maid_data );
		$state_arr 		 = array(
								"Alabama" => "Alabama",
								"Alaska" => "Alaska",
								"Arizona" => "Arizona",
								"Arkansas" => "Arkansas",
								"California" => "California",
								"Colorado" => "Colorado",
								"Connecticut" => "Connecticut",
								"Delaware" => "Delaware",
								"District of Columbia" => "District of Columbia",
								"Florida" => "Florida",
								"Georgia" => "Georgia",
								"Hawaii" => "Hawaii",
								"Idaho" => "Idaho",
								"Illinois" => "Illinois",
								"Indiana" => "Indiana",
								"Iowa" => "Iowa",
								"Kansas" => "Kansas",
								"Kentucky" => "Kentucky",
								"Louisiana" => "Louisiana",
								"Maine" => "Maine",
								"Maryland" => "Maryland",
								"Massachusetts" => "Massachusetts",
								"Michigan" => "Michigan",
								"Minnesota" => "Minnesota",
								"Mississippi" => "Mississippi",
								"Missouri" => "Missouri",
								"Montana" => "Montana",
								"Nebraska" => "Nebraska",
								"Nevada" => "Nevada",
								"New Hampshire" => "New Hampshire",
								"New Jersey" => "New Jersey",
								"New Mexico" => "New Mexico",
								"New York" => "New York",
								"North Carolina" => "North Carolina",
								"North Dakota" => "North Dakota",
								"Ohio" => "Ohio",
								"Oklahoma" => "Oklahoma",
								"Oregon" => "Oregon",
								"Pennsylvania" => "Pennsylvania",
								"Rhode Island" => "Rhode Island",
								"South Carolina" => "South Carolina",
								"South Dakota" => "South Dakota",
								"Tennessee" => "Tennessee",
								"Texas" => "Texas",
								"Utah" => "Utah",
								"Vermont" => "Vermont",
								"Virginia" => "Virginia",
								"Washington" => "Washington",
								"West Virginia" => "West Virginia",
								"Wisconsin" => "Wisconsin",
								"Wyoming" => "Wyoming"
							);
		$address 		 = !empty($this->maid_data['address'])			? $this->maid_data['address'] 			: '';
		$address_number	 = !empty($this->maid_data['address_number'])	? $this->maid_data['address_number']	: '';
		$city 			 = !empty($this->maid_data['city'])				? $this->maid_data['city'] 				: '';
		$state 			 = !empty($this->maid_data['state'])			? $this->maid_data['state'] 			: '';
		$zipcode		 = !empty($this->maid_data['zipcode'])			? $this->maid_data['zipcode'] 			: '';
		$cellphone 		 = !empty($this->maid_data['cellphone'])		? $this->maid_data['cellphone']			: '';
		$homephone		 = !empty($this->maid_data['homephone'])		? $this->maid_data['homephone'] 		: '';
		$driver_liscence = !empty($this->maid_data['driver_liscence'])	? $this->maid_data['driver_liscence']	: '';
		$vehicle		 = !empty($this->maid_data['vehicle'])			? $this->maid_data['vehicle']			: '';
		$miles			 = !empty($this->maid_data['miles'])			? $this->maid_data['miles']				: '';
		$skype			 = !empty($this->maid_data['skype'])			? $this->maid_data['skype']				: '';
		?>
		<h1><?php _e( 'Contact Information', 'house_care_club' ); ?></h1>
		<form method="post">
			<p><?php echo __( 'We take your privacy very seriously. Your application will be used solely for the purpose of considerationyou’re your employment by us or one of our affiliated companies.', 'house_care_club' ); ?></p>
			
			<div class="row">
				<div class="form-group col-xs-12 col-md-8">
					<label for="maid-address">Address</label>
					<?php $this->text_field( 'address', $address, 'text' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-4">
					<label for="maid-address-number">Suite/Apt</label>
					<?php $this->text_field( 'address_number', $address_number, 'text' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-4">
					<label for="maid-city">City</label>
					<?php $this->text_field( 'city', $city, 'text' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-4">
					<label for="maid-state">State</label>
					<?php $this->select_field( 'state', $state, $state_arr, 'enhanced' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-4">
					<label for="maid-zipcode">Zip-Code</label>
					<?php $this->text_field( 'zipcode', $zipcode, 'text' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<label for="maid-cellphone">Cell Phone</label>
					<?php $this->text_field( 'cellphone', $cellphone, 'tel' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<label for="maid-homephone">Home Phone</label>
					<?php $this->text_field( 'homephone', $homephone, 'tel' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<p class="question">Do you have a valid driver’s license in good standing?</p>
					<?php $this->yes_no_field( 'driver_liscence', $driver_liscence ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<p class="question">Do you have a vehicle to drive to a member's home for work?</p>
					<?php $this->yes_no_field( 'vehicle', $vehicle ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<label for="maid-miles">What is the maximum miles you're willing to drive to a member's home?</label>
					<div class="input-group col-xs-12 col-md-4">
						<?php $this->select_field( 
											'miles', 
											$miles, 
											array( 
												'Under 10' => 'Under 10',
												'10-25' => '10-25',
												'25-50' => '25-50',
												'50+' => '50+',											) 
										); ?>
						<label class="input-group-addon" for="maid-miles">Miles</label>
					</div>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<label for="maid-skype">Skype username if you have one</label>
					<div class="input-group col-xs-12 col-md-4">
						<?php $this->text_field( 'skype', $skype, 'text' ); ?>
					</div>
				</div>
			</div>
			
			<p class="maid-setup-actions step">
				<!--<a href="<?php// echo esc_url( $this->get_previous_step_link() ); ?>" class="button button-large button-next"><?php// _e( 'Previous Step', 'house_care_club' ); ?></a>-->
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<!--<a href="<?php// echo esc_url( $this->get_next_step_link() ); ?>" class="button button-large button-next"><?php// _e( 'Skip this step', 'house_care_club' ); ?></a>-->
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Contact Info Save.
	 */
	public function maid_contact_info_save() {
		check_admin_referer( 'maid-setup' );//check if comming through the setup

		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$post_data 						= $_POST['maid_data'];
		
		$maid_data['address']			= sanitize_text_field( $post_data['address']);
		$maid_data['address_number']	= sanitize_text_field( $post_data['address_number']);
		$maid_data['city']				= sanitize_text_field( $post_data['city']);
		$maid_data['state']				= sanitize_text_field( $post_data['state']);
		$maid_data['zipcode']			= sanitize_text_field( $post_data['zipcode']);
		$maid_data['cellphone']			= sanitize_text_field( $post_data['cellphone']);
		$maid_data['homephone']			= sanitize_text_field( $post_data['homephone']);
		$maid_data['driver_liscence']	= sanitize_text_field( $post_data['driver_liscence']);
		$maid_data['vehicle']			= sanitize_text_field( $post_data['vehicle']);
		$maid_data['miles']				= sanitize_text_field( $post_data['miles']);
		$maid_data['skype']				= sanitize_text_field( $post_data['skype']);
		
		$updating = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
		
	}

	/**
	 * Background Info.
	 */
	public function maid_background_info() {
		$court_arr 		= array( 
								'Jury Duty' => 'jury_duty',
								'Defendant' => 'defendant',
								'Plaintiff' => 'plaintiff',
								'Mediation' => 'mediation',
								'None' => 'none',
								);
		$agreement			= !empty($this->maid_data['agreement'])			? $this->maid_data['agreement']				: 'yes';
		$background_auth	= !empty($this->maid_data['background_auth'])	? $this->maid_data['background_auth']		: 'yes';
		$verify_auth		= !empty($this->maid_data['verify_auth'])		? $this->maid_data['verify_auth']			: 'yes';
		$questioned_police	= !empty($this->maid_data['questioned_police'])	? $this->maid_data['questioned_police']		: 'yes';
		$questioned_details	= !empty($this->maid_data['questioned_details'])? $this->maid_data['questioned_details']	: '';
		$court_appearences	= !empty($this->maid_data['court_appearences'])	? explode(',',$this->maid_data['court_appearences'])	: array();
		$country_arrest		= !empty($this->maid_data['country_arrest'])	? $this->maid_data['country_arrest']		: 'yes';
		$arrest_details		= !empty($this->maid_data['arrest_details'])	? $this->maid_data['arrest_details']		: '';
		?>
		<h1><?php _e( 'Background Information', 'house_care_club' ); ?></h1>
		<form method="post">
			<p><?php echo __( 'If we are interested in employing you, we will have a full background and criminal history check conducted on you.  If you make any misrepresentations in this application you will be automatically disqualified, and held, liable for any cost we incur. ', 'house_care_club' ); ?></p>
			<div class="row">
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Do you agree to provide all complete and truthful answers?</p>
					<?php $this->yes_no_field( 'agreement', $agreement ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Do you authorize us have a background and criminal records check conducted on you, at no charge to you?</p>
					<?php $this->yes_no_field( 'background_auth', $background_auth ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Do you authorize us to verify, or have verified on our behalf, all statements contained in his Application and to check information about you with your references?</p>
					<?php $this->yes_no_field( 'verify_auth', $verify_auth ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Have you ever been questioned by police about your possibly committing a crime?</p>
					<?php $this->yes_no_field( 'questioned_police', $questioned_police ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<label for="maid-questioned-police-details">If Yes, Provide Details</label>
					<?php $this->textarea_field( 'questioned_details', $questioned_details ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Tell us about any court appearances you’ve had?</p>
					<?php $this->checkbox_field( 'court_appearences', $court_appearences, $court_arr, 'yes' ); ?>
				
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Have you ever been arrested in any country?</p>
					<?php $this->yes_no_field( 'country_arrest', $country_arrest ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<label for="maid-arrest-details">If Yes, Provide Details</label>
					<?php $this->textarea_field( 'arrest_details', $arrest_details ); ?>
				</div>
				
			</div>
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Background Info Save.
	 */
	public function maid_background_info_save() {
		check_admin_referer( 'maid-setup' );

		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$post_data 						= $_POST['maid_data'];
		
		$maid_data['agreement']			= sanitize_text_field( $post_data['agreement']);
		$maid_data['background_auth']	= sanitize_text_field( $post_data['background_auth']);
		$maid_data['verify_auth']		= sanitize_text_field( $post_data['verify_auth']);
		$maid_data['questioned_police']	= sanitize_text_field( $post_data['questioned_police']);
		$maid_data['questioned_details']= sanitize_text_field( $post_data['questioned_details']);
		$maid_data['court_appearences']	= sanitize_text_field( is_array($post_data['court_appearences'])? implode( ',', $post_data['court_appearences']): $post_data['court_appearences'] );
		$maid_data['country_arrest']	= sanitize_text_field( $post_data['country_arrest']);
		$maid_data['arrest_details']	= sanitize_text_field( $post_data['arrest_details']);
		
		$updating = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
	}

	/**
	 * Personal Info.
	 */
	public function maid_setup_personal_info() {
		$gender_arr				= array( 'Male' => 'male', 'Female' => 'female' );
		$month_arr				= array(
									'January' 	=> '1',
									'Febuary' 	=> '2',
									'March' 	=> '3',
									'April' 	=> '4',
									'May' 		=> '5',
									'June' 		=> '6',
									'July' 		=> '7',
									'August' 	=> '8',
									'September' => '9',
									'October' 	=> '10',
									'November' 	=> '11',
									'December' 	=> '12',
									);
		$day_arr				= array();
		for( $d = 1; $d <= 31; $d++ ){
			$day_arr[$d] = $d;
		}
		$year_arr				= array();
		for( $y = 1900; $y <= (int)date('Y'); $y++ ){
			$year_arr[$y] = $y;
		}
		
		$language_arr			= array(
									'English'				=>'english',
									'Spanish'				=>'spanish',
									'English and Spanish'	=>'english_and_spanish',
									'Neither'				=>'neither',
									);
		$education_arr			= array(
									'High school - Graduated'					=>'high_school_graduated',
									'High school - Attended not completed'		=>'high_school_attended_not_completed',
									'Junior College - Graduated'				=>'junior_college_graduated',
									'Junior College - Attended not completed'	=>'junior_college_attended_not_completed',
									'College - Graduated'						=>'college_graduated',
									'College - Attended not completed'			=>'college_attended_not_completed',
									'Graduate School - Graduated'				=>'graduate_school_graduated',
									'Graduate School - Attended not completed'	=>'graduate_school_attended_not_completed',
									'Trade School - Graduated'					=>'trade_school_graduated',
									'Trade School - Attended not completed'		=>'trade_school_attended_not_completed',
									'None'										=>'none',
									);			
		
		$gender					= !empty($this->maid_data['gender'])			? $this->maid_data['gender']			: '';
		$dob					= !empty($this->maid_data['dob'])				? date_parse_from_format("m/d/Y", $this->maid_data['dob'])				: date('m/d/Y');
		$citizen				= !empty($this->maid_data['citizen'])			? $this->maid_data['citizen']			: '';
		$legal_employment		= !empty($this->maid_data['legal_employment'])	? $this->maid_data['legal_employment']	: '';
		$has_tax_id				= !empty($this->maid_data['has_tax_id'])		? $this->maid_data['has_tax_id']		: '';
		$has_ssn				= !empty($this->maid_data['has_ssn'])			? $this->maid_data['has_ssn']			: '';
		$has_check_account		= !empty($this->maid_data['has_check_account'])	? $this->maid_data['has_check_account']	: '';
		$language_fluency		= !empty($this->maid_data['language_fluency'])	? $this->maid_data['language_fluency']	: '';
		$other_language			= !empty($this->maid_data['other_language'])	? $this->maid_data['other_language']	: '';
		$report_income			= !empty($this->maid_data['report_income'])		? $this->maid_data['report_income']		: 'yes';
		$smoke_cigarettes 		= !empty($this->maid_data['smoke_cigarettes'])	? $this->maid_data['smoke_cigarettes']	: '';
		$smoke_agreement 		= !empty($this->maid_data['smoke_agreement'])	? $this->maid_data['smoke_agreement']	: '';
		$education_level 		= !empty($this->maid_data['education_level'])	? $this->maid_data['education_level']	: '';
		$currently_employed 	= !empty($this->maid_data['currently_employed'])? $this->maid_data['currently_employed']: '';
		$hours_employed 		= !empty($this->maid_data['hours_employed'])	? $this->maid_data['hours_employed']	: '';
		$hours_weekly 			= !empty($this->maid_data['hours_weekly'])		? $this->maid_data['hours_weekly']		: '';
		$hours_monthly	 		= !empty($this->maid_data['hours_monthly'])		? $this->maid_data['hours_monthly']		: '';
		$lowest_rate 			= !empty($this->maid_data['lowest_rate'])		? $this->maid_data['lowest_rate']		: '';
		$certifications 		= !empty($this->maid_data['certifications'])	? explode(',',$this->maid_data['certifications'])	: array();
		$other_certifications 	= !empty($this->maid_data['other_certifications'])	? str_replace(",","\r\n",$this->maid_data['other_certifications'])	: '';
		?>
		<h1><?php _e( 'Personal Info', 'house_care_club' ); ?></h1>
		<form method="post">
			<div class="row">
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Gender</p>
					<?php $this->radio_field( 'gender', $gender, $gender_arr, 'yes' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<p class="question">Please Enter your date Of birth</p>
					
					<?php $this->select_field( 'dob_month', $dob['month'], $month_arr, '', 'col-xs-12 col-md-4' ); ?>
					<?php $this->select_field( 'dob_day', $dob['day'], $day_arr, '', 'col-xs-12 col-md-4' ); ?>
					<?php $this->select_field( 'dob_year', $dob['year'], $year_arr, '', 'col-xs-12 col-md-4' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Are you a U.S. Citizen?</p>
					<?php $this->yes_no_field( 'citizen', $citizen ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Are you legally allowed to be employed in the United States?</p>
					<?php $this->yes_no_field( 'legal_employment', $legal_employment ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Do you have a Tax ID Number?</p>
					<?php $this->yes_no_field( 'has_tax_id', $has_tax_id ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Do you have a Social Security Number?</p>
					<?php $this->yes_no_field( 'has_ssn', $has_ssn ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Do you have a checking account so we can direct deposit your paychecks?</p>
					<?php $this->yes_no_field( 'has_check_account', $has_check_account ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Please select if you can speak english or spanish fluently:</p>
					<?php $this->radio_field( 'language_fluency', $language_fluency, $language_arr, 'yes' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<p class="question">Other Languages:</p>
					<?php $this->text_field( 'other_language', $other_language ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Can we report all income we pay you?</p>
					<?php $this->yes_no_field( 'report_income', $report_income ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Do you smoke cigarettes?</p>
					<?php $this->yes_no_field( 'smoke_cigarettes', $smoke_cigarettes ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Do you agree to not smoke while working inside a member’s house?</p>
					<?php $this->yes_no_field( 'smoke_agreement', $smoke_agreement ); ?>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">What is your highest level of education?</p>
					<?php $this->radio_field( 'education_level', $education_level, $education_arr ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Are you currently employed?</p>
					<?php $this->yes_no_field( 'currently_employed', $currently_employed, 'yes' ); ?>
				</div>
				<div class="form-group col-xs-12 col-md-6<?php echo $this->dependent_class( $currently_employed, 'yes', 'currently_employed' ); ?>">
					<p class="question">How many hours a week are you currently employed in total?</p>
					<div class="col-xs-12 col-md-3">
						<div class="input-group">
						<?php $this->text_field( 'hours_employed', $hours_employed, 'text' ); ?>
						<label class="input-group-addon" for="maid-hours-employed">Hours</label>
						</div>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">How many hours of employment are you looking for?</p>
					<div class="col-xs-12 col-md-3">
						<div class="input-group">
							<label class="input-group-addon" for="maid-hours-weekly">Weekly</label>
							<?php $this->text_field( 'hours_weekly', $hours_weekly, 'text' ); ?>
							<label class="input-group-addon" for="maid-hours-weekly">Hours</label>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="input-group">
							<label class="input-group-addon" for="maid-hours-monthly">Monthly</label>
							<?php $this->text_field( 'hours_monthly', $hours_monthly, 'text' ); ?>
							<label class="input-group-addon" for="maid-hours-monthly">Hours</label>
						</div>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">If we can provide all the hours of work you want, what is the lowest hourly pay rate you are willing to accept?</p>
					<div class="col-xs-12 col-md-2">
						<div class="input-group">
							<label class="input-group-addon" for="maid-lowest-rate">$</label>
							<?php $this->text_field( 'lowest_rate', $lowest_rate, 'text' ); ?>
							<label class="input-group-addon" for="maid-lowest-rate">an hour</label>
						</div>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Indicate any Licenses, Training, and/or  Memberships you have:</p>
					<div class="col-xs-12 col-md-4">
						<?php
						$cert_arr_one	= array(
											'First Aid Certified'					=> 'First Aid Certified',
											'CPR Certified'					=> 'CPR Certified',
											'Registered Nurse RN'					=> 'Registered Nurse RN',
											'Certified Nursing Assistant'					=> 'Certified Nursing Assistant',
											'Physical Therapist'					=> 'Physical Therapist',
											'Emergency Oxygen'					=> 'Emergency Oxygen',
											'Certified Home Health Aid'					=> 'Certified Home Health Aid',
											'Working with disabled'					=> 'Working with disabled',
											);
						?>
						<?php $this->checkbox_field( 'certifications', $certifications, $cert_arr_one ); ?>
					</div>
					<div class="col-xs-12 col-md-4">
						<?php
						$cert_arr_two	= array(
											'Child Development Care (CDA)'					=> 'Child Development Care (CDA)',
											'Teaching Degree'					=> 'Teaching Degree',
											'Early Childhood Education (ECE)'					=> 'Early Childhood Education (ECE)',
											'Counseling Degree'					=> 'Counseling Degree',
											'Speech Therapist'					=> 'Speech Therapist',
											'Exceptional Student Education (ESE)'					=> 'Exceptional Student Education (ESE)',
											'NAFCC Certified'					=> 'NAFCC Certified',
											'Special Needs Care'					=> 'Special Needs Care',
											);
						?>
						<?php $this->checkbox_field( 'certifications', $certifications, $cert_arr_two ); ?>
					</div>
					<div class="col-xs-12 col-md-4">
						<?php
						$cert_arr_three	= array(
											'Pet first Aid'					=> 'Pet first Aid',
											'Pet CPR Certified'					=> 'Pet CPR Certified',
											'NAPPS Member'					=> 'NAPPS Member',
											);
						?>
						<?php $this->checkbox_field( 'certifications', $certifications, $cert_arr_three ); ?>
						<div class="form-group col-xs-12">
							<label for="maid-other-certifications">Other</label>
							<p class="help-block">Enter each on a separate line</p>
							<?php $this->textarea_field( 'other_certifications', $other_certifications ); ?>
						</div>
					</div>
				</div>
			</div>
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Personal Info Save.
	 */
	public function maid_setup_personal_info_save() {
		check_admin_referer( 'maid-setup' );
		
		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$post_data 						= $_POST['maid_data'];
		
		if ( !empty($post_data['dob_month']) ):
			$maid_data['dob'] = sanitize_text_field( $post_data['dob_month']).'/'.sanitize_text_field( $post_data['dob_day']).'/'.sanitize_text_field( $post_data['dob_year']);
		endif;
		
		$maid_data['gender']				= sanitize_text_field( $post_data['gender']);
		$maid_data['citizen']				= sanitize_text_field( $post_data['citizen']);
		$maid_data['legal_employment']		= sanitize_text_field( $post_data['legal_employment']);
		$maid_data['has_tax_id']			= sanitize_text_field( $post_data['has_tax_id']);
		$maid_data['has_ssn']				= sanitize_text_field( $post_data['has_ssn']);
		$maid_data['has_check_account']		= sanitize_text_field( $post_data['has_check_account']);
		$maid_data['language_fluency']		= sanitize_text_field( $post_data['language_fluency']);
		$maid_data['other_language']		= sanitize_text_field( $post_data['other_language']);
		$maid_data['report_income']			= sanitize_text_field( $post_data['report_income']);
		$maid_data['smoke_cigarettes']		= sanitize_text_field( $post_data['smoke_cigarettes']);
		$maid_data['smoke_agreement']		= sanitize_text_field( $post_data['smoke_agreement']);
		$maid_data['education_level']		= sanitize_text_field( $post_data['education_level']);
		$maid_data['currently_employed']	= sanitize_text_field( $post_data['currently_employed']);
		$maid_data['hours_employed']		= sanitize_text_field( $post_data['hours_employed']);
		$maid_data['hours_weekly']			= sanitize_text_field( $post_data['hours_weekly']);
		$maid_data['hours_monthly']			= sanitize_text_field( $post_data['hours_monthly']);
		$maid_data['lowest_rate']			= sanitize_text_field( $post_data['lowest_rate']);
		$maid_data['certifications']		= sanitize_text_field( is_array($post_data['certifications'])? implode( ',', $post_data['certifications']): $post_data['certifications'] );
		$maid_data['other_certifications']	= sanitize_text_field( preg_replace("/\r\n|\r/", ",", $post_data['other_certifications']) );
		
		
		$updating = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}

	/**
	 * Services Info.
	 */
	public function maid_setup_services_info() {
		$services_arr 		= array( 
								'House Cleaning' => 'house_cleaning',
								'Office Cleaning' => 'office_cleaning',
								'Personal Assitant Services' => 'personal_assistant_services',
								'Child Care / Mother\'s Helper' => 'child_care_mothers_helper',
								'Special Needs Child Care' => 'special_needs_child_care',
								'Pet Care Services' => 'pet_care_services',
								'Senior Services' => 'senior services',
								'House Watch Services' => 'house_watch_services',
								);
		$types_arr 			= array( 
								'Hired directly by Client' => 'hired_directly_by_client',
								'Employed by a company that was hired by clients' => 'employed_by_company_hired_by_client', 
								'My own company' => 'my_own_company', 
								'None of the Above' => 'none_of_the_above',
								);
		$ocs_arr			= array(
								'Deep Cleaning' => 'deep_cleaning',
								'Inside and Outside Cleaning' => 'inside_outside_cleaning',
								'Green Cleaning (Training Needed)' => 'green_cleaning_training',
								'Green Cleaning (Experienced)' => 'green_cleaning_experienced',
								'None of the above' => 'none_of_the_above',
								);
		$home_size_arr		= array(
								'Under 1,000 Sq Ft.' => 'under_1000_sq_ft',
								'1,001 - 2,000 Sq. Ft.' => '1001_2000_sq_ft',
								'2,001 - 3,000 Sq. Ft.' => '2001_3000_sq_ft',
								'3,001 – 4,000 Sq. Ft.' => '3001_4000_sq_ft',
								'4001 - 5,000 Sq. Ft.' => '4001_5000_sq_ft',
								'Over 5,000 Sq. Ft.' => 'over_5000_sq_ft',
								);
		$np_ss_arr			= array(
								'All Rooms' => array(
									'Organize closets' => 'organize-closets',
									'Organize drawers' => 'organize-drawers',
									'Clean and organize shelves' => 'clean-and-organize-shelves',
									),
								'Windows' => array(
									'Blinds – wipe down' => 'blinds–wipe down',
									'Drapes - vacuum' => 'drapes-vacuum',
									'Sliding glass doors inside / outside' => 'sliding-glass-doors-inside-outside',
									'Sliding door tracks' => 'sliding-door-tracks',
									'Spot clean interior windows' => 'spot-clean-interior-windows',
									'Window Tracks and sills' => 'window-tracks-and-sills',
									'Windows inside 1st floor' => 'windows-inside-1st-floor',
									),
								'Kitchen' => array(
									'Appliances – clean inside' => 'appliances–clean-inside',
									'Cabinets – clean inside' => 'cabinets–clean-inside',
									'Freezer - defrost and clean' => 'freezer-defrost-and-clean',
									'Oven scrub clean / degrease' => 'oven-scrub-clean-degrease',
									'Pantry - clean and organize' => 'pantry-clean-and-organize',
									'Refrigerator shelves / drawers' => 'refrigerator-shelves-drawers',
									'Set aside expired food items' => 'set-aside-expired-food-items',
									),
								'Outside' => array(
									'Appliances located outside' => 'appliances-located-outside',
									'Barbecue grill - clean' => 'barbecue-grill-clean',
									'Balcony – sweep / mop' => 'balcony–sweep-mop',
									'Driveways - sweep' => 'driveways-sweep',
									'Furniture – wipe down' => 'furniture–wipe-down',
									'Clean outside kitchen' => 'clean-outside-kitchen',
									'Patio – dust ceilings' => 'patio–dust-ceilings',
									'Patio – dust ceiling fans' => 'patio–dust-ceiling-fans',
									'Patio – clean ceiling fans' => 'patio–clean-ceiling-fans',
									'Pool - add water' => 'pool-add-water',
									'Pool – balance water chemicals' => 'pool-balance-water-chemicals',
									'Pool – clean top' => 'pool–clean-top',
									'Pool – clean filter' => 'pool-clean-filter',
									'Pool - clean pool toys / floats' => 'pool-clean-pool-toys-floats',
									'Yard - remove minor debris' => 'yard-remove-minor-debris',
									'Walkways - sweep' => 'walkways-sweep',
									),
								'Floors' => array(
									'Area rugs' => 'area-rugs',
									'Carpet(s) shampoo' => 'carpet-shampoo',
									'Tile / marble and grout' => 'tile-marble-and-grout',
									'Wood floors – wax' => 'wood-floors–wax',
									),
								'Dining Room' => array(
									'Clean inside china cabinet' => 'clean-inside-china-cabinet',
									),
								'Exercise Room' => array(
									'Equipment - wipe down' => 'equipment-wipe-down',
									'Mats - wipe down' => 'mats-wipe-down',
									),
								'Ceilings' => array(
									'Replace alarm systems batteries' => 'replace-alarm-systems-batteries',
									'Replace carbon monoxide batteries' => 'replace-carbon-monoxide-batteries',
									'Ceiling fan(s)' => 'ceiling-fan',
									'Chandelier(s)' => 'chandelier',
									'Ceiling corners and molding' => 'ceiling-corners-and-molding',
									'Dust ceiling corners' => 'dust-ceiling-corners',
									'Exhaust fan' => 'exhaust-fan',
									'Light fixtures Ceiling' => 'light-fixtures-ceiling',
									'Smoke detector / batteries' => 'smoke-detector-batteries',
									),
								'Garage' => array(
									'Floors – sweep / mop / vacuum' => 'floors–sweep-mop-vacuum',
									'Shelving – clean and organize' => 'shelving–clean-and-organize',
									'Walls – wash' => 'walls–wash',
									'Wash cars' => 'wash-cars',
									),
								);
		$supplies_arr		= array(
								'None' => 'none',
								'Broom' => 'Broom',
								'Magic Eraser' => 'magic-eraser',
								'Cleaner caddy' => 'cleaner-caddy',
								'Microfiber clothes' => 'microfiber-clothes',
								'Dustpan' => 'dustpan',
								'Microfiber mop' => 'microfiber-mop',
								'Extendable duster' => 'extendable-duster',
								'Mop and bucket' => 'mop-and-bucket',
								'Gloves (Rubber)' => 'gloves-rubber',
								'Mop head replacements' => 'mop-head-replacements',
								'Iron' => 'iron',
								'Steamer' => 'steamer',
								'Grout steamer' => 'grout-steamer',
								'Toilet Brush' => 'toilet-brush',
								'Vacuum' => 'vacuum',
								);
								
		$services 					= !empty($this->maid_data['services'])					? $this->maid_data['services']					: array();
		$standard_services			= !empty($this->maid_data['standard_services'])			? $this->maid_data['standard_services']			: '';
		$other_cleaning_services	= !empty($this->maid_data['other_cleaning_services'])	? $this->maid_data['other_cleaning_services']	: array();
		$max_home_size				= !empty($this->maid_data['max_home_size'])				? $this->maid_data['max_home_size']				: '';
		$climb_ladder				= !empty($this->maid_data['climb_ladder'])				? $this->maid_data['climb_ladder']				: '';
		$move_furniture				= !empty($this->maid_data['move_furniture'])			? $this->maid_data['move_furniture']			: '';
		$special_cleaning_services	= !empty($this->maid_data['special_cleaning_services'])	? $this->maid_data['special_cleaning_services']	: array();
		$cleaning_supplies			= !empty($this->maid_data['cleaning_supplies'])			? $this->maid_data['cleaning_supplies']			: array();
		?>
		<h1><?php _e( 'Services Information', 'house_care_club' ); ?></h1>
		<form method="post">
			<p><?php  _e( 'Many of our members request additional services other than cleaning.', 'house_care_club' ); ?></p>
			<p><?php  _e( 'Many of these members are willing to pay additional money, cover expenses, and schedule many more days of service a week. Please indicate below any of the services you’re willing to provide If the payment and terms are to your satisfaction.', 'house_care_club' ); ?></p>
			<div class="row">
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th><?php _e( 'Services You Will Provide', RM_TEXT_DOMAIN); ?></th>
								<th><?php _e( 'Years of Experience', RM_TEXT_DOMAIN); ?></th>
								<th><?php _e( 'Types(s) of experience you’ve had?', RM_TEXT_DOMAIN); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;
							foreach( $services_arr as $key=>$val ):
								$service_key = array_search($key, array_column($services,'name'));
								//var_dump( $service_key );
								if ( $service_key !== FALSE ) : 
									$name	= $services[$service_key]['name'];
									$xp 	= $services[$service_key]['xp'];
									$types 	= $services[$service_key]['types'];
								else:
									$name	= '';
									$xp 	= '';
									$types	= array();
								endif;
							?>
							<tr>
								<td>
									<div class="checkbox">
									<label class="radio-inline">
										<input type="checkbox" name="maid_data[service][<?php echo $i; ?>][name]" id="maid-<?php echo str_replace('_','-',$val); ?>" value="<?php echo $key; ?>" <?php checked( $name, $key , true); ?>> <?php echo $key; ?>
									</label>
									</div>
								</td>
								<td>
									<div class="col-xs-12 col-md-6">
										<div class="input-group">
											<input type="text" name="maid_data[service][<?php echo $i; ?>][xp]" id="maid-<?php echo str_replace('_','-',$val); ?>-xp" class="form-control"value="<?php echo $xp; ?>" >
											<label class="input-group-addon" for="maid-<?php echo str_replace('_','-',$val); ?>-xp">Years</label>
										</div>
									</div>
									
								</td>
								<td>
									<?php
									foreach( $types_arr as $types_key=>$types_val ):
									?>
									<div class="checkbox">
									<label class="radio-inline">
										<input type="checkbox" name="maid_data[service][<?php echo $i; ?>][types][]" id="maid-<?php echo str_replace('_','-',$val); ?>-<?php echo str_replace('_','-',$types_val); ?>" value="<?php echo $types_key; ?>" <?php checked( true, in_array( $types_key, $types) , true); ?>> <?php echo $types_key; ?>
									</label>
									</div>
									<?php
									endforeach;
									?>
								</td>
							</tr>
							<?php
							$i++;
							endforeach;
							?>
						</tbody>
					</table>
				</div>
							
			</div>
			<div class="row">
				<div class="form-group col-xs-12 col-md-12">
					<p class="question">Are you willing to provide the following Standard Cleaning Services on every visit to our Member’s house?</p>
					<?php $this->yes_no_field( 'standard_services', $standard_services ); ?>
					<table class="table table-striped">
						<thead>
							<tr>
								<th><?php _e( 'Pre-Cleaning', RM_TEXT_DOMAIN); ?></th>
								<th><?php _e( 'Kitchen', RM_TEXT_DOMAIN); ?></th>
								<th><?php _e( 'Bathroom', RM_TEXT_DOMAIN); ?></th>
								<th><?php _e( 'Bedrooms', RM_TEXT_DOMAIN); ?></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php _e( 'Clutter collected and placed', RM_TEXT_DOMAIN); ?></th>
								<td><?php _e( 'Appliance exteriors cleaned', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Bathtub / Jacuzzi cleaned', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Beds - made', RM_TEXT_DOMAIN); ?></td>
							</tr>
							<tr>
								<td><?php _e( 'Trash collected', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Cabinet fronts cleaned', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Cabinet front wiped down', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Furniture dusted', RM_TEXT_DOMAIN); ?></td>
							</tr>
							<tr>
								<td><?php _e( 'Laundry collected', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Counter tops / backsplash cleaned', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Mirrors cleaned', RM_TEXT_DOMAIN); ?></td>
								<th><?php _e( 'Foyer', RM_TEXT_DOMAIN); ?></th>
							</tr>
							<tr>
								<th><?php _e( 'All Rooms', RM_TEXT_DOMAIN); ?></th>
								<td><?php _e( 'Dishwasher unloaded and loaded', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Counter tops / back splash wiped down', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Stair hand rails wiped down', RM_TEXT_DOMAIN); ?></td>
							</tr>
							<tr>
								<td><?php _e( 'Flat surface items straightened', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Dish drainer washed', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Shower and doors cleaned', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Stairs - sweep / vacuum stairs', RM_TEXT_DOMAIN); ?></td>
							</tr>
							<tr>
								<td><?php _e( 'Door handles / light switches wiped down', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Microwave wiped down inside / outside', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Mirrors wiped down', RM_TEXT_DOMAIN); ?></td>
								<th><?php _e( 'Laundry Room', RM_TEXT_DOMAIN); ?></th>
							</tr>
							<tr>
								<td><?php _e( 'Mirrors / table tops / chairs wiped down', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Pet food bowls cleaned', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Sink and faucet cleaned', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Sink sanitized', RM_TEXT_DOMAIN); ?></td>
							</tr>
							<tr>
								<td><?php _e( 'Floors sweep / vacuum / mop', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Pots / pans hand washed if neccessary', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Toilets & Bidet disinfected', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Washer / dryer wiped down', RM_TEXT_DOMAIN); ?></td>
							</tr>
							<tr>
								<td><?php _e( '', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Stove top, grates / knobs washed', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Toilet paper & soap restocked', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( '', RM_TEXT_DOMAIN); ?></td>
							</tr>
							<tr>
								<td><?php _e( '', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Trash emptied', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( 'Towels folded and hung', RM_TEXT_DOMAIN); ?></td>
								<td><?php _e( '', RM_TEXT_DOMAIN); ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Indicate the following Cleaning Services you are also willing to provide to our Members when requested: </p>
					<div class="col-xs-12">
						<?php $this->checkbox_field( 'other_cleaning_services', $other_cleaning_services, $ocs_arr ); ?>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Indicate the maximum size house you have experience servicing: </p>
					<div class="col-xs-12">
						<?php $this->radio_field( 'max_home_size', $max_home_size, $home_size_arr ); ?>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Will you climb on a ladder to dust high ceiling areas, or change smoke detector batteries or ceiling light bulbs? </p>
					<div class="col-xs-12">
						<?php $this->yes_no_field( 'climb_ladder', $climb_ladder ); ?>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Can you move furniture if needed to clean underneath? </p>
					<div class="col-xs-12">
						<?php $this->yes_no_field( 'move_furniture', $move_furniture ); ?>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Our members may ask us to provide any or all of the following Special Cleaning  services.  If you can’t provide a specific service we can send someone to help you when needed. <strong>Please indicate any services listed below that you are <u>NOT WILLING</u> to provide?</strong> </p>
					
						<?php
						foreach( $np_ss_arr as $section_key=>$section_value):
						?>
						<div class="col-xs-12 col-sm-4">
							<p><?php echo $section_key; ?></p>
							<?php $this->checkbox_field( 'special_cleaning_services', $special_cleaning_services, $section_value ); ?>
						</div>
						<?php
						endforeach;
						?>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Indicate the types of cleaning supplies you are willing to provide:</p>
					<div class="col-xs-12">
						<?php $this->checkbox_field( 'cleaning_supplies', $cleaning_supplies, $supplies_arr ); ?>
					</div>
				</div>
			</div>
				
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Services Info save.
	 */
	public function maid_setup_services_info_save() {
		check_admin_referer( 'maid-setup' );
		
		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$post_data 						= $_POST['maid_data'];
		$services 						= $_POST['maid_data']['service'];
		$services_data					= array();

		$i=0;
		foreach( $services as $service):
			if ( !empty( $service['name'] ) ):

				$services_data['services'][$i]['name']	= $service['name'];
				$services_data['services'][$i]['xp']	= $service['xp'];
				$services_data['services'][$i]['types'] = $service['types'];
				$i++;
			endif;
		endforeach;
		
		$maid_data['services'] 					= !empty($services_data['services'])				? $services_data['services']			 : array();
		$maid_data['standard_services'] 		= sanitize_text_field($post_data['standard_services']);
		$maid_data['other_cleaning_services'] 	= !empty($post_data['other_cleaning_services'])		? $post_data['other_cleaning_services']	 : array();
		$maid_data['max_home_size']		 		= sanitize_text_field($post_data['max_home_size']);
		$maid_data['climb_ladder']		 		= sanitize_text_field($post_data['climb_ladder']);
		$maid_data['move_furniture']		 	= sanitize_text_field($post_data['move_furniture']);
		$maid_data['special_cleaning_services'] = !empty($post_data['special_cleaning_services'])	? $post_data['special_cleaning_services']: array();
		$maid_data['cleaning_supplies']			= !empty($post_data['cleaning_supplies'])			? $post_data['cleaning_supplies']		 : array();
		
		$updating = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}
	
	/**
	 * Assistant Info.
	 */
	public function maid_setup_assistant_info() {
		$as_arr	= array(
					'Wash dishes' 								=> 'wash-dishes',
					'Meal preparation, serve, clean-up' 		=> 'meal-preparation-serve-clean-up',
					'Grocery shop / put away purchases' 		=> 'grocery-shop-put-away-purchases',
					'Schedule home services, wait / supervise' 	=> 'schedule-home-services-wait-supervise',
					'Make appointments and reservations' 		=> 'make-appointments-and-reservations',
					'Handle long wait time phone calls' 		=> 'handle-long-wait-time-phone-calls',
					'Laundry – wash and dry' 					=> 'laundry–wash-and-dry',
					'Laundry - iron / steam / fold and put away'=> 'laundry-iron-steam-fold-and-put-away',
					'Laundry –bed linens - wash' 				=> 'laundry-bed-linens-wash',
					'Laundry –bed linens - iron' 				=> 'laundry–bed-linens-iron',
					'Laundry –dining linens – wash' 			=> 'laundry–dining-linens–wash',
					'Laundry – dining linens - iron' 			=> 'Laundry–dining-linens-iron',
					'Luggage pack and unpack' 					=> 'luggage-pack-and-unpack',
					'Gift Wrapping' 							=> 'gift-wrapping',
					);
		$ts_arr	= array(
					'Run errands using your own car'		=> 'run-errands-using-your-own-car',
					'Take cars for gas, air in tires, oil'	=> 'take-cars-for-gas-air-in-tires-oil',
					'Drop off / pick children from school/ after school activities'	=> 'drop-off-pick-children-from-school-after-school-activities',
					'Drop off / pick children to medical appointments'	=> 'drop-off-pick-children-to-medical-appointments',
					'Drop off / pick up dry cleaning'			=> 'drop-off-pick-up-dry-cleaning',
					'Drop off / pick up prescriptions'			=> 'drop-off-pick-up-prescriptions',
					'Drop off / pick up car for service'		=> 'drop-off-pick-up-car-for-service',
					'Take pets for grooming'					=> 'take-pets-for-grooming',
					);
		$ca_arr	= array(
					'Air conditioning'	=> 'air-conditioning',
					'Heat'				=> 'heat',
					'Seat Belts'		=> 'seat-belts',
					'Air Bags'			=> 'air-bags',
					);
					
		$assistant_services			= !empty($this->maid_data['assistant_services'])		? $this->maid_data['assistant_services']		: array();
		$use_own_vehicle			= !empty($this->maid_data['use_own_vehicle'])			? $this->maid_data['use_own_vehicle']			: '';
		$transportation_services	= !empty($this->maid_data['transportation_services'])	? $this->maid_data['transportation_services']	: array();
		$traffic_violation			= !empty($this->maid_data['traffic_violation'])			? $this->maid_data['traffic_violation']			: '';
		$liability_insurance		= !empty($this->maid_data['liability_insurance'])		? $this->maid_data['liability_insurance']		: '';
		$liability_insurance_limit	= !empty($this->maid_data['liability_insurance_limit'])	? $this->maid_data['liability_insurance_limit']	: '';
		$accident_personal_injury	= !empty($this->maid_data['accident_personal_injury'])	? $this->maid_data['accident_personal_injury']	: '';
		$accident_date				= !empty($this->maid_data['accident_date'])				? $this->maid_data['accident_date']				: '';
		$accident_years				= isset($this->maid_data['accident_years'])				? $this->maid_data['accident_years']			: '';
		$car_manufacturer			= !empty($this->maid_data['car_manufacturer'])			? $this->maid_data['car_manufacturer']			: '';
		$car_year					= !empty($this->maid_data['car_year'])					? $this->maid_data['car_year']					: '';
		$car_accessories			= !empty($this->maid_data['car_accessories'])			? $this->maid_data['car_accessories']			: array();
		?>
		<h1><?php _e( 'Assistant Service Information', 'house_care_club' ); ?></h1>
		<form method="post">
			
			<div class="row">
				<div class="form-group col-xs-12">
					<p class="question">Select any of the following Personal Assistant Services you’re willing to provide: </p>
					<div class="col-xs-12">
						<?php $this->checkbox_field( 'assistant_services', $assistant_services, $as_arr, 'no', 'yes' ); ?>
					</div>
				</div>
			</div>
			
			<h2><?php _e( 'Services Using Your Own Vehicle', 'house_care_club' ); ?></h2>
			<div class="row">
				<div class="form-group col-xs-12">
					<p class="question">Are you willing to provide services using your own vehicle? </p>
					<?php $this->yes_no_field( 'use_own_vehicle', $use_own_vehicle); ?>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Select any of the following Transportation Services you are <u>WILLING</u> to provide if the times and compensation are good for you: </p>
					<div class="col-xs-12">
						<?php $this->checkbox_field( 'transportation_services', $transportation_services, $ts_arr, 'no', 'yes' ); ?>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Have you ever had a driving violation for Drugs or Alcohol? </p>
					<?php $this->yes_no_field( 'traffic_violation', $traffic_violation); ?>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Do you have liability Insurance? </p>
					<?php $this->yes_no_field( 'liability_insurance', $liability_insurance ); ?>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">How much is the dollar limit on the liability insurance on your vehicle? </p>
					<div class="col-xs-12 col-md-4">
						<div class="input-group">
							<label class="input-group-addon" for="maid-liability-insurance-limit">$</label>
							<?php $this->text_field( 'liability_insurance_limit', $liability_insurance_limit, 'text' ); ?>
							<label class="input-group-addon" for="maid-liability-insurance-limit">Auto liability insurance limit</label>
						</div>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">As a driver, have you ever had a driving accident that resulted in personal injury above $10,000? </p>
					<?php $this->yes_no_field( 'accident_personal_injury', $accident_personal_injury); ?>					
				</div>
				<div class="form-group col-xs-12">
					<label for="maid-accident-years">How many years has it been since you were driving and had an accident (if any)? </label>
					<div class="input-group col-xs-12 col-md-2">
						<?php $this->text_field( 'accident_years', $accident_years, 'text' ); ?>
						<label class="input-group-addon" for="maid-accident-years">Years</label>
					</div>
					<br>
					<div class="input-group col-xs-12 col-md-3">
						<label class="input-group-addon" for="maid-accident-date">Date of accident</label>
						<?php $this->text_field( 'accident_date', $accident_date, 'text', 'yes' ); ?>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Please indicate the following about your car  </p>
					<div class="input-group col-xs-12 col-md-2 clearfix">
						<label class="input-group-addon" for="maid-car-manufacturer">Manufacturer</label>
						<?php $this->text_field( 'car_manufacturer', $car_manufacturer, 'text' ); ?>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<div class="input-group col-xs-12 col-md-2 clearfix">
						<label class="input-group-addon" for="maid-car-manufacturer">Year</label>
						<?php $this->text_field( 'car_year', $car_year, 'text' ); ?>
					</div>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Accessories: </p>
					<div class="col-xs-12">
						<?php $this->checkbox_field( 'car_accessories', $car_accessories, $ca_arr ); ?>
					</div>
				</div>
			</div>
				
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Assistant Info save.
	 */
	public function maid_setup_assistant_info_save() {
		check_admin_referer( 'maid-setup' );
		
		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$post_data 						= $_POST['maid_data'];
		
		$maid_data['assistant_services']		= !empty($post_data['assistant_services'])		? $post_data['assistant_services']		: array();
		$maid_data['use_own_vehicle'] 			= sanitize_text_field($post_data['use_own_vehicle']);
		$maid_data['transportation_services']	= !empty($post_data['transportation_services'])	? $post_data['transportation_services']	: array();
		$maid_data['traffic_violation'] 		= sanitize_text_field($post_data['traffic_violation']);
		$maid_data['liability_insurance']		= !empty($post_data['liability_insurance'])		? sanitize_text_field($post_data['liability_insurance']) : '';
		$maid_data['liability_insurance_limit']	= !empty($post_data['liability_insurance_limit'])? sanitize_text_field($post_data['liability_insurance_limit']) : '';
		$maid_data['accident_personal_injury']	= !empty($post_data['accident_personal_injury'])? sanitize_text_field($post_data['accident_personal_injury']):'';
		$maid_data['accident_date']				= sanitize_text_field($post_data['accident_date']);
		$maid_data['accident_years']			= sanitize_text_field($post_data['accident_years']);
		$maid_data['car_manufacturer']			= sanitize_text_field($post_data['car_manufacturer']);
		$maid_data['car_year']					= sanitize_text_field($post_data['car_year']);
		$maid_data['car_accessories']			= !empty($post_data['car_accessories'])			? $post_data['car_accessories']			: array();
		
		$updating = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}
	
	/**
	 * Child Care Services Info.
	 */
	public function maid_setup_child_care_info() {
		$cs_arr		= array(
					'Watch children while cleaning' 	=> 'watch-children-while-cleaning',
					'After school babysitting' 			=> 'after-school-babysitting',
					'Sick children babysitting'			=> 'sick-children-babysitting',
					'School days off babysitting'		=> 'school-days-off-babysitting',
					'Mother’s helper for infants'		=> 'mothers-helper-for-infants',
					'Mother’s helper for Toddlers'		=> 'mothers-helper-for-toddlers',
					'Parent’s night out babysitting'	=> 'parents-night-out-babysitting',
					'Travel with parent help supervise'	=> 'travel-with-parent-help-supervise',
					);
		$cs_arr_two	= array(
					'Playtime/reading/TV' 			=> 'playtime-reading-tv',
					'Play educational games'		=> 'play-educational-games',
					'Help getting clothes out'		=> 'help-getting-clothes-out',
					'Prepare snacks after school'	=> 'prepare-snacks-after-school',
					'Go with parent to supervise'	=> 'go-with-parent-to-supervise',					
					);
		$scxp_arr= array(
					'None' 				=> 'none',
					'Autism' 			=> 'autism',
					'A.D.H.D' 			=> 'adhd',
					'A.D.D' 			=> 'add',
					'Completely Blind' 	=> 'completely-blind',
					'Cerebral Palsy' 	=> 'cerebral-palsy',
					'Deafness' 			=> 'deafness',
					'Dyslexia' 			=> 'dyslexia',
					'Epilepsy'		 	=> 'epilepsy',
					'Multiple Sclerosis'=> 'multiple-sclerosis',
					);
		$provide_child_care			 = !empty($this->maid_data['provide_child_care'])			? $this->maid_data['provide_child_care']		 : '';
		$child_care_services		 = !empty($this->maid_data['child_care_services'])		 	? $this->maid_data['child_care_services']		 : array();
		$swim_able					 = !empty($this->maid_data['swim_able'])					? $this->maid_data['swim_able']					 : '';
		$num_child_watch			 = !empty($this->maid_data['num_child_watch'])				? $this->maid_data['num_child_watch']			 : '';
		$twins_xp					 = !empty($this->maid_data['twins_xp'])						? $this->maid_data['twins_xp']					 : '';
		$special_child_care_xp		 = !empty($this->maid_data['special_child_care_xp'])		? $this->maid_data['special_child_care_xp']		 : array();
		$other_special_child_care_xp = !empty($this->maid_data['other_special_child_care_xp'])	? $this->maid_data['other_special_child_care_xp']: '';
		?>
		<h1><?php _e( 'Child Care Services Information', 'house_care_club' ); ?></h1>
		<form method="post">			
			<div class="row">
				<div class="form-group col-xs-12">
					<p class="question">Are you willing to provide child care services? </p>
					<div class="col-xs-12">
						<?php $this->yes_no_field( 'provide_child_care', $provide_child_care, 'yes' ); ?>
					</div>
				</div>
			</div>
			<div class="row<?php echo $this->dependent_class( $provide_child_care, 'yes', 'provide_child_care' ); ?>">
				<div class="form-group col-xs-12">
					<p class="question">If yes, which of the following services are you willing to provide?  </p>
					<div class="col-xs-6">
						<?php $this->checkbox_field( 'child_care_services', $child_care_services, $cs_arr, 'no', 'yes' ); ?>
					</div>
					<div class="col-xs-6">
						<?php $this->checkbox_field( 'child_care_services', $child_care_services, $cs_arr_two, 'no', 'yes-no' ); ?>
					</div>
				</div>
			</div>
			<div class="row<?php echo $this->dependent_class( $provide_child_care, 'yes', 'provide_child_care' ); ?>">
				<div class="form-group col-xs-12">
					<p class="question">Are you able to swim in case a child falls into a pool? </p>
					<div class="col-xs-12">
						<?php $this->yes_no_field( 'swim_able', $swim_able ); ?>
					</div>
				</div>
			</div>
			<h2 class="<?php echo $this->dependent_class( $provide_child_care, 'yes', 'provide_child_care' ); ?>"><?php _e( 'Special Child Needs Experience', 'house_care_club' ); ?></h2>
			<div class="row<?php echo $this->dependent_class( $provide_child_care, 'yes', 'provide_child_care' ); ?>">
				<div class="form-group col-xs-12">
					<p class="question">How many children are you comfortable watching at the same time? </p>
					<div class="col-xs-12">
						<?php 
						$child_arr	= array(
										'One' => 'one',
										'Two' => 'two',
										'Three'=>'three',
										'Over 3'=>'over-three',
										);
						
						$this->radio_field( 'num_child_watch', $num_child_watch, $child_arr ); ?>
					</div>
				</div>
			</div>
			<div class="row<?php echo $this->dependent_class( $provide_child_care, 'yes', 'provide_child_care' ); ?>">
				<div class="form-group col-xs-12">
					<p class="question">Do you have experience with twins/multiples? </p>
					<div class="col-xs-12">
						<?php $this->yes_no_field( 'twins_xp', $twins_xp ); ?>
					</div>
				</div>
			</div>
			<div class="row<?php echo $this->dependent_class( $provide_child_care, 'yes', 'provide_child_care' ); ?>">
				<div class="form-group col-xs-12">
					<p class="question">What types of children with special needs have you cared for?</p>
					<div class="col-xs-12">
						<?php $this->checkbox_field( 'special_child_care_xp', $special_child_care_xp, $scxp_arr ); ?>
					</div>
					<div class="col-xs-12">
					<label for="maid-other-special-child-care-xp">Other Special Needs Child Care Experience (Describe)</label>
					<?php $this->textarea_field( 'other_special_child_care_xp', $other_special_child_care_xp ); ?>
					</div>
				</div>
			</div>
				
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Child Care Services Info save.
	 */
	public function maid_setup_child_care_info_save() {
		check_admin_referer( 'maid-setup' );
		
		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$post_data 						= $_POST['maid_data'];
		
		$maid_data['provide_child_care']			= !empty($post_data['provide_child_care'])		? sanitize_text_field($post_data['provide_child_care']	)			: '';
		$maid_data['child_care_services']			= !empty($post_data['child_care_services'])		? $post_data['child_care_services']		: array();
		$maid_data['swim_able']						= !empty($post_data['swim_able'])				? $post_data['swim_able']				: '';
		$maid_data['num_child_watch']			 	= !empty($post_data['num_child_watch'])			? $post_data['num_child_watch']			: '';
		$maid_data['twins_xp']					 	= !empty($post_data['twins_xp'])				? $post_data['twins_xp']				: '';
		$maid_data['special_child_care_xp']			= !empty($post_data['special_child_care_xp'])	? $post_data['special_child_care_xp']	: array();
		$maid_data['other_special_child_care_xp']	= sanitize_text_field($post_data['other_special_child_care_xp']);
		
		$updating = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}
	
	/**
	 * Senior Companion Info.
	 */
	public function maid_setup_senior_care_info() {
		$scs_arr  = array(
					'None' 										=> 'none',
					'Accompany to movies/shopping/restaurants' 	=> 'accompany-to-movies-shopping-restaurants',
					'Board games or playing cards' 				=> 'board-games-or-playing-cards',
					'Companionship' 							=> 'companionship',
					'Family advocate' 							=> 'family-advocate',
					'Help with hobbies' 						=> 'help-with-hobbies',
					'Monitor diet and eating' 					=> 'monitor-diet-and-eating',
					);
		$scsxp_arr= array(
					'None' 					=> 'none',
					'Alzheimer’s' 			=> 'clzheimers',
					'Cerebral Palsy' 		=> 'cerebral-palsy',
					'Completely Blind' 		=> 'completely-blind',
					'Deafness' 				=> 'deafness',
					'Dementia' 				=> 'dementia',
					'Epilepsy' 				=> 'epilepsy',
					'Mental Illness' 		=> 'mental-illness',
					'Multiple Sclerosis'	=> 'multiple-sclerosis',
					'Schizophrenia' 		=> 'schizophrenia',
					'Stroke' 				=> 'stroke',
					'Wheelchair bound' 		=> 'wheelchair-bound',
					);
		$senior_care_services		  = !empty($this->maid_data['senior_care_services'])		? $this->maid_data['senior_care_services']		  : array();
		$special_senior_care_xp		  = !empty($this->maid_data['special_senior_care_xp'])		? $this->maid_data['special_senior_care_xp']	  : array();
		$other_special_senior_care_xp = !empty($this->maid_data['other_special_senior_care_xp'])? $this->maid_data['other_special_senior_care_xp']: '';
		?>
		<h1><?php _e( 'Senior Services Information', 'house_care_club' ); ?></h1>
		<form method="post">
			<div class="row">
				<div class="form-group col-xs-12">
					<p class="question">Select ALL Senior Services you are willing to provide to our members: </p>
					<div class="col-xs-12">
						<?php $this->checkbox_field( 'senior_care_services', $senior_care_services, $scs_arr, 'no', 'yes' ); ?>
					</div>
				</div>
			</div>
			<h2><?php _e( 'Senior Special Needs Experience', 'house_care_club' ); ?></h2>
			<div class="row">
				<div class="form-group col-xs-12">
					<p class="question">Check All the types of Special Needs experience you have?</p>
					<div class="col-xs-12">
						<?php $this->checkbox_field( 'special_senior_care_xp', $special_senior_care_xp, $scsxp_arr, 'no', 'yes' ); ?>
					</div>
					<div class="col-xs-12">
						<label for="maid-other-special-senior-care-xp">Other Senior Special Needs Experience (Describe)</label>
						<?php $this->textarea_field( 'other_special_senior_care_xp', $other_special_senior_care_xp ); ?>
					</div>
				</div>
			</div>
				
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Senior Companion Services Info save.
	 */
	public function maid_setup_senior_care_info_save() {
		check_admin_referer( 'maid-setup' );
		
		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$post_data 						= $_POST['maid_data'];
		
		$maid_data['senior_care_services']			= !empty($post_data['senior_care_services'])	? $post_data['senior_care_services']	: array();
		$maid_data['special_senior_care_xp']		= !empty($post_data['special_senior_care_xp'])	? $post_data['special_senior_care_xp']	: array();
		$maid_data['other_special_senior_care_xp']	= sanitize_text_field($post_data['other_special_senior_care_xp']);
		
		$updating = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}
	
	/**
	 * Pet Care Services Info.
	 */
	public function maid_setup_pet_care_info() {
		$pet_help 					= !empty($this->maid_data['pet_help'])				? $this->maid_data['pet_help']				: '';
		$pet_care_services 			= !empty($this->maid_data['pet_care_services'])		? $this->maid_data['pet_care_services']		: array();
		$add_pet_care_services 		= !empty($this->maid_data['add_pet_care_services'])	? $this->maid_data['add_pet_care_services']	: array();
		$pet_overnight 					= !empty($this->maid_data['pet_overnight'])				? $this->maid_data['pet_overnight']				: '';
		$pet_boarding 		= !empty($this->maid_data['pet_boarding'])	? $this->maid_data['pet_boarding']	: '';
		$max_pet_size 				= !empty($this->maid_data['max_pet_size'])			? $this->maid_data['max_pet_size']			: '';
		$house_broken 				= !empty($this->maid_data['house_broken'])			? $this->maid_data['house_broken']			: '';
		$fenced_yard 				= !empty($this->maid_data['fenced_yard'])			? $this->maid_data['fenced_yard']			: '';
		$children_under_seven 		= !empty($this->maid_data['children_under_seven'])	? $this->maid_data['children_under_seven']	: '';
		$board_pets_same_time 		= !empty($this->maid_data['board_pets_same_time'])	? $this->maid_data['board_pets_same_time']	: '';
		?>
		<h1><?php _e( 'Pet Care Services Information', 'house_care_club' ); ?></h1>
		<form method="post">			
			<div class="row">
				<div class="form-group col-xs-12">
					<p class="question">Are you willing to provide help with a members dog or cat?</p>
					<?php $this->yes_no_field( 'pet_help', $pet_help, 'yes'); ?>
				</div>
				<div class="form-group col-xs-12<?php echo $this->dependent_class( $pet_help, 'yes', 'pet_help' ); ?>">
					<p class="question">Select ALL Pet Care Services you ARE WILLING to provide:</p>
					<?php $this->checkbox_field( 
									'pet_care_services',
									$pet_care_services,
									array(
										'Pet Sitting - Daytime'  => 'pet-sitting-daytime',
										'30 minute walk'		 => '30-minute-walk',
										'Drop in visits'		 => 'drop-in-visits',
										'Bathing'				 => 'bathing',
										'Full Grooming'			 => 'full-grooming',
										'Dog training'			 => 'dog-training',
										'None'					 => 'none',
									)
									,'no'
									,'yes'
								); 
					?>
				</div>
				<div class="form-group col-xs-12<?php echo $this->dependent_class( $pet_help, 'yes', 'pet_help' ); ?>">
					<p class="question">Select ALL additional Pet Care Services you can provide:</p>
					<?php $this->checkbox_field( 
									'add_pet_care_services',
									$add_pet_care_services,
									array(
										'Medication Administration'  => 'medication-administration',
										'Short Notice Care'		 => 'short-notice-care',
										'None'					 => 'none',
									)
									,'no'
									,'yes'
								); 
					?>
				</div>
				<div class="form-group col-xs-12<?php echo $this->dependent_class( $pet_help, 'yes', 'pet_help' ); ?>">
					<p class="question">Would you occasionally stay overnight in member's home to care for pet?</p>
					<?php $this->yes_no_field( 'pet_overnight', $pet_overnight ); ?>
				</div>
				<div class="form-group col-xs-12<?php echo $this->dependent_class( $pet_help, 'yes', 'pet_help' ); ?>">
					<p class="question">Would you board a dog or cat at YOUR HOME?</p>
					<?php $this->yes_no_field( 'pet_boarding', $pet_boarding ); ?>
				</div>
				<div class="form-group col-xs-12<?php echo $this->dependent_class( $pet_help, 'yes', 'pet_help' ); ?>">
					<p class="question">If yes, what is the maximum size of dog willing to board in your home</p>
					<?php $this->radio_field( 
									'max_pet_size',
									$max_pet_size,
									array(
										'15 Pounds' 	=> 'fifteen-pounds',
										'40 Pounds'		=> 'forty-pounds',
										'100 Pounds' 	=> 'hundred-pounds',
										'101+ Pounds'	=> 'hundred-one-plus-pounds',
									)
								); 
					?>
				</div>
				<div class="form-group col-xs-12<?php echo $this->dependent_class( $pet_help, 'yes', 'pet_help' ); ?>">
					<p class="question">Do boarded dogs need to be house broken?</p>
					<?php $this->yes_no_field( 'house_broken', $house_broken); ?>
				</div>
				<div class="form-group col-xs-12<?php echo $this->dependent_class( $pet_help, 'yes', 'pet_help' ); ?>">
					<p class="question">Is your yard fenced?</p>
					<?php $this->yes_no_field( 'fenced_yard', $fenced_yard); ?>
				</div>
				<div class="form-group col-xs-12<?php echo $this->dependent_class( $pet_help, 'yes', 'pet_help' ); ?>">
					<p class="question">Do any children under the age of 7 live in your home?</p>
					<?php $this->yes_no_field( 'children_under_seven', $children_under_seven); ?>
				</div>
				<div class="form-group col-xs-12<?php echo $this->dependent_class( $pet_help, 'yes', 'pet_help' ); ?>">
					<p class="question">Do you board other dogs/cats at the same time?</p>
					<?php $this->yes_no_field( 'board_pets_same_time', $board_pets_same_time); ?>
				</div>
			</div>
				
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Pet Care Services Info save.
	 */
	public function maid_setup_pet_care_info_save() {
		check_admin_referer( 'maid-setup' );
		
		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$post_data 						= $_POST['maid_data'];
		
		$maid_data['pet_help'] 				= !empty($post_data['pet_help'])				? sanitize_text_field($post_data['pet_help'])		: '';			
		$maid_data['pet_overnight'] 		= !empty($post_data['pet_overnight'])			? sanitize_text_field($post_data['pet_overnight'])	: '';
		$maid_data['pet_boarding'] 			= !empty($post_data['pet_boarding'])			? sanitize_text_field($post_data['pet_boarding'])	: '';
		$maid_data['pet_care_services']		= !empty($post_data['pet_care_services'])		? $post_data['pet_care_services']					: array();
		$maid_data['add_pet_care_services'] = !empty($post_data['add_pet_care_services'])	? $post_data['add_pet_care_services']				: array();
		$maid_data['max_pet_size']			= sanitize_text_field($post_data['max_pet_size']);
		$maid_data['house_broken']			= sanitize_text_field($post_data['house_broken']);
		$maid_data['fenced_yard']			= sanitize_text_field($post_data['fenced_yard']);
		$maid_data['children_under_seven']	= sanitize_text_field($post_data['children_under_seven']);
		$maid_data['board_pets_same_time']	= sanitize_text_field($post_data['board_pets_same_time']);
		
		$updating = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}
	
	/**
	 * House Watch Services Info.
	 */
	public function maid_setup_house_watch_info() {
		$house_watch_services 	= !empty($this->maid_data['house_watch_services'])		? $this->maid_data['house_watch_services']		: array();
		?>
		<h1><?php _e( 'House Watch Services Information', 'house_care_club' ); ?></h1>
		<form method="post">			
			<div class="row">
				<div class="form-group col-xs-12">
					<p class="question"><?php _e( 'Indicate all the following House Watch Services you are willing to provide for a house if you are cleaning the house while the homeowner travels out of town.', 'house_care_club' ); ?></p>
					<div class="col-xs-12 col-md-6">
						<?php $this->checkbox_field( 'house_watch_services', $house_watch_services,
												array(
													'None'=>'None',
													'Monitor weather warnings'=>'monitor-weather-warnings',
													'Check for damage'=>'check-for-damage',
													'Check outside lights and systems'=>'check-outside-lights-and-systems',
													'Run water through plumbing'=>'run-water-through-plumbing',
													'Wait for service providers and supervise'=>'wait-for-service-providers-and-supervise',
													),
												'no'
												, 'yes'
												); ?>
					</div>
					<div class="col-xs-12 col-md-6">
						<?php $this->checkbox_field( 'house_watch_services', $house_watch_services,
												array(
													'Pick up mail'=>'pick-up-mail',
													'Respond to security alarms'=>'respond-to-security-alarms',
													'Start autos to charge batteries'=>'start-autos-to-charge-batteries',
													'Make house look lived in'=>'make-house-look-lived-in',
													'Check electrical and plumbing systems'=>'check-electrical-and-plumbing-systems',
													),
												'no'
												, 'yes-no'
												); ?>
					</div>					
				</div>
			</div>
				
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Assistant Info save.
	 */
	public function maid_setup_house_watch_info_save() {
		check_admin_referer( 'maid-setup' );
		
		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$post_data 						= $_POST['maid_data'];
		
		$maid_data['house_watch_services']	= !empty($post_data['house_watch_services']) ? $post_data['house_watch_services']: array();
		
		$updating = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}
	
	/**
	 * Photo Info.
	 */
	public function maid_setup_photo_info() {
		//var_dump($this->vendor_data);
		$hide_remove_image_link = '';
		
		$logo						= !empty($this->vendor_data['logo'])					? $this->vendor_data['logo']						: '';
		
		$logo_image_url = wp_get_attachment_image_src( $logo, 'full' );
		
		if ( empty( $logo_image_url ) ) {
			$hide_remove_image_link = 'display:none;';
		}

		?>
		<h1><?php _e( 'Photo Required', 'house_care_club' ); ?></h1>
		<form method="post">
			<div class="row">
				<div class="form-group col-xs-12 col-sm-4">
					<a href="#" class="maid-remove-image dashicons dashicons-no" style="<?php echo esc_attr( $hide_remove_image_link ); ?>" title="<?php esc_attr_e( 'Click to remove image', RM_TEXT_DOMAIN ); ?>"></a>
					<?php if ( is_array( $logo_image_url ) && ! empty( $logo_image_url ) ) { ?>
							<a href="#" class="maid-upload-logo"><img src="<?php echo esc_url( $logo_image_url[0] ); ?>" class="maid-logo-preview-image img-responsive" /></a>
			
					<?php } else { ?>
							<a href="#" class="maid-upload-logo"><img src="<?php echo CHILD_URI.'/images/logo-placeholder.png';?>" class="maid-logo-preview-image" /></a>
			
					<?php } ?>
					<br />
					<br />
					<input type="hidden" name="maid_data[logo]" value="<?php echo esc_attr( $logo ); ?>" />
					<a href="#" class="maid-upload-logo button"><?php esc_html_e( 'Upload Profile Photo', RM_TEXT_DOMAIN ); ?></a>
									
				</div>
				<div class="form-group col-xs-12 col-sm-8">
				<p class="help-block">Please follow our rules when uploading your photo:
<ul><li>Wear professional clothes (e.g. no low-cut tops or items with writing).</li>
<li>Show your face clearly—no hats or sunglasses.</li>
<li>Make sure no other adults or children appear in your photo.</li></ul></p>	
				</div>
			</div>
			
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Work History Info save.
	 */
	public function maid_setup_photo_info_save() {
		check_admin_referer( 'maid-setup' );
		
		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$vendor_data					= $this->vendor_data;
		$post_data 						= $_POST['maid_data'];
		
		$vendor_data['logo']		= sanitize_text_field($post_data['logo']);
		
		$updating_vendor = update_term_meta( $this->maid_id, 'vendor_data', $vendor_data );
		if ( is_wp_error($updating_vendor) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}
	
	/**
	 * Work History Info.
	 */
	public function maid_setup_work_history_info() {
		//var_dump($this->vendor_data);
		
		$profile 					= !empty($this->vendor_data['profile'])					? $this->vendor_data['profile']						: '';
		$referral					= !empty($this->maid_data['referral'])					? $this->maid_data['referral']						: array();
		$access_computer_webcam		= !empty($this->maid_data['access_computer_webcam'])	? $this->maid_data['access_computer_webcam']		: '';
		$computer_webcame_compat	= !empty($this->maid_data['computer_webcame_compat'])	? $this->maid_data['computer_webcame_compat']		: '';
		$smartphone_webcam_compat	= !empty($this->maid_data['smartphone_webcam_compat'])	? $this->maid_data['smartphone_webcam_compat']		: '';
		
		?>
		<h1><?php _e( 'Work History Information', 'house_care_club' ); ?></h1>
		<form method="post">
			<div class="row">
				<div class="form-group col-xs-12">
					<label for="maid-vendor-description">Please write a detailed BIO about yourself and why a House Care Club member would want YOU to provide services to them and their family?</label>
					<?php $this->textarea_field( 'profile', $profile, 10 ); ?>
					<p><a href="#" title="<?php _e( 'See description examples', 'house_care_club' ); ?>"><?php _e( 'See description examples', 'house_care_club' ); ?></a></p>
				</div>
			</div>
			
			<div class="row">
				<p class="question">Please provide contact information for a reference that you provided paid services for?</p>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th><?php _e( 'First and Last name', RM_TEXT_DOMAIN); ?></th>
								<th><?php _e( 'Relationship', RM_TEXT_DOMAIN); ?></th>
								<th><?php _e( 'Phone Number', RM_TEXT_DOMAIN); ?></th>
								<th><?php _e( 'Email', RM_TEXT_DOMAIN); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;
							for( $i=0; $i < 4; $i++ ):
								$name			= !empty($referral[$i]['name'])			? $referral[$i]['name'] 		: '';
								$relationship	= !empty($referral[$i]['relationship'])	? $referral[$i]['relationship'] : '';
								$phone			= !empty($referral[$i]['phone'])		? $referral[$i]['phone']		: '';
								$email			= !empty($referral[$i]['email'])		? $referral[$i]['email'] 		: '';
							?>
							<tr>
								<td>
									<input type="text" name="maid_data[referral][<?php echo $i; ?>][name]" id="maid-referral-<?php echo $i; ?>-name" value="<?php echo $name; ?>" >
								</td>
								<td>
									<div class="radio">
										<label class="radio-inline">
											<input type="radio" name="maid_data[referral][<?php echo $i; ?>][relationship]" id="maid-referral-<?php echo $i; ?>-relationship-client" value="Client" <?php checked('Client', $relationship, true ); ?>> Client
										</label>
									</div>
									<div class="radio">
										<label class="radio-inline">
											<input type="radio" name="maid_data[referral][<?php echo $i; ?>][relationship]" id="maid-referral-<?php echo $i; ?>-relationship-employer" value="Employer" <?php checked('Employer', $relationship, true ); ?>> Employer
										</label>
									</div>
								</td>
								<td>
									<input type="tel" name="maid_data[referral][<?php echo $i; ?>][phone]" id="maid-referral-<?php echo $i; ?>-phone" value="<?php echo $phone; ?>" >
								</td>
								<td>
									<input type="text" name="maid_data[referral][<?php echo $i; ?>][email]" id="maid-referral-<?php echo $i; ?>-email" value="<?php echo $email; ?>" >
								</td>
							</tr>
							<?php
							endfor;
							?>
						</tbody>
					</table>
				</div>		
			</div>
			<h1><?php _e( 'Interview Information', 'house_care_club' ); ?></h1>
			<p><?php  _e( 'Once we review your application, our Human Resource department will conduct a face to face interview using a webcam.  You can use your  computer or a computer at a Fed-Ex office, internet cafe or a Hotel Business Office', 'house_care_club' ); ?></p>
			<div class="row">
				<div class="form-group col-xs-12">
					<p class="question">Will you be able to access a computer with a webcam so we can interview you?</p>
					<?php $this->yes_no_field( 'access_computer_webcam', $access_computer_webcam); ?>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Do you have a home computer with webcam capability?</p>
					<?php $this->yes_no_field( 'computer_webcame_compat', $computer_webcame_compat); ?>
				</div>
				<div class="form-group col-xs-12">
					<p class="question">Do you have a smart phone with webcam capability?</p>
					<?php $this->yes_no_field( 'smartphone_webcam_compat', $smartphone_webcam_compat); ?>
				</div>
			</div>
			
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Work History Info save.
	 */
	public function maid_setup_work_history_info_save() {
		check_admin_referer( 'maid-setup' );
		
		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$vendor_data					= $this->vendor_data;
		$post_data 						= $_POST['maid_data'];
		
		$maid_data['referral']					= !empty($post_data['referral'])				? $post_data['referral']				: array();
		$maid_data['access_computer_webcam']	= sanitize_text_field( $post_data['access_computer_webcam'] );
		$maid_data['computer_webcame_compat']	= sanitize_text_field( $post_data['computer_webcame_compat'] );
		$maid_data['smartphone_webcam_compat']	= sanitize_text_field( $post_data['smartphone_webcam_compat'] );
		
		$vendor_data['profile']		= sanitize_text_field($post_data['profile']);
		
		$updating		 = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		$updating_vendor = update_term_meta( $this->maid_id, 'vendor_data', $vendor_data );
		if ( is_wp_error($updating) || is_wp_error($updating_vendor) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}
	
	/**
	 * Authorization Info.
	 */
	public function maid_setup_authorization() {
		$signature		= !empty($this->maid_data['signature'])			? $this->maid_data['signature']			: '';
		$signature_date	= !empty($this->maid_data['signature_date'])	? $this->maid_data['signature_date']	: '';
		?>
		<h1><?php _e( 'Authorization', 'house_care_club' ); ?></h1>
		<form method="post">
			<p><?php  _e( 'I acknowledge that the facts I have set forth in this application and any supplemental information I provide are true and complete to the best of my knowledge. I understand that, if employed, falsified statements on this application shall be considered sufficient cause for immediate discharge. Even if discharged I will still be held responsible for meeting all the requirements set forth in the Terms and Conditions and Privacy Statements contained on this website.', 'house_care_club' ); ?></p>
			<p><?php  _e( 'I hereby authorize House Care Club or its representatives to conduct a background check and also check criminal and predatory sex records and   investigate of all of my statements contained herein and references provided or any of past employers even if I did not provide their name to you, and I authorize them  to give you any and all information concerning my employment, and any pertinent information they may have, and release all parties from all liability for any damage that may result from furnishing same.', 'house_care_club' ); ?></p>
			<p><?php  _e( 'I understand that neither the completion of this application nor any other part of my consideration for employment establishes any obligation for the company to hire me. If I am hired, I understand that either the company or I can terminate my employment at any time and for any reason, with or without cause and without prior notice. I understand that no representative of the company has the authority to make any assurance to the contrary.', 'house_care_club' ); ?></p>
			<p><?php  _e( 'I understand that I am required to abide by all rules and regulations of the company.', 'house_care_club' ); ?></p>
		
			<div class="row">
				<div class="form-group col-xs-12">
					<label for="maid-signature">Signature (type name):</label>
					<?php $this->text_field( 'signature', $signature ); ?>
				</div>
				<div class="form-group col-xs-12">
					<label for="maid-signature-date">Date: </label>
					<?php $this->text_field( 'signature_date', $signature_date, 'datepicker' ); ?>
				</div>
			</div>
				
			<p class="maid-setup-actions step">
				<input type="hidden" value="<?php echo esc_attr($this->get_the_user_ip() ); ?>" name="maid_data[authorization_ip]" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Next', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Authorization Info save.
	 */
	public function maid_setup_authorization_save() {
		check_admin_referer( 'maid-setup' );
		
		/*
		 * Actions go here
		 */	
		$maid_data						= $this->maid_data;
		$post_data 						= $_POST['maid_data'];
		
		$maid_data['signature']			= sanitize_text_field( $post_data['signature'] );
		$maid_data['signature_date']	= sanitize_text_field( $post_data['signature_date'] );
		$maid_data['authorization_ip']	= sanitize_text_field( $post_data['authorization_ip'] );
		
		$updating = update_term_meta( $this->maid_id, 'maid_data', $maid_data );
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( $this->get_next_step_link() ) );//redirect to next step after succesfull completion of this step
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}

	/**
	 * Final step.
	 */
	public function maid_setup_ready() {
		
		?>
		<h1><?php _e( 'Your House Keeper Profile is Ready!', 'house_care_club' ); ?></h1>
		<p><?php _e( 'A representative will contact you shortly', 'house_care_club' ); ?></p>
		<form method="post">
			<div class="maid-setup-next-steps">
				<div class="row">
					
				</div>
			</div>
			<p class="maid-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Previous', 'house_care_club' ); ?>" name="save_step_prev" />
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Complete', 'house_care_club' ); ?>" name="save_step_next" />
				<?php wp_nonce_field( 'maid-setup' ); ?>
			</p>
		</form>
		<?php
		//hcc_send_housekeeper_info( $this->maid_id );
	}
	
	/**
	 * Final Step save.
	 */
	public function maid_setup_ready_save() {
		check_admin_referer( 'maid-setup' );
		
		hcc_send_housekeeper_info( $this->maid_id );
		
		/*
		 * Actions go here
		 */	
		if( !empty( $_POST['save_step_next'] ) ):
			$maid_setup_completed = 1;
		else:
			$maid_setup_completed = 0;
		endif;
		
		
		$updating = update_term_meta( $this->maid_id, 'maid_setup_completed', $maid_setup_completed);
		if ( is_wp_error($updating) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup&step='.$this->step.'&error=maid_data_error' ) );
			exit;
		else:
			if ( !empty($_POST['save_step_prev']) ):
				wp_redirect( esc_url_raw( $this->get_previous_step_link() ) );//redirect to previous step after succesfull completion of this step
				exit;
			else:
				wp_redirect( esc_url_raw( admin_url() ) );//redirect to admin
				exit;
			endif;
		endif;
		
		/*
		 * End of actions
		 */	
		
	}
}

new Rand_Admin_Setup_Wizard();

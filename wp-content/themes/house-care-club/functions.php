<?php
define( 'RAND_VERSION' , '1.0' );
define( 'PARENT_DIR', get_template_directory() );
define( 'PARENT_URI', get_template_directory_uri() );
define( 'CHILD_DIR', get_stylesheet_directory() );
define( 'CHILD_URI', get_stylesheet_directory_uri() );
define( 'RM_TEXT_DOMAIN', 'rand_marketing' );
define( 'HCC_TEXTDOMAIN', 'house-care-club');

function hcc_is_member(){
	if ( ! function_exists( 'wc_memberships' ) ) {
		return false;
	}
	// get any active user memberships (requires Memberships 1.4+)
	$user_id = get_current_user_id();
	$args = array( 
		'status' => array( 'active', 'complimentary', 'pending', 'free_trial' ),
	);
	
	$active_memberships = wc_memberships_get_user_memberships( $user_id, $args );
	//write_log( $active_memberships );
	// only proceed if the user has no active memberships
	if ( empty( $active_memberships ) ) {
		return false;
	}else{
		return true;	
	}
}

function hcc_replace_underscore( $string = '' ){
	return str_replace( '_', ' ', $string );
}

function add_wcpv_product_vendors_columns($columns){
	if( current_user_can( 'manage_options' ) ):
    	$columns['hcc_send_app'] = 'Application';
		$columns['hcc_approve_app'] = 'Approved?';
    endif;
	return $columns;
}
add_filter('manage_edit-wcpv_product_vendors_columns', 'add_wcpv_product_vendors_columns');

function hcc_send_action($content,$column_name,$term_id){
	if( $column_name == 'hcc_send_app' ):
   		$content = '<a href="'.wp_nonce_url(admin_url('edit-tags.php?taxonomy=wcpv_product_vendors&post_type=product&hcc_action=send-app&term_id='.$term_id), 'hcc_send_app', 'hcc_send_app_nonce').'" class="button button-primary">Re-send</a>';
	endif;
	if( $column_name == 'hcc_approve_app' ):
		$maid_approved = get_term_meta( $term_id, 'maid_approved', true ) !== FALSE ? get_term_meta( $term_id, 'maid_approved', true ) : 0 ;
		if( $maid_approved ):
   			$content = '<a href="'.wp_nonce_url(admin_url('edit-tags.php?taxonomy=wcpv_product_vendors&post_type=product&hcc_action=unapprove&term_id='.$term_id), 'hcc_unapprove_app', 'hcc_unapprove_app_nonce').'" class="button button-primary">Unapprove</a>';
		else:
			$content = '<a href="'.wp_nonce_url(admin_url('edit-tags.php?taxonomy=wcpv_product_vendors&post_type=product&hcc_action=approve&term_id='.$term_id), 'hcc_approve_app', 'hcc_approve_app_nonce').'" class="button button-primary">Approve</a>';
		endif;
	endif;
	return $content;
}
add_filter('manage_wcpv_product_vendors_custom_column','hcc_send_action',10,3);

function hcc_send_app_action(){
	if ( (isset($_GET['hcc_send_app_nonce']) && wp_verify_nonce($_GET['hcc_send_app_nonce'], 'hcc_send_app') ) && ( isset($_GET['hcc_action']) && $_GET['hcc_action'] == 'send-app' ) && isset($_GET['term_id']) ) {
		hcc_send_housekeeper_info( $_GET['term_id'] );
	}
	if ( (isset($_GET['hcc_approve_app_nonce']) && wp_verify_nonce($_GET['hcc_approve_app_nonce'], 'hcc_approve_app') ) && ( isset($_GET['hcc_action']) && $_GET['hcc_action'] == 'approve' ) && isset($_GET['term_id']) ) {
		$updating = update_term_meta( $_GET['term_id'], 'maid_approved', 1);
	}
	if ( (isset($_GET['hcc_unapprove_app_nonce']) && wp_verify_nonce($_GET['hcc_unapprove_app_nonce'], 'hcc_unapprove_app') ) && ( isset($_GET['hcc_action']) && $_GET['hcc_action'] == 'unapprove' ) && isset($_GET['term_id']) ) {
		$updating = update_term_meta( $_GET['term_id'], 'maid_approved', 0);
	}
	if( isset( $updating ) ):
		if ( is_wp_error($updating) ):
			add_action( 'admin_notices', 'hcc_app_approval_admin_notice__error' );
		else:
			add_action( 'admin_notices', 'hcc_app_approval_admin_notice__success' );
		endif;
	endif;
}
add_action( 'init', 'hcc_send_app_action' );

function hcc_app_approval_admin_notice__success() {
    ?>
    <div class="notice notice-success is-dismissable">
        <p><?php _e( 'Approval Status Updated!', HCC_TEXTDOMAIN ); ?></p>
    </div>
    <?php
}


function hcc_app_approval_admin_notice__error() {
	$class = 'notice notice-error is-dismissable';
	$message = __( 'Couldn\'t update approval please try again!', HCC_TEXTDOMAIN );

	printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message ); 
}

function hcc_send_housekeeper_info( $housekeeper_id ){
	//$current_user					= wp_get_current_user();
	//$user_id						= $current_user->ID;
	$vendor_data				= get_term_meta( $housekeeper_id, 'vendor_data', true);
	$housekeeper_data			= get_term_meta( $housekeeper_id, 'maid_data', true);
	$housekeeper				= get_term_by( 'id', $housekeeper_id, 'wcpv_product_vendors' );
	$to				= get_option( 'admin_email' );
	$subject 		= __( 'House Care Club Housekeeper Application Submission', HCC_TEXTDOMAIN );
	$headers		= array('Content-Type: text/html; charset=UTF-8');
	//var_dump( $vendor_data );
	//var_dump( $housekeeper_data );
	
	$court_appearences = is_array($housekeeper_data['court_appearences'])?implode( ',', $housekeeper_data['court_appearences'] ):$housekeeper_data['court_appearences'];
	$email_message 	=	"<strong>Name:</strong><br/> ".$housekeeper->name."<br/>".
						"<strong>Bio:</strong><br/> ".$vendor_data['profile']."<br/>".
						"<strong>Logo:</strong><br/> ".$vendor_data['logo']."<br/>".
						"<strong>Address:</strong><br/> ".$housekeeper_data['address']." ".$housekeeper_data['address_number']." ".$housekeeper_data['city'].", ".$housekeeper_data['state']." ".$housekeeper_data['zipcode']."<br/>".
						"<strong>Cellphone:</strong><br/> ".$housekeeper_data['cellphone']."<br/>".
						"<strong>Homephone:</strong><br/> ".$housekeeper_data['homephone']."<br/>".
						"<strong>Dirver Liscence?</strong><br/> ".$housekeeper_data['driver_liscence']."<br/>".
						"<strong>Has Vehicle?</strong><br/> ".$housekeeper_data['vehicle']."<br/>".
						"<strong>Miles willing to drive:</strong><br/> ".$housekeeper_data['miles']."<br/>".
						"<strong>Skype username:</strong><br/> ".$housekeeper_data['skype']."<br/>".
						"<strong>Do you agree to provide all complete and truthful answers?</strong><br/> ".$housekeeper_data['agreement']."<br/>".
						"<strong>Do you authorize us have a background and criminal records check conducted on you, at no charge to you?</strong><br/> ".$housekeeper_data['background_auth']."<br/>".
						"<strong>Do you authorize us to verify, or have verified on our behalf, all statements contained in his Application and to check information about you with your references?</strong><br/> ".$housekeeper_data['verify_auth']."<br/>".
						"<strong>Have you ever been questioned by police about your possibly committing a crime?</strong><br/> ".$housekeeper_data['questioned_police']."<br/>".
						"<strong>If Yes, Provide Details</strong><br/> ".$housekeeper_data['questioned_details']."<br/>".
						"<strong>Tell us about any court appearances you’ve had?</strong><br/> ".$court_appearences."<br/>".
						"<strong>Have you ever been arrested in any country?</strong><br/> ".$housekeeper_data['country_arrest']."<br/>".
						"<strong>If Yes, Provide Details</strong><br/> ".$housekeeper_data['arrest_details']."<br/>".
						"<strong>Gender:</strong><br/> ".$housekeeper_data['gender']."<br/>".
						"<strong>Date of Brith:</strong><br/> ".$housekeeper_data['dob']."<br/>".
						"<strong>Are you a citizen?</strong><br/> ".$housekeeper_data['citizen']."<br/>".
						"<strong>Allowed to legally work in the US?</strong><br/> ".$housekeeper_data['legal_employment']."<br/>".
						"<strong>Has Tax ID?</strong><br/> ".$housekeeper_data['has_tax_id']."<br/>".
						"<strong>Has Social Security Number?</strong><br/> ".$housekeeper_data['has_ssn']."<br/>".
						"<strong>Has Checking Account</strong><br/> ".$housekeeper_data['has_check_account']."<br/>".
						"<strong>Language Fluency</strong><br/> ".$housekeeper_data['language_fluency']."<br/>".
						"<strong>Can we report all income we pay you?</strong><br/> ".$housekeeper_data['report_income']."<br/>".
						"<strong>Do you smoke cigarettes?</strong><br/> ".$housekeeper_data['smoke_cigarettes']."<br/>".
						"<strong>Do you agree to not smoke while working inside a member’s house?</strong><br/> ".$housekeeper_data['smoke_agreement']."<br/>".
						"<strong>What is your highest level of education?</strong><br/> ".$housekeeper_data['education_level']."<br/>".
						"<strong>Are you currently employed?</strong><br/> ".$housekeeper_data['currently_employed']."<br/>".
						"<strong>How many hours a week are you currently employed in total?</strong><br/> ".$housekeeper_data['hours_employed']."<br/>".
						"<strong>How many hours of employment are you looking for weekly?</strong><br/> ".$housekeeper_data['hours_weekly']."<br/>".
						"<strong>How many hours of employment are you looking for monthly?</strong><br/> ".$housekeeper_data['hours_monthly']."<br/>".
						"<strong>If we can provide all the hours of work you want, what is the lowest hourly pay rate you are willing to accept?</strong><br/> ".$housekeeper_data['lowest_rate']."<br/>".
						"<strong>Indicate any Licenses, Training, and/or  Memberships you have:</strong><br/> ".$housekeeper_data['certifications']."<br/>".
						"<strong>Other Certifications</strong><br/> ".$housekeeper_data['other_certifications']."<br/>".
						"<h2><strong>Services You Will Provide</strong></h2><br/>"
						;
	foreach( $housekeeper_data['services'] as $service ):
		$email_message .= "<strong>Service:</strong> " . $service['name'] . "<br/> &#9;<strong>Years of Experience:</strong> " . $service['xp'] . "<br/> &#9;<strong>Experience type(s):</strong> ";
		foreach( $service['types'] as $xp_type ):
			$email_message .= $xp_type . ',';
		endforeach;
		$email_message .= '<br />';
	endforeach;
	$email_message .= "<strong>Willing to provide Standard Cleaning Services</strong><br/> ".$housekeeper_data['standard_services']."<br/>".
					  "<strong>Cleaning Services willing to provide to our Members when requested:</strong><br/> ".implode( ',', $housekeeper_data['other_cleaning_services'])."<br/>".
					  "<strong>Maximum size house you have experience servicing</strong><br/> ".$housekeeper_data['max_home_size']."<br/>".
					  "<strong>Will you climb on a ladder to dust high ceiling areas, or change smoke detector batteries or ceiling light bulbs?</strong><br/> ".$housekeeper_data['climb_ladder']."<br/>".
					  "<strong>Can you move furniture if needed to clean underneath?</strong><br/> ".$housekeeper_data['move_furniture']."<br/>".
					  "<strong>Special Cleaning  services that you are <strong><u>NOT WILLING</u></strong> to provide?</strong><br/> ".implode( ',', $housekeeper_data['special_cleaning_services'])."<br/>".
					  "<strong>Types of cleaning supplies you are willing to provide:</strong><br/> ".implode( ',',$housekeeper_data['cleaning_supplies'])."<br/>".
					  "<strong>Personal Assistant Services you’re willing to provide:</strong><br/> ".implode( ',', $housekeeper_data['assistant_services'])."<br/>".
					  "<strong>Are you willing to provide services using your own vehicle?</strong><br/> ".$housekeeper_data['use_own_vehicle']."<br/>".
					  "<strong>Transportation Services you are <u>WILLING</u> to provide if the times and compensation are good for you:</strong><br/> ".implode( ',', $housekeeper_data['transportation_services'] )."<br/>".
					  "<strong>Have you ever had a driving violation for Drugs or Alcohol?</strong><br/> ".$housekeeper_data['traffic_violation']."<br/>".
					  "<strong>How much is the dollar limit on the liability insurance on your vehicle? </strong><br/> ".$housekeeper_data['liability_insurance']."<br/>".
					  "<strong>Auto liability insurance limit </strong><br/> ".$housekeeper_data['liability_insurance_limit']."<br/>".
					  "<strong>As a driver, have you ever had a driving accident that resulted in personal injury above $10,000? </strong><br/> ".$housekeeper_data['accident_personal_injury']."<br/>".
					  "<strong>How many years has it been since you were driving and had an accident (if any)? </strong><br/> ".$housekeeper_data['accident_years']."<br/>".
					  "<strong>Date of accident </strong><br/> ".$housekeeper_data['accident_date']."<br/>".
					  "<strong>Car Manufacturer </strong><br/> ".$housekeeper_data['car_manufacturer']."<br/>".
					  "<strong>Car Year </strong><br/> ".$housekeeper_data['car_year']."<br/>".
					  "<strong>Car Accessories </strong><br/> ".implode( ',',$housekeeper_data['car_accessories'])."<br/>".
					  "<strong>Child Care Services </strong><br/> ".implode( ',',$housekeeper_data['child_care_services'])."<br/>".
					  "<strong>able to swim in case a child falls into a pool? </strong><br/> ".$housekeeper_data['swim_able']."<br/>".
					  "<strong>How many children are you comfortable watching at the same time? </strong><br/> ".$housekeeper_data['num_child_watch']."<br/>".
					  "<strong>Do you have experience with twins/multiples? </strong><br/> ".$housekeeper_data['twins_xp']."<br/>".
					  "<strong>What types of children with special needs have you cared for? </strong><br/> ".implode( ',',$housekeeper_data['special_child_care_xp'])."<br/>".
					  "<strong>Other Special Needs Child Care Experience (Describe) </strong><br/> ".$housekeeper_data['other_special_child_care_xp']."<br/>".
					  "<strong>Senior Services </strong><br/> ".implode( ',',$housekeeper_data['senior_care_services'])."<br/>".
					  "<strong>Special Needs experience </strong><br/> ".implode( ',',$housekeeper_data['special_senior_care_xp'])."<br/>".
					  "<strong>Other Senior Special Needs Experience (Describe) </strong><br/> ".$housekeeper_data['other_special_senior_care_xp']."<br/>".
					  "<strong>Provide help with a members dog or cat? </strong><br/> ".$housekeeper_data['pet_help']."<br/>".
					  "<strong>Pet Care Services WILLING to provide: </strong><br/> ".implode( ',',$housekeeper_data['pet_care_services'])."<br/>".
					  "<strong>Additional Pet Care Services you can provide: </strong><br/> ".implode( ',',$housekeeper_data['add_pet_care_services'])."<br/>".
					  "<strong>Would you occasionally stay overnight in member's home to care for pet? </strong><br/> ".$housekeeper_data['pet_overnight']."<br/>".
					  "<strong>Would you board a dog or cat at YOUR HOME? </strong><br/> ".$housekeeper_data['pet_boarding']."<br/>".
					  "<strong>If yes, what is the maximum size of dog willing to board in your home </strong><br/> ".$housekeeper_data['max_pet_size']."<br/>".
					  "<strong>Do boarded dogs need to be house broken? </strong><br/> ".$housekeeper_data['house_broken']."<br/>".
					  "<strong>Is your yard fenced? </strong><br/> ".$housekeeper_data['fenced_yard']."<br/>".
					  "<strong>Do children live in your home under 7 years old? </strong><br/> ".$housekeeper_data['children_under_seven']."<br/>".
					  "<strong>Do you board other dogs/cats at the same time? </strong><br/> ".$housekeeper_data['board_pets_same_time']."<br/>".
					  "<strong>House Watch Services willing to provide for a house if you are cleaning the house while the homeowner travels out of town. </strong><br/> ".implode( ',',$housekeeper_data['house_watch_services'])."<br/>".
					  "<h2>Referrals</h2><br/><br/>";
					  
	foreach( $housekeeper_data['referral'] as $referral ):
		if( $referral['name'] != '' ):
			$email_message .= "&#9;<strong>Name </strong>".$referral['name']."<br/> &#9;<strong>Relationship </strong>".$referral['relationship']."<br/> &#9;<strong>Phone </strong>".$referral['phone']."<br/> &#9;<strong>Email </strong>".$referral['email']."<br/>";
		endif;
	endforeach; 
	
	$email_message .= "<strong>Access to a computer with a webcam so we can interview you? </strong><br/> ".$housekeeper_data['access_computer_webcam']."<br/>".
					  "<strong>Do you have a home computer with webcam capability? </strong><br/> ".$housekeeper_data['computer_webcame_compat']."<br/>".
					  "<strong>Do you have a smart phone with webcam capability? </strong><br/> ".$housekeeper_data['smartphone_webcam_compat']."<br/>".
					  "<strong>Signature (type name): </strong><br/> ".$housekeeper_data['signature']."<br/>".
					  "<strong>Signature Date </strong><br/> ".$housekeeper_data['signature_date']."<br/>";		
	//echo $email_message;
	//write_log(wp_mail( $to, $subject, $email_message, $headers ));
	if ( wp_mail( $to, $subject, $email_message, $headers ) ):
		add_action( 'admin_notices', 'hcc_registration_email_admin_notice__success' );
	else:
		add_action( 'admin_notices', 'hcc_registration_email_admin_notice__error' );
	endif;	
}

function hcc_registration_email_admin_notice__success() {
    ?>
    <div class="notice notice-success is-dismissable">
        <p><?php _e( 'Your Application has been sent to the admin email!', HCC_TEXTDOMAIN ); ?></p>
    </div>
    <?php
}


function hcc_registration_email_admin_notice__error() {
	$class = 'notice notice-error is-dismissable';
	$message = __( 'The email couldn\' be sent. Don\'t worry the application is safe.', HCC_TEXTDOMAIN );

	printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message ); 
}

function house_care_theme_enqueue_styles() {

    $parent_style = 'rand-style';
	wp_dequeue_style( $parent_style );
	wp_deregister_style( $parent_style );
    wp_enqueue_style( $parent_style, PARENT_URI . '/style.css' );
    wp_enqueue_style( 'house-care-style', CHILD_URI . '/style.css', array( $parent_style ) );
	wp_enqueue_style( 'open-sans-style','https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic');
	if ( is_page_template( 'search-housekeeper.php' ) || is_tax('wcpv_product_vendors') || null !== get_query_var( 'my-favorites' ) )
		wp_enqueue_style( 'courgette-style','https://fonts.googleapis.com/css?family=Courgette');
	
}
add_action( 'wp_enqueue_scripts', 'house_care_theme_enqueue_styles', 11 );

add_action( 'wp_footer', 'hcc_rand_theme_wp_footer_start', 0 );
function hcc_rand_theme_wp_footer_start(){
if( !is_page_template( 'search-housekeeper.php' ) ):
?>
<!-- Modal -->
<div class="modal fade" id="login-register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content p-y-2 p-x-2">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="myModalLabel"><strong>To Contact One of Our Housekeepers,<br />
You Must Become a Free Trial Member<br />
<span class="blue-text uppercase">CREATE YOUR ACCOUNT</span></strong></h4>
			</div>
			<div class="modal-body gray-box p-y-2 p-x-2 form-horizontal"> <?php echo do_shortcode('[wp_register_form]'); ?> </div>
			<div class="modal-footer">
			<ul class="fa-ul text-left small">
				<li><i class="fa-li fa fa-asterisk" aria-hidden="true"></i>
 All items marked with an * are required.</li>
 				<li><i class="fa-li fa fa-asterisk" aria-hidden="true"></i>
 Email Address is used as User Id.</li>
				<li><i class="fa-li fa fa-asterisk" aria-hidden="true"></i>
 We will not sell, share, rent, or distribute your personal information to third parties, and will not send you any unauthorized emails.</li></ul>
			</div>
		</div>
	</div>
</div>
<?php
endif;
?>
<!-- Modal -->
<div class="modal fade" id="search-housekeepers" tabindex="-1" role="dialog" aria-labelledby="searchLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content p-y-2 p-x-2">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="searchLabel"><strong>Search Hoouse Keepers</strong></h4>
			</div>
			<div class="modal-body gray-box p-y-2 p-x-2 form-horizontal"> <?php echo do_shortcode('[housekeeper_search]'); ?> </div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<?php
}
function hcc_wp_head(){
	//if( !is_page_template( 'search-housekeeper.php' ) ):
	?>
	<script>
	jQuery(document).ready(function() {
		jQuery( 'a[href=#login-register-popup]' ).on( 'click', function(e){
			e.preventDefault();
			jQuery('#login-register-modal').modal('show');
		});
	});
	</script>
	<?php	
	//endif;
}
add_action( 'wp_head', 'hcc_wp_head' );
add_action( 'wp_footer', 'hcc_rand_theme_wp_footer_end', 1000 );
function hcc_rand_theme_wp_footer_end(){

if ( is_user_logged_in() ):
?>
<script>
jQuery(document).ready(function($){
	//HOUSEKEEPER FAVORITES AJAX BUTTON
	$('#favorites-btn').on( 'click', function(e) {
		e.preventDefault();
		var button = this;
		var favaction = jQuery(this).data('action');
		var hcc_id = jQuery(this).data('hcc-id');
		//console.log( favaction );
		if ( jQuery(this).hasClass('disabled') ){
			return;
		}else{
			jQuery(this).addClass('disabled');
			jQuery.ajax({
				url : '<?php echo admin_url( 'admin-ajax.php' ); ?>',
				type : 'post',
				data : {
					action : 'hcc_' + favaction + '_favorite',
					hcc_id : hcc_id
				},
				success : function( response ) {
					if ( favaction == 'add' ){				
						if ( response == 'success' ){
							jQuery(button).attr('data-action', 'remove' ).data('action', 'remove' );
							jQuery(button).removeClass('blue').addClass('green');
							jQuery(button).find('.fa').removeClass('fa-heart-o');
							jQuery(button).find('.fa').addClass('fa-heart');
							jQuery(button).find('.text').html('Added to Favorites<br/><small>click again to remove from favorites</small>');
						}else{
							
						}
					}else{
						if ( response == 'success' ){
							jQuery(button).attr('data-action', 'add').data('action', 'add' );
							jQuery(button).removeClass('green').addClass('blue');
							jQuery(button).find('.fa').removeClass('fa-heart');
							jQuery(button).find('.fa').addClass('fa-heart-o');
							jQuery(button).find('.text').html('Add to Favorites');
						}else{
							
						}
					}
					jQuery(button).removeClass('disabled');
				}
			});
		}
	})
	//HOUSEKEEPER CONTACT AJAX
	$('#contact-btn.ajax-action').on( 'click', function(e) {
		e.preventDefault();
		var button = this;
		var hcc_id = jQuery(this).data('hcc-id');
		//console.log( favaction );
		if ( jQuery(this).hasClass('disabled') ){
			return;
		}else{
			jQuery(this).addClass('disabled');
			jQuery.ajax({
				url : '<?php echo admin_url( 'admin-ajax.php' ); ?>',
				type : 'post',
				data : {
					action : 'hcc_contact_housekeeper',
					hcc_id : hcc_id
				},
				success : function( response ) {								
					if ( response == 'success' ){
						window.location.href = "<?php echo get_the_permalink( get_option( 'woocommerce_myaccount_page_id' ) ).'house-profile/'; ?>";
					}else{
						$('#contact-modal').modal('show');
						//$('#favorites-modal .modal-body').html(response);
					}
					jQuery(button).removeClass('disabled');
				}
			});
		}
	})
});
</script>
<?php
endif;	
}

function house_care_admin_enqueue_scripts( $hook ){
	wp_enqueue_style( 'house-care-admin-style', CHILD_URI . '/css/vendor.css' );
	
	if ( 'user-edit.php' == $hook || 'profile.php' == $hook ) {
        wp_enqueue_script( 'rand-admin-ui' );
		wp_enqueue_style( 'rand-admin-ui' );
		wp_enqueue_script( 'rand-select2' );
		wp_enqueue_style( 'rand-select2' );
    }
}
add_action( 'admin_enqueue_scripts', 'house_care_admin_enqueue_scripts', 11, 1 );
add_action( 'init', 'redirect_maid_to_wizard');

function redirect_maid_to_wizard(){
	$user = wp_get_current_user();
	//var_dump($user->roles);
	$user_id = get_current_user_id();
	$maids = WC_Product_Vendors_Utils::get_all_vendor_data( $user_id );

	if ( !empty( $maids ) ):
		$arrayKeys = array_keys($maids);
			
		$term_id = $maids[$arrayKeys[0]]['term_id'];
		$maid_setup_completed = get_term_meta( $term_id, 'maid_setup_completed', true ) !== FALSE ? get_term_meta( $term_id, 'maid_setup_completed', true ) : 0 ;
		//var_dump( $maid_setup_completed , !$maid_setup_completed);
		
		if( is_admin() && array_intersect( array( 'wc_product_vendors_pending_vendor','wc_product_vendors_manager_vendor','wc_product_vendors_admin_vendor' ), $user->roles ) && (!isset($_GET['page']) || $_GET['page'] !== 'maid-setup') && !$maid_setup_completed && (  ! defined( 'DOING_AJAX' ) ) ):
			wp_safe_redirect( esc_url_raw( admin_url().'index.php?page=maid-setup' ) );
			exit;
		endif;
	endif;
}

add_action( 'init', 'includes' );

function includes(){
	if ( ! empty( $_GET['page'] ) ) {
		switch ( $_GET['page'] ) {
			case 'maid-setup' :
				include_once( CHILD_DIR.'/maid_setup/maid_setup-wizard.php' );
			break;
		}
	}
}

add_action( 'wcpv_shortcode_registration_form_process', 'housecare_vendors_reg_custom_fields_save', 10, 2 );
function housecare_vendors_reg_custom_fields_save( $args, $items ) {
	$term = get_term_by( 'name', $items['vendor_name'], 'wcpv_product_vendors' );
	//$vendor_data = get_term_meta( $term->term_id, 'vendor_data' , true );
	//write_log( $vendor_data );
	if ( isset( $items['middlename'] ) && ! empty( $items['middlename'] ) ) {
		$maid_data['maid_middlename'] = sanitize_text_field( $items['middlename'] );
		update_term_meta( $term->term_id, 'maid_data', $maid_data );
	}
}


add_action( 'wcpv_shortcode_registration_form_process', 'hcc_wcpv_shortcode_registration_form_process', 10, 2 );
function hcc_wcpv_shortcode_registration_form_process( $args, $form_items ){
	//write_log( $args );
	//write_log( $form_items );
	$link = esc_url_raw( admin_url() );
	if( !is_user_logged_in() ):
		$secure_cookie = is_ssl() ? true : false;
        wp_set_auth_cookie($args['user_id'], true, $secure_cookie);
	else:
		$current_user = wp_get_current_user();
		wp_update_user( array( 'ID' => $current_user->ID, 'role' => 'wc_product_vendors_admin_vendor' ) );
	endif;
}

add_filter( 'wcpv_shortcode_registration_form_validation_errors', 'hcc_wcpv_shortcode_registration_form_validation_errors', 10, 2 );
function hcc_wcpv_shortcode_registration_form_validation_errors( $errors, $form_items ){
	//write_log( $errors );
	//write_log( $form_items );
	/*$vendor_name_val = __( 'Vendor Name is a required field.', 'woocommerce-product-vendors' );
	if ( in_array( $vendor_name_val, $errors ) ):
		$key = array_search( $vendor_name_val, $errors );
		unset($errors[$key]);
	endif;*/
	$vendor_desc_val = __( 'Vendor Description is a required field.', 'woocommerce-product-vendors' );
	if ( in_array( $vendor_desc_val, $errors ) ):
		$key = array_search( $vendor_desc_val, $errors );
		unset($errors[$key]);
	endif;
	//write_log( $errors );
	return $errors;
}

function housecare_change_post_menu_label() {
	if ( WC_Product_Vendors_Utils::is_admin_vendor() ) {
		remove_menu_page( 'vc-welcome' );
		remove_menu_page( 'wcpv-vendor-settings' );
		remove_menu_page( 'wcpv-vendor-orders' );
		remove_menu_page( 'upload.php' );
		remove_menu_page( 'edit.php?post_type=product' );
		
		remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );
		
		add_menu_page( __( 'Housekeeper Profile', HCC_TEXTDOMAIN ), __( 'Housekeeper Profile', HCC_TEXTDOMAIN ), 'manage_product', 'wcpv-maid-settings', 'housecare_render_settings_page' , 'dashicons-admin-settings', 60.77 );

				
	}
}
add_action( 'admin_menu', 'housecare_change_post_menu_label',99 );

function housecare_render_settings_page() {
	wp_enqueue_script( 'wc-enhanced-select' );
	wp_enqueue_script( 'jquery-tiptip' );

	$vendor_data = WC_Product_Vendors_Utils::get_vendor_data_from_user();

	// handle form submission
	if ( ! empty( $_POST['wcpv_save_vendor_settings_nonce'] ) && ! empty( $_POST['vendor_data'] ) ) {
		// continue only if nonce passes
		if ( wp_verify_nonce( $_POST['wcpv_save_vendor_settings_nonce'], 'wcpv_save_vendor_settings' ) ) {

			$posted_vendor_data = $_POST['vendor_data'];

			// sanitize
			$posted_vendor_data = array_map( 'sanitize_text_field', $posted_vendor_data );
			$posted_vendor_data = array_map( 'stripslashes', $posted_vendor_data );

			// sanitize html editor content
			$posted_vendor_data['profile'] = ! empty( $_POST['vendor_data']['profile'] ) ? wp_kses_post( stripslashes( $_POST['vendor_data']['profile'] ) ) : '';

			// merge the changes with existing settings
			$posted_vendor_data = array_merge( $vendor_data, $posted_vendor_data );

			if ( update_term_meta( WC_Product_Vendors_Utils::get_logged_in_vendor(), 'vendor_data', $posted_vendor_data ) ) {

				// grab the newly saved settings
				$vendor_data = WC_Product_Vendors_Utils::get_vendor_data_from_user();
			}
		}	
	}

	// logo image
	$logo             = ! empty( $vendor_data['logo'] ) ? $vendor_data['logo'] : '';
	
	$hide_remove_image_link = '';
	
	$logo_image_url = wp_get_attachment_image_src( $logo, 'full' );
	
	if ( empty( $logo_image_url ) ) {
		$hide_remove_image_link = 'display:none;';
	}
	
	$profile           = ! empty( $vendor_data['profile'] ) ? $vendor_data['profile'] : '';
	$email             = ! empty( $vendor_data['email'] ) ? $vendor_data['email'] : '';
	$paypal            = ! empty( $vendor_data['paypal'] ) ? $vendor_data['paypal'] : '';
	$vendor_commission = ! empty( $vendor_data['commission'] ) ? $vendor_data['commission'] : get_option( 'wcpv_vendor_settings_default_commission', '0' );

	include_once( 'woocommerce-product-vendors/views/html-vendor-store-settings-page.php' );

	return true;
}

add_filter( 'wcpv_shortcode_register_vendor_args', 'house_care_default_vendor_role', 1 );
function house_care_default_vendor_role( $args ){
	$args['role'] = 'wc_product_vendors_admin_vendor';
	
	return $args;
}

add_action( 'wpcf7_init', 'housecare_add_shortcode_referral' );
function housecare_add_shortcode_referral() {
    wpcf7_add_shortcode( 'referral_url', 'housecare_referral_shortcode_handler' ); // "clock" is the type of the form-tag
}
 
function housecare_referral_shortcode_handler( $tag ) {
    $referral_id = get_user_meta( get_current_user_id(), "gens_referral_id", true);
	return esc_url(add_query_arg( 'raf', $referral_id, get_home_url() ));
}

add_filter( 'rand_social_icons_options', 'add_hcc_social_networks', 10, 1 );
function add_hcc_social_networks($options){
	$options[] = array( "name" => __('Linked In', HCC_TEXTDOMAIN),
						"desc" => __('Enter the URL to your Linkedin profile.', HCC_TEXTDOMAIN),
						"id" => "linkedin",
						"type" => "text"
						); 
	return $options;
}
add_filter( 'rand_before_support_options', 'add_hcc_before_support_options', 10, 1 );
function add_hcc_before_support_options($options){
	$options_pages = array();  
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	//$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
    	$options_pages[$page->ID] = $page->post_title;
	}
	$options[] = array( "name" =>  __('Housekeeper Search', HCC_TEXTDOMAIN),
						"type" => "heading"); 
		$options[] = array(
			'name' => __( 'Select the Housekeeper Search Results Page', HCC_TEXTDOMAIN ),
			'desc' => __( '', HCC_TEXTDOMAIN ),
			'id' => 'hcc_search_result_page',
			'class'	=> 'enhanced-select',
			'type' => 'select',
			'options' => $options_pages,
		);
		$options[] = array(
			'name' => __( 'Select the Housekeeper Membership Plans Page', HCC_TEXTDOMAIN ),
			'desc' => __( '', HCC_TEXTDOMAIN ),
			'id' => 'hcc_plans_page',
			'class'	=> 'enhanced-select',
			'type' => 'select',
			'options' => $options_pages,
		);
		$options[] = array(
			'name' => __( 'Select the Green Cleaning Certified Logo', HCC_TEXTDOMAIN ),
			'desc' => __( '', HCC_TEXTDOMAIN ),
			'id' => 'hcc_gc_cert',
			'type' => 'upload',
		);
		$options[] = array(
			'name' => __( 'Select the Health Aid/Nursing Certified Logo', HCC_TEXTDOMAIN ),
			'desc' => __( '', HCC_TEXTDOMAIN ),
			'id' => 'hcc_hn_cert',
			'type' => 'upload',
		);
		$options[] = array(
			'name' => __( 'Select the CPR Certified Logo', HCC_TEXTDOMAIN ),
			'desc' => __( '', HCC_TEXTDOMAIN ),
			'id' => 'hcc_cpr_cert',
			'type' => 'upload',
		);
		$options[] = array(
			'name' => __( 'Select the Pet CPR Certified Logo', HCC_TEXTDOMAIN ),
			'desc' => __( '', HCC_TEXTDOMAIN ),
			'id' => 'hcc_pcpr_cert',
			'type' => 'upload',
		);
		$options[] = array(
			'name' => __( 'Select the First Aid Certified Logo', HCC_TEXTDOMAIN ),
			'desc' => __( '', HCC_TEXTDOMAIN ),
			'id' => 'hcc_fa_cert',
			'type' => 'upload',
		);
		$options[] = array(
			'name' => __( 'Select the Teaching Assitant Certified Logo', HCC_TEXTDOMAIN ),
			'desc' => __( '', HCC_TEXTDOMAIN ),
			'id' => 'hcc_ta_cert',
			'type' => 'upload',
		);
	return $options;
}

if ( is_plugin_active( 'js_composer/js_composer.php' ) ) :
	add_filter( 'rand_vc_icon_styles', 'hcc_vc_icon_styles', 1 );
	function hcc_vc_icon_styles( $icon_styles ){
		$icon_styles["Alternate Box"] = 'box-alt'; 	
		return $icon_styles;
	}
	
	function hcc_vc_mapping(){
		vc_map( array(
			"name" => __( "Housekeeper Form", HCC_TEXTDOMAIN ),
			"base" => "housekeeper_search",
			"category" => __( "Content", HCC_TEXTDOMAIN ),
			"params" => array(
					 
			 ),
		) );
	}
	add_action( 'rand_vc_mapping', 'hcc_vc_mapping' );
endif;


add_filter( 'rand_tinymce_custom_colors', 'hcc_tinymce_custom_colors', 1 );
function hcc_tinymce_custom_colors( $custom_colors ){
	$custom_colors = '"ff7b24", "HCC Orange",
						"5ea334", "HCC Green",
						"23599b", "HCC Blue"';
	return $custom_colors;	
}

add_filter( 'rand_tinymce_textcolor_rows', 'hcc_tinymce_textcolor_rows', 1 );
function hcc_tinymce_textcolor_rows( $rows ){
	$rows = 6;
	return $rows;	
}

add_filter( 'wcpv_vendor_taxonomy_args', 'hcc_wcpv_vendor_taxonomy_args' );
function hcc_wcpv_vendor_taxonomy_args( $args ){
	$labels = array(
			'name'              => _x( 'Housekeepers', 'taxonomy general name', HCC_TEXTDOMAIN ),
			'singular_name'     => _x( 'Housekeeper', 'taxonomy singular name', HCC_TEXTDOMAIN ),
			'search_items'      => __( 'Search Housekeepers', HCC_TEXTDOMAIN ),
			'all_items'         => __( 'All Housekeepers', HCC_TEXTDOMAIN ),
			'popular_items'     => __( 'Popular Housekeepers', HCC_TEXTDOMAIN ),
			'parent_item'       => __( 'Parent Housekeeper', HCC_TEXTDOMAIN ),
			'parent_item_colon' => __( 'Parent Housekeeper:', HCC_TEXTDOMAIN ),
			'edit_item'         => __( 'Edit Housekeeper', HCC_TEXTDOMAIN ),
			'view_item'         => __( 'View Housekeeper Page', HCC_TEXTDOMAIN ),
			'update_item'       => __( 'Update Housekeeper', HCC_TEXTDOMAIN ),
			'add_new_item'      => __( 'Add New Housekeeper', HCC_TEXTDOMAIN ),
			'new_item_name'     => __( 'New Housekeeper Name', HCC_TEXTDOMAIN ),
			'menu_name'         => __( 'Housekeepers', HCC_TEXTDOMAIN ),
			'not_found'         => __( 'No Housekeepers Found', HCC_TEXTDOMAIN ),
		);

		$new_args = array(
			'hierarchical'       => false,
			'labels'             => $labels,
			'show_ui'            => true,
			'show_admin_column'  => true,
			'query_var'          => true,
			'capabilities'       => array( 'manage_categories' ),
			'rewrite'            => array( 'slug' => 'housekeepers' ),
			'show_in_quick_edit' => false,
		);
		
	return array_merge($args, $new_args);	
}
function hcc_get_column_class( $how_many, $small = 'col-xs-12' ){
	if( $how_many >= 4 && $how_many % 4 == 0 ):
		$class = $small . ' col-md-3';
	elseif( $how_many % 3 == 0):
		$class = $small . ' col-md-4';
	elseif( $how_many == 2):
		$class = $small . ' col-md-6';
	elseif( $how_many == 1 ) :
		$class = $small;
	else:
		$class = $small . ' col-md-3';
	endif;
	return $class;		
}
function hcc_search_form_shortcode( $atts ){
	$atts = shortcode_atts( array(
		'id'			=> '',
	 	'post_type'		=> 'post',
		'featured_only'	=> 'no',
		'limit'			=> 10,
		'action'		=> '',
		'template' 		=> ''  
    ), $atts, 'housekeeper_search' );
	
	$output = '';
	ob_start();

		include( CHILD_DIR . '/maidform.php' );

	$output .= ob_get_clean();
	
	return $output;

}
add_shortcode( 'housekeeper_search', 'hcc_search_form_shortcode');

add_filter( 'login_message', 'hcc_login_message');
function hcc_login_message( $message ){
	return '';
}

add_filter( 'login_url', 'hcc_login_url' );
function hcc_login_url( $link  ) {
	
	if( ( !( is_admin() || ( isset($_SERVER['REQUEST_URI']) && ( strpos( $_SERVER['REQUEST_URI'], 'wp-admin') || strpos( $_SERVER['REQUEST_URI'], 'wp-login') ) ) ) ) && ( function_exists( "is_woocommerce_activated" ) && is_woocommerce_activated() ) ) :
		$link = get_the_permalink( get_option( 'woocommerce_myaccount_page_id' ) );
	endif;
	return $link;
}

//1. Add a new form element...
add_action( 'register_form', 'hcc_register_form' );
function hcc_register_form() {
wp_enqueue_script( 'password-strength-meter' );
$first_name = ( ! empty( $_POST['first_name'] ) ) ? trim( $_POST['first_name'] ) : '';
$last_name = ( ! empty( $_POST['last_name'] ) ) ? trim( $_POST['last_name'] ) : '';
$billing_phone = ( ! empty( $_POST['billing_phone'] ) ) ? trim( $_POST['billing_phone'] ) : '';
	
	?>
	<p>
		<label for="first_name"><?php _e( 'First Name', HCC_TEXTDOMAIN ) ?></label>
			<input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>" size="25" />
	</p>
	<p>
		<label for="last_name"><?php _e( 'Last Name', HCC_TEXTDOMAIN ) ?></label>
			<input type="text" name="last_name" id="last_name" class="input" value="<?php echo esc_attr( wp_unslash( $last_name ) ); ?>" size="25" />
	</p>
	<p>
		<label for="billing_phone"><?php _e( 'Phone Number', HCC_TEXTDOMAIN ) ?></label>
			<input type="tel" name="billing_phone" id="billing_phone" class="input" value="<?php echo esc_attr( wp_unslash( $billing_phone ) ); ?>" size="25" />
	</p>
	<p>
		<label for="password"><?php _e( 'Password', HCC_TEXTDOMAIN ) ?></label>
	 		<input type="password" name="password" class="input" />
	 	
	 </p>
	 <p>
		<label for="repeat_password"><?php _e( 'Repeat Password', HCC_TEXTDOMAIN ) ?></label>
			<input type="password" name="repeat_password" class="input" />
		
 	</p>
 <span id="password-strength"></span>
 <script>
 function checkPasswordStrength( $pass1,
                                $pass2,
                                $strengthResult,
                                $submitButton,
                                blacklistArray ) {
        var pass1 = $pass1.val();
    var pass2 = $pass2.val();
 
    // Reset the form & meter
    $submitButton.attr( 'disabled', 'disabled' );
        $strengthResult.removeClass( 'short bad good strong' );
 
    // Extend our blacklist array with those from the inputs & site data
    blacklistArray = blacklistArray.concat( wp.passwordStrength.userInputBlacklist() )
 
    // Get the password strength
    var strength = wp.passwordStrength.meter( pass1, blacklistArray, pass2 );
 
    // Add the strength meter results
    switch ( strength ) {
 
        case 2:
            $strengthResult.addClass( 'bad' ).html( pwsL10n.bad );
            break;
 
        case 3:
            $strengthResult.addClass( 'good' ).html( pwsL10n.good );
            break;
 
        case 4:
            $strengthResult.addClass( 'strong' ).html( pwsL10n.strong );
            break;
 
        case 5:
            $strengthResult.addClass( 'short' ).html( pwsL10n.mismatch );
            break;
 
        default:
            $strengthResult.addClass( 'short' ).html( pwsL10n.short );
 
    }
 
    // The meter function returns a result even if pass2 is empty,
    // enable only the submit button if the password is strong and
    // both passwords are filled up
    if ( 4 === strength && '' !== pass2.trim() ) {
        $submitButton.removeAttr( 'disabled' );
    }
 
    return strength;
}
 
jQuery( document ).ready( function( $ ) {
    // Binding to trigger checkPasswordStrength
    $( 'body' ).on( 'keyup', 'input[name=password], input[name=repeat_password]',
        function( event ) {
            checkPasswordStrength(
                $('input[name=password]'),         // First password field
                $('input[name=repeat_password]'), // Second password field
                $('#password-strength'),           // Strength meter
                $('input[type=submit]'),           // Submit button
                ['black', 'listed', 'word']        // Blacklisted words
            );
        }
    );
});
 </script>
	<?php 
	global $housekeeper;
	if ( is_page_template('search-housekeeper.php') || is_tax('wcpv_product_vendors') ): ?>
 		<input type="hidden" name="housekepper_id" value="<?php echo $housekeeper->term_id; ?>" />
	<?php
	endif;
}

//2. Add validation. In this case, we make sure first_name is required.
add_filter( 'registration_errors', 'hcc_registration_errors', 10, 3 );
function hcc_registration_errors( $errors, $sanitized_user_login, $user_email ) {
	
	if ( empty( $_POST['first_name'] ) || ( ! empty( $_POST['first_name'] ) && trim( $_POST['first_name'] ) == '' ) ) {
		$errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a first name.', HCC_TEXTDOMAIN ) );
	}
	if ( empty( $_POST['first_name'] ) || ( ! empty( $_POST['first_name'] ) && trim( $_POST['first_name'] ) == '' ) ) {
		$errors->add( 'last_name_error', __( '<strong>ERROR</strong>: You must include a last name.', HCC_TEXTDOMAIN ) );
	}
	if ( empty( $_POST['billing_phone'] ) || ( ! empty( $_POST['billing_phone'] ) && trim( $_POST['billing_phone'] ) == '' ) ) {
		$errors->add( 'billing_phone_error', __( '<strong>ERROR</strong>: You must include a phone number.', HCC_TEXTDOMAIN ) );
	}
	if ( $_POST['password'] !== $_POST['repeat_password'] ) {
		$errors->add( 'passwords_not_matched', "<strong>ERROR</strong>: Passwords must match" );
	}
	if ( strlen( $_POST['password'] ) < 8 ) {
		$errors->add( 'password_too_short', "<strong>ERROR</strong>: Passwords must be at least eight characters long" );
	}
	
	return $errors;
}

//3. Finally, save our extra registration user meta.
add_action( 'user_register', 'hcc_user_register' );
function hcc_user_register( $user_id ) {
	if ( ! empty( $_POST['first_name'] ) ) {
		update_user_meta( $user_id, 'first_name', trim( $_POST['first_name'] ) );
	}
	if ( ! empty( $_POST['last_name'] ) ) {
		update_user_meta( $user_id, 'last_name', trim( $_POST['last_name'] ) );
	}
	if ( ! empty( $_POST['billing_phone'] ) ) {
		update_user_meta( $user_id, 'billing_phone', trim( $_POST['billing_phone'] ) );
	}
	if ( ! empty( $_POST['password'] ) ) {
		update_user_meta( $user_id, 'user_pass', trim( $_POST['password'] ) );
	}
	if ( ! empty( $_POST['housekepper_id'] ) ) {
		update_user_meta( $user_id, 'housekeper_to_contact', trim( $_POST['housekepper_id'] ) );
	}
	
	$housekeepers = !empty( of_get_option('hcc_search_result_page') )		? get_permalink(of_get_option('hcc_search_result_page')) 		: get_home_url();
	//write_log( wp_get_referer());
	//write_log( $housekeepers);
	$pattern = '/'. preg_quote($housekeepers, '/') .'/';
	//write_log( $pattern );
	//write_log( preg_match( $pattern, wp_get_referer() ) );
	if ( preg_match( $pattern, wp_get_referer() ) ):
		//wp_set_current_user($user_id);
		$secure_cookie = is_ssl() ? true : false;
        wp_set_auth_cookie($user_id, true, $secure_cookie);
        $link = !empty( of_get_option( 'hcc_plans_page' ) ) ? get_the_permalink( of_get_option( 'hcc_plans_page' ) ) : get_site_url();
        wp_redirect( $link );
        exit;
	endif;
}

add_filter( 'gettext', 'hcc_edit_password_email_text' );
function hcc_edit_password_email_text ( $text ) {
	if ( $text == 'Registration confirmation will be emailed to you.' ) {
		$text = '';
	}
	
	if( $text == 'Go Shop' ){
		$text = __( 'Book Your Housekeeper', HCC_TEXTDOMAIN );
	}
	
	if( $text == 'Return To Shop' || $text == 'Continue Shopping' ){
		$text = __( 'Back to My Housekeeper', HCC_TEXTDOMAIN );
	}
	
	if( $text == 'Your request has been submitted.  You will be contacted shortly.' ){
		$text = __( 'You have been registered and successfully logged in. You\'ll be redirected in a moment, if not refresh the page. Check your email for your log in information', HCC_TEXTDOMAIN );
	}
	
	return $text;
}

add_filter('next_posts_link_attributes', 'hcc_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'hcc_posts_link_attributes');

function hcc_posts_link_attributes() {
    return 'class="box-btn blue"';
}

add_action( 'woocommerce_before_cart', 'hcc_container_before_content');
add_action( 'woocommerce_before_cart_is_empty', 'hcc_container_before_content');
add_action( 'woocommerce_before_customer_login_form', 'hcc_container_before_content');
add_action( 'hcc_woocommerce_my_account_before_content', 'hcc_container_before_content' );
function hcc_container_before_content(){
	echo '<div class="container p-y-2 p-x-2">';
}

add_action( 'woocommerce_after_cart', 'hcc_container_after_content');
add_action( 'woocommerce_after_cart_is_empty', 'hcc_container_before_content');
add_action( 'woocommerce_after_customer_login_form', 'hcc_container_before_content');
add_action( 'hcc_woocommerce_my_account_after_content', 'hcc_container_before_content' );
add_action( 'hcc_memberships_after_my_memberships', 'hcc_container_before_content' );
function hcc_container_after_content(){
	echo '</div>';
}


remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
add_action( 'woocommerce_before_shop_loop_item', 'hcc_template_loop_product_link_open' , 10);
$is_membership = false;
function hcc_template_loop_product_link_open(){
	global $is_membership, $post;
	$cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
	//var_dump( $cats );
	
	foreach( $cats as $cat):
		//var_dump($cat);
		if ( $cat->slug == 'memberships' ):
			$is_membership = true;
		endif;
	endforeach;
	if ( $is_membership ):
		echo '<div class="woocommerce-Loop membership-block ' . $post->post_name . '">';
	else:
		echo '<a href="' . get_the_permalink() . '" class="woocommerce-LoopProduct-link">';	
	endif;
}
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
add_action( 'woocommerce_after_shop_loop_item', 'hcc_template_loop_product_link_close', 5 );
function hcc_template_loop_product_link_close(){
	//$product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
	global $is_membership;
	if ( !$is_membership ):
		echo '</a>';
	endif;
}
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
add_action( 'woocommerce_after_shop_loop_item', 'hcc_template_loop_add_to_cart', 10, 1);
function hcc_template_loop_add_to_cart( $args = array() ){
	global $product, $is_membership;

		if ( $product ) {
			$defaults = array(
				'quantity' => 1,
				'class'    => implode( ' ', array_filter( array(
						'button',
						'product_type_' . $product->product_type,
						$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
						$product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : ''
				) ) )
			);

			$args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( $args, $defaults ), $product );

			wc_get_template( 'loop/add-to-cart.php', $args );
		}
		if ( $is_membership ):
			echo '</div></div></div>';
		endif;
}
add_action( 'woocommerce_after_shop_loop_item_title', 'hcc_template_loop_product_holder', 9 );
function hcc_template_loop_product_holder(){
	global $is_membership;
	if ( $is_membership ):
		echo '<div class="membership-holder"><div class="membership-holder-inner">';
	endif;
}
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );


add_filter( 'woocommerce_product_add_to_cart_text' , 'hcc_woocommerce_product_add_to_cart_text' );
/**
 * custom_woocommerce_template_loop_add_to_cart
*/
function hcc_woocommerce_product_add_to_cart_text() {
	global $product;
	
	$product_type = $product->product_type;
	
	//var_dump( $product_type );
	
	switch ( $product_type ) {
		case 'external':
			return __( 'Buy product', 'woocommerce' );
		break;
		case 'grouped':
			return __( 'View products', 'woocommerce' );
		break;
		case 'simple':
			return __( 'Add to cart', 'woocommerce' );
		break;
		case 'variable':
			return __( 'Select options', 'woocommerce' );
		break;
		case 'subscription':
			return __( 'Select', HCC_TEXTDOMAIN );
		default:
			return __( 'Read more', 'woocommerce' );
	}
	
}
add_filter( 'woocommerce_subscriptions_product_price_string', 'hcc_woocommerce_subscriptions_product_price_string', 10, 3 );

function hcc_woocommerce_subscriptions_product_price_string( $subscription_string, $product, $include ) {
	
	//var_dump( $subscription_string, $product, $include );
    /****
    var_dump($product); Various variables are available to us
    ****/
	$terms = get_the_terms( $product->id, 'product_tag' );
	//var_dump( $terms );
	$term_array = array();
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
		foreach ( $terms as $term ) {
			$term_array[] = $term->name;
		}
	}
	$tags = implode(' ', $term_array);
	if ( $product->subscription_period == 'year' ):
		$months = $product->subscription_period_interval * 12;
		$billed_text = 'BILLED $' . $product->subscription_price . ' ANNUALLY';
    else:
		if ( $product->subscription_period == 'month' && $product->subscription_period_interval == '1' ):
			$billed_text = 'BILLED MONTH to MONTH';
		elseif ( $product->subscription_period == 'month' && $product->subscription_period_interval == '3' ):
			$billed_text = 'BILLED $' . $product->subscription_price . ' QUARTERLY';
		endif;
		$months = $product->subscription_period_interval;
	endif;
	
	
	
	//var_dump( $months );
	return '<div class="month-price text-center">' . wc_price( $product->subscription_price / $months ) . '/<br /><span class="month-label">MONTH</span></div><div class="billed-text text-center">' . $billed_text . '</div><div class="tag">' . $tags . '</div>';
}

/**
 * Redirect users after add to cart for memberships category.
 */
function hcc_add_to_cart_redirect( $url ) {
	if ( ! isset( $_REQUEST['add-to-cart'] ) || ! is_numeric( $_REQUEST['add-to-cart'] ) ) {
		return $url;
	}
	
	$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_REQUEST['add-to-cart'] ) );
	
	//write_log( has_term( 'memberships', 'product_cat', $product_id ));
	// Only redirect products that have the 't-shirts' category
	if ( has_term( 'memberships', 'product_cat', $product_id ) ) {
		$url = WC()->cart->get_checkout_url();
	}
	return $url;
}
add_filter( 'woocommerce_add_to_cart_redirect', 'hcc_add_to_cart_redirect' );

// remove Order Notes from checkout field in Woocommerce
add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );

remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
add_action( 'woocommerce_checkout_after_customer_details', 'woocommerce_checkout_payment', 20 );

add_filter( 'woocommerce_add_notice', 'hcc_booking_cart_notice' );
function hcc_booking_cart_notice( $message ) {
    if( preg_match('/^A booking for (.*) has been removed from your cart due to inactivity\.$/', $message) )
        $message = 'Your booking has been removed from your cart due to inactivity.';

    return $message;
}

/*********************************************************************************************/
/** WooCommerce - Modify each individual input type $args defaults /**
/*********************************************************************************************/

add_filter('woocommerce_form_field_args','hcc_form_field_args',10,3);

function hcc_form_field_args( $args, $key, $value = null ) {

/*********************************************************************************************/
/** This is not meant to be here, but it serves as a reference
/** of what is possible to be changed. /**

$defaults = array(
    'type'              => 'text',
    'label'             => '',
    'description'       => '',
    'placeholder'       => '',
    'maxlength'         => false,
    'required'          => false,
    'id'                => $key,
    'class'             => array(),
    'label_class'       => array(),
    'input_class'       => array(),
    'return'            => false,
    'options'           => array(),
    'custom_attributes' => array(),
    'validate'          => array(),
    'default'           => '',
);
********************************************************************************************/

// Start field type switch case

switch ( $args['type'] ) {

    case "select" :  /* Targets all select input type elements, except the country and state select input types */
        $args['class'][] = 'form-group'; // Add a class to the field's html element wrapper - woocommerce input types (fields) are often wrapped within a <p></p> tag  
        $args['input_class'] = array('form-control', 'input-lg'); // Add a class to the form input itself
        //$args['custom_attributes']['data-plugin'] = 'select2';
        $args['label_class'] = array('control-label');
        $args['custom_attributes'] = array( 'data-plugin' => 'select2', 'data-allow-clear' => 'true', 'aria-hidden' => 'true',  ); // Add custom data attributes to the form input itself
    break;

    case 'country' : /* By default WooCommerce will populate a select with the country names - $args defined for this specific input type targets only the country select element */
        $args['class'][] = 'form-group single-country';
		$args['input_class'] = array( 'input-l'); // add class to the form input itself
        $args['label_class'] = array('control-label');
    break;

    case "state" : /* By default WooCommerce will populate a select with state names - $args defined for this specific input type targets only the country select element */
        $args['class'][] = 'form-group'; // Add class to the field's html element wrapper 
        $args['input_class'] = array( 'input-l'); // add class to the form input itself
        //$args['custom_attributes']['data-plugin'] = 'select2';
        $args['label_class'] = array('control-label');
        $args['custom_attributes'] = array( 'data-plugin' => 'select2', 'data-allow-clear' => 'true', 'aria-hidden' => 'true',  );
    break;


    case "password" :
    case "text" :
    case "email" :
    case "tel" :
    case "number" :
        $args['class'][] = 'form-group';
        //$args['input_class'][] = 'form-control input-lg'; // will return an array of classes, the same as bellow
        $args['input_class'] = array('form-control', 'input-lg');
        $args['label_class'] = array('control-label');
    break;

    case 'textarea' :
        $args['input_class'] = array('form-control', 'input-lg');
        $args['label_class'] = array('control-label');
    break;

    case 'checkbox' :  
    break;

    case 'radio' :
    break;

    default :
        $args['class'][] = 'form-group';
        $args['input_class'] = array('form-control', 'input-lg');
        $args['label_class'] = array('control-label');
    break;
    }

    return $args;
}

function hcc_array_insert_after( $needle, $haystack, $new_key, $new_value) {

	if ( array_key_exists( $needle, $haystack ) ) {

		$new_array = array();

		foreach ( $haystack as $key => $value ) {

			$new_array[ $key ] = $value;

			if ( $key === $needle ) {
				$new_array[ $new_key ] = $new_value;
			}
		}

		return $new_array;
	}

	return $haystack;
}

function hcc_endpoints() {
    add_rewrite_endpoint( 'house-profile', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'my-housekeeper', EP_ROOT | EP_PAGES );
	add_rewrite_endpoint( 'my-favorites', EP_ROOT | EP_PAGES );
}

add_action( 'init', 'hcc_endpoints' );

function hcc_query_vars( $vars ) {
    $vars[] = 'house-profile';
	$vars[] = 'my-housekeeper';
	$vars[] = 'my-favorites';

    return $vars;
}

add_filter( 'query_vars', 'hcc_query_vars', 0 );

function hcc_wc_add_menu_items( $menu_items ) {

	// Add our menu item after the Orders tab if it exists, otherwise just add it to the end
	if ( array_key_exists( 'dashboard', $menu_items ) ) {
		$menu_items = hcc_array_insert_after( 'dashboard', $menu_items, 'house-profile', __( 'Your House Profile', HCC_TEXTDOMAIN ) );
		$menu_items = hcc_array_insert_after( 'house-profile', $menu_items, 'my-housekeeper', __( 'My Housekeeper', HCC_TEXTDOMAIN ) );
		$menu_items = hcc_array_insert_after( 'my-housekeeper', $menu_items, 'my-favorites', __( 'My Favorites', HCC_TEXTDOMAIN ) );
	} else {
		$menu_items['house-profile'] = __( 'Your House Profile', HCC_TEXTDOMAIN );
		$menu_items['my-housekeeper'] = __( 'My Housekeeper', HCC_TEXTDOMAIN );
		$menu_items['my-favorites'] = __( 'My Favorites', HCC_TEXTDOMAIN );
	}

	return $menu_items;
}
add_filter( 'woocommerce_account_menu_items', 'hcc_wc_add_menu_items' );

function hcc_your_house_profile_endpoint_content(){
	wc_get_template( 'myaccount/house-profile.php', array(), '', CHILD_DIR . '/woocommerce/templates/' );
}
add_action( 'woocommerce_account_house-profile_endpoint', 'hcc_your_house_profile_endpoint_content');

function hcc_my_housekeeper_endpoint_content(){
	wc_get_template( 'myaccount/my-housekeeper.php', array(), '', CHILD_DIR . '/woocommerce/templates/' );
}
add_action( 'woocommerce_account_my-housekeeper_endpoint', 'hcc_my_housekeeper_endpoint_content');

function hcc_my_favorites_endpoint_content(){
	wc_get_template( 'myaccount/my-favorites.php', array(), '', CHILD_DIR . '/woocommerce/templates/' );
}
add_action( 'woocommerce_account_my-favorites_endpoint', 'hcc_my_favorites_endpoint_content');

function hcc_woocommerce_return_to_shop_redirect(){
	return esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) . 'my-housekeeper' );
}
add_action( 'woocommerce_return_to_shop_redirect', 'hcc_woocommerce_return_to_shop_redirect' );

function hcc_remove_hooks_on_my_housekeeper_endpoint(){
	global $wp;
	//var_dump( $wp );
	if ( isset( $wp->query_vars[ 'my-housekeeper' ] ) ){
		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
		add_action( 'woocommerce_before_single_product_summary', 'hcc_show_housekeeper_image', 20 );
		add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
	}
	if ( isset( $wp->query_vars[ 'members_area' ] ) ){
		add_action('woocommerce_before_account_navigation', 'hcc_container_before_content');
	}
}
add_action( 'wp', 'hcc_remove_hooks_on_my_housekeeper_endpoint');

function hcc_show_housekeeper_image(){
	wc_get_template( 'my-housekeeper/housekeeper-image.php' );
}

function wcs_woo_remove_reviews_tab($tabs) {
	unset($tabs['reviews']);
	return $tabs;
}
/* 
 * ====================================================== HOUSE KEEPER FORM SCRIPTS ==================================================
 */
require_once( CHILD_DIR . '/includes/house-profile-form.php' ); //require house keeper form scripts

add_action( 'show_user_profile', 'hcc_show_extra_profile_fields', 1 );
add_action( 'edit_user_profile', 'hcc_show_extra_profile_fields', 1 );

function hcc_show_extra_profile_fields( $user ) { 
	$args = array(
						'hierarchical' => false,
						'hide_empty'   => false,
						'meta_query'	=> array(
												array(
													'key'	=> 'maid_approved',
													'value'	=> 1,
													'compare' => '='
												),
											),
					);
	$housekeepers 		= get_terms('wcpv_product_vendors', $args );
?> 	
    <h3>Hosekeeper</h3>
 
    <table class="form-table">
 
        <tr>
            <th><label for="housekeper_to_contact">Housekeeper Desired to Contact</label></th>
 
            <td>
                <select name="housekeper_to_contact" id="housekeper_to_contact" class="regular-text enhanced" >
					<option value=""></option>
				<?php
				if ( ! empty( $housekeepers ) ) :
					foreach( $housekeepers as $housekeeper ) :
				?>
					<option value="<?php echo $housekeeper->term_id; ?>" <?php selected( esc_attr( get_the_author_meta( 'housekeper_to_contact', $user->ID ) ), $housekeeper->term_id ); ?>><?php echo $housekeeper->name; ?></option>
				<?php
					endforeach;
				endif;
				?>
				</select><br />
                <span class="description">The housekeeper user chose to contact.</span>
            </td>
        </tr>
		<tr>
            <th><label for="housekeper_assigned">Assigned Housekeeper</label></th>
 
            <td>
                <select class="enhanced regular-text" name="housekeper_assigned" id="housekeper_assigned" <?php  if( !current_user_can('manage_options') ): ?> disabled="disabled"<?php endif; ?> >
					<option value=""></option>
				<?php
				if ( ! empty( $housekeepers ) ) :
					foreach( $housekeepers as $housekeeper ) :
				?>
					<option value="<?php echo $housekeeper->term_id; ?>" <?php selected( esc_attr( get_the_author_meta( 'housekeper_assigned', $user->ID ) ), $housekeeper->term_id ); ?>><?php echo $housekeeper->name; ?></option>
				<?php
					endforeach;
				endif;
				?>
				</select><br />
                <span class="description">The housekeeper assigned to the user.</span>
            </td>
        </tr>
		<tr>
            <th><label for="housekeper_favorites">Favorite Housekeepers</label></th>
 
            <td><?php //var_dump( get_the_author_meta( 'housekeper_favorites', $user->ID ) ); ?>
                <select name="housekeper_favorites[]" id="housekeper_favorites" class="regular-text enhanced" multiple="multiple" >
					<option value=""></option>
				<?php
				
				if ( ! empty( $housekeepers ) ) :
					foreach( $housekeepers as $housekeeper ) :
						$favorites = !empty( get_the_author_meta( 'housekeper_favorites', $user->ID ) ) ? get_the_author_meta( 'housekeper_favorites', $user->ID ) : array();
				?>
					<option value="<?php echo $housekeeper->term_id; ?>" <?php selected( true, in_array( $housekeeper->term_id, $favorites ) , true); ?>><?php echo $housekeeper->name; ?></option>
				<?php
					endforeach;
				endif;
				?>
				</select><br />
                <span class="description">The housekeeper user chose to contact.</span>
            </td>
        </tr>
 
    </table>
<?php }

add_action( 'personal_options_update', 'hcc_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'hcc_save_extra_profile_fields' );
 
function hcc_save_extra_profile_fields( $user_id ) {
 
    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
 
    /* Copy and paste this line for additional fields. Make sure to change 'twitter' to the field ID. */
	if ( isset( $_POST['housekeper_to_contact'] ) )
    	update_user_meta( absint( $user_id ), 'housekeper_to_contact', wp_kses_post( $_POST['housekeper_to_contact'] ) );
	if ( isset( $_POST['housekeper_assigned'] ) )
		update_user_meta( absint( $user_id ), 'housekeper_assigned', wp_kses_post( $_POST['housekeper_assigned'] ) );
	if ( isset( $_POST['housekeper_favorites'] ) )
		update_user_meta( absint( $user_id ), 'housekeper_favorites', wp_kses_post( $_POST['housekeper_favorites'] ) );
}

add_action( 'wp_ajax_hcc_add_favorite', 'hcc_ajax_add_favorite' );
add_action( 'wp_ajax_nopriv_hcc_add_favorite', 'hcc_ajax_add_favorite' );

function hcc_ajax_add_favorite() {
    // Handle request then generate response using WP_Ajax_Response
	$housekeeper_id = wp_kses_post( $_POST['hcc_id'] );
	$user_id 		= get_current_user_id();
	$favorites 		= !empty( get_user_meta( $user_id, 'housekeper_favorites', true ) ) ? get_user_meta( $user_id, 'housekeper_favorites', true ) : array();
	//write_log( $favorites);
	array_push( $favorites, $housekeeper_id );
	//write_log( $favorites);
	
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { 
		$updating = update_user_meta( absint( $user_id ), 'housekeper_favorites', $favorites );
		if ( is_wp_error($updating) ):
			echo 'error';
		else:
			echo 'success';
		endif;
	}
	die();
}

add_action( 'wp_ajax_hcc_remove_favorite', 'hcc_ajax_remove_favorite' );
add_action( 'wp_ajax_nopriv_hcc_remove_favorite', 'hcc_ajax_remove_favorite' );

function hcc_ajax_remove_favorite() {
    // Handle request then generate response using WP_Ajax_Response
	$housekeeper_id = wp_kses_post( $_POST['hcc_id'] );
	$user_id 		= get_current_user_id();
	$favorites 		= !empty( get_user_meta( $user_id, 'housekeper_favorites', true ) ) ? get_user_meta( $user_id, 'housekeper_favorites', true ) : array();
	//write_log( $favorites);
	$favorites		= array_diff( $favorites, array( $housekeeper_id ) );
	//write_log( $favorites);
	
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { 
		$updating = update_user_meta( absint( $user_id ), 'housekeper_favorites', $favorites );
		if ( is_wp_error($updating) ):
			echo 'error';
		else:
			echo 'success';
		endif;
	}
	die();
}

add_action( 'wp_ajax_hcc_contact_housekeeper', 'hcc_ajax_contact_housekeeper' );
add_action( 'wp_ajax_nopriv_hcc_contact_housekeeper', 'hcc_ajax_contact_housekeeper' );

function hcc_ajax_contact_housekeeper() {
    // Handle request then generate response using WP_Ajax_Response
	$housekeeper_id = wp_kses_post( $_POST['hcc_id'] );
	$user_id 		= get_current_user_id();
	//write_log( $favorites);
	//array_push( $favorites, $housekeeper_id );
	//write_log( $favorites);
	
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { 
		$updating = update_user_meta( absint( $user_id ), 'housekeper_to_contact', $housekeeper_id );
		if ( is_wp_error($updating) ):
			echo 'error';
		else:
			echo 'success';
		endif;
	}
	die();
}
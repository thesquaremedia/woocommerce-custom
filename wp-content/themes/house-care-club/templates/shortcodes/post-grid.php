<?php
$output		= '<div class="post-grid-holder"><div class="post-grid"'. $id .'>';
// The Loop
while ( $posts->have_posts() ) {
	$posts->the_post();
	
	$image = '';
	if( has_post_thumbnail() ):
		$image			=  get_the_post_thumbnail(get_the_ID(), 'full') ;
	endif;
	
	$link 		= get_the_permalink();
	$title 		= get_the_title();
	$content	= get_the_content();
	
	//var_dump( $image );
	$output 	.= '<div class="post-grid-item"><div class="post-grid-item-inner"><p class="text-center">"' . $content . '"</p><p class="text-center"><strong> - ' . $title . '</strong></p></div></div>';
}
/* Restore original Post Data 
 * NB: Because we are using new WP_Query we aren't stomping on the 
 * original $wp_query and it does not need to be reset with 
 * wp_reset_query(). We just need to set the post data back up with
 * wp_reset_postdata().
 */
wp_reset_postdata();
$output .= '<div class="clearfix"></div></div></div>';

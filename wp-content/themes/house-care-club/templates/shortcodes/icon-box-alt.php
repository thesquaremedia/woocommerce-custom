<?php
if ( !empty($content) ):
	$output .= '<div class="icon-box box-style' . $style . '">' . $inner_icon . $icon . '<div class="icon-box-content">' . wpautop($content) . '</div></div>';
else:
	$output .= $icon;
endif;
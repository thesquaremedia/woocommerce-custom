// JavaScript Document
/*global wc_setup_params */
jQuery( function( $ ) {

	$( '.button-next' ).on( 'click', function() {
		$('.maid-setup-content').block({
			message: null,
			overlayCSS: {
				background: '#fff',
				opacity: 0.6
			}
		});
		return true;
	} );

	$( '.maid-setup' ).on( 'click', '.maid-upload-logo', function( e ) {
		e.preventDefault();

		// create the media frame
		var i18n = maid_setup_local,
			inputField = $( this ).parents( '.form-group' ).find( 'input[name="maid_data[logo]"]' ),
			previewField = $( this ).parents( '.form-group' ).find( '.maid-logo-preview-image' ),
			mediaFrame = wp.media.frames.mediaFrame = wp.media({

				title: i18n.modalLogoTitle,

				button: {
					text: i18n.buttonLogoText
				},

				// only images
				library: {
					type: 'image'
				},

				multiple: false
			});

		// after a file has been selected
		mediaFrame.on( 'select', function() {
			var selection = mediaFrame.state().get( 'selection' );

			selection.map( function( attachment ) {

				attachment = attachment.toJSON();

				if ( attachment.id ) {

					// add attachment id to input field
					inputField.val( attachment.id );

					// show preview image
					previewField.prop( 'src', attachment.url ).removeClass( 'hide' );

					// show remove image icon
					$( inputField ).parents( '.form-group' ).find( '.maid-remove-image' ).show();
				}
			});
		});

		// open the modal frame
		mediaFrame.open();
	});

	$( '.maid-setup' ).on( 'click', '.maid-remove-image', function( e ) {
		e.preventDefault();
		var i18n = maid_setup_local;
		$( this ).hide();
		$( this ).parents( '.form-group' ).find( '.maid-logo-preview-image' ).prop( 'src', i18n.CHILD_URI+'/images/logo-placeholder.png' );
		$( 'input[name="maid_data[logo]"]' ).val( '' );
	});
	
	
	$('.datepicker').datetimepicker({
		format: 'MM/DD/YYYY',
		keepOpen: true,	
		showClear: true,
		showClose: true,
	});
	$('.datepicker').on('focus',function(e){
		$('.datepicker').data("DateTimePicker").show();
	});	
	
	$('.tel, input[type="tel"]').mask("(999) 999-9999");
	
	$('.select-all').on( 'change', function() {
		var checkboxes = $(this).attr('name');
		if(this.checked) {
			$( 'input[data-all="'+checkboxes+'"]:not(input[value=None])' ).prop('checked', true);
			$( 'input[data-all="'+checkboxes+'"][value=None]' ).prop('checked', false);
		}else{
			$( 'input[data-all="'+checkboxes+'"]' ).prop('checked', false);
		}
	});

	$( '.has-dependencies' ).on( 'click' , function(e){
		console.log( $(this).data('dependency-value') , $(this).val() );
		if( $(this).is(':radio') && $(this).is(':checked') && $(this).val() == $(this).data('dependency-value') ){
			$( '.dependent-'+$(this).data('selector') ).removeClass('hidden-xs hidden-sm hidden-md hidden-lg');
			$( '.dependent-'+$(this).data('selector') ).css({ 'display' : 'none'}).show(400);
		}else{
			$( '.dependent-'+$(this).data('selector') ).css({ 'display' : 'block'}).hide(400).addClass('hidden-xs hidden-sm hidden-md hidden-lg');
		}
	});
	
} );
